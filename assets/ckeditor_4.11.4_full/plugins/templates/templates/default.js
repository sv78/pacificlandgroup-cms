CKEDITOR.addTemplates(
        "default",
        {imagesPath: CKEDITOR.getUrl("/assets/themes/default/img/ckeditor4_templates_images/"),
          templates: [
            /*
            {
              title: "Section White 33 Example",
              image: "section_white_33.gif",
              description: "A full width adaptive block devided on 2 zones - 1. Background with an image; 2. Some content assumed",
              html: '<div class="row-section row-section_light bg-color_white"><div class="row-section__bg row-section__bg_33 row-section__bg_left bg_civil-engineering"></div><div class="row-section__txt row-section__txt_67 row-section__txt_right"><div class="row-section__content-container"><h1 class="h-simple">Header H1</h1><hr class="short-line"><p>Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here.</p></div></div></div>'
            },*/
            /*
            {
              title: "Section White 33",
              image: "section_white_33.gif",
              description: "A full width adaptive block devided on 2 zones - 1. Background with an image; 2. Some content assumed.<br>Important: to change side image you should copy new image relative URI, go to `Source` mode and replace existing URI manually inside of `div` tag and its attribute `style` `background-image`.",
              html: '<div class="row-section row-section_light bg-color_white"><div class="row-section__bg row-section__bg_33 row-section__bg_left bg_civil-engineering" style="background-image:url(\'/assets/uploads/highest-and-best-use-analysis/key-benefits.jpg\')"></div><div class="row-section__txt row-section__txt_67 row-section__txt_right"><div class="row-section__content-container"><h1 class="h-simple">Header H1</h1><hr class="short-line"><p>Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here.</p></div></div></div>'
            },
     */
				
												{
              title: "Youtube Video",
              image: "youtube_video.gif",
              description: "Creates a basic container for Youtube video.",
              html: '<div class="video-container"><iframe src="https://www.youtube.com/embed/rn__FWhxRBg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="video-content"></iframe></div>'
            },
												{
              title: "Vimeo Video",
              image: "vimeo_video.gif",
              description: "Creates a basic container for Vimeo video.",
              html: '<div class="video-container"><iframe src="https://player.vimeo.com/video/471128176" frameborder="0" allowfullscreen class="video-content"></iframe></div>'
            },
            {
              title: "Container Darky",
              image: "section_darky.gif",
              description: "A full width block on a dark color background",
              html: '<div class="row-section row-section_dark bg-color_darky"><div class="row-section__txt row-section__txt_100 row-section__txt_right"><div class="row-section__content-container"><h2 class="h-simple">Header H2</h2><hr class="short-line"><p>Some text goes here. Some text goes here. Some text goes here. </p></div></div></div>'
            },
            {
              title: "Container Noble Blue",
              image: "section_noble-blue.gif",
              description: "A full width block on a very dark color background",
              html: '<div class="row-section row-section_dark bg-color_noble-blue"><div class="row-section__txt row-section__txt_100 row-section__txt_right"><div class="row-section__content-container"><h2 class="h-simple">Header H2</h2><hr class="short-line"><p>Some text goes here. Some text goes here. Some text goes here. </p></div></div></div>'
            },
            {
              title: "Container Light Gray",
              image: "section_light-gray.gif",
              description: "A full width block on a very light gray color background",
              html: '<div class="row-section row-section_light bg-color_light-gray"><div class="row-section__txt row-section__txt_100 row-section__txt_right"><div class="row-section__content-container"><h2 class="h-simple">Header H2</h2><hr class="short-line"><p>Some text goes here. Some text goes here. Some text goes here. </p></div></div></div>'
            },
            {
              title: "Container White",
              image: "section_white.gif",
              description: "A full width block on white color background",
              html: '<div class="row-section row-section_light bg-color_white"><div class="row-section__txt row-section__txt_100 row-section__txt_right"><div class="row-section__content-container"><h1 class="h-simple">Header H1</h1><hr class="short-line"><p>Some text goes here. Some text goes here. Some text goes here. </p></div></div></div>'
            },
            {title: "Internal block",
              image: "header_line_text.gif",
              description: "Header with a line and than text goes.",
              html: '<h1 class="h-simple">Header</h1><hr class="short-line"><p>Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here. Some text goes here.</p>'
            },
            {title: "Industry News Announcement",
              image: "industry-news_announcement.gif",
              description: "Creates industry news announcement elements. Don't forget to set links. It's better practice to use relative links with forward slash, for example: `/land-planner-corner/current-news-uri-segment`. Trailing slash is not required.",
              html: '<h4>Month 20xx</h4><p><a class="color_blue" href="#"><span class="_bold">Current News Header (link)</span> </a></p><p>A brief description of current news</p><p><a class="color_blue" href="#">Read more</a></p>'
            },
            {
              title: "Staff Card",
              image: "staff_card.gif",
              description: "Creates `Staff Card` block. Important: if person image changed don't forget to change `alt` attribute for `img` tag and `href` attribure for blue colored `a` tag. Also this block contains some invisible elements, so it's better way to edit it in `Source` mode.",
              html: '<div class="staff-card"><div class="staff-card__img-container"><img alt="Oleg Verbenkov" src="/assets/uploads/staff/oleg-verbenkov.jpg"></div><div class="staff-card__txt-container"><h5 class="staff-card__header">Talk to us</h5><p>Talk to us about ... Please contact <a class="color_a _bold _italic" href="/pacific-land-group-team#plg_staff_oleg_verbenkov">Oleg</a> ... some text goes here ... <mark>some color marked text goes here</mark>.</p><div class="staff-card__icons-panel"><a href="/contact-us" class="staff-card__icon staff-card__icon_email"></a><a href="tel:6045011624" class="staff-card__icon staff-card__icon_phone"></a><a class="staff-card__icon-prone-number staff-card__icon-prone-number_hidden" href="tel:6045011624">604-501-1624</a></div></div></div>'
            }
          ]
        }
);