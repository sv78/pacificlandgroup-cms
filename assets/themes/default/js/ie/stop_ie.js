;
var msg = location.hostname + '\r\n\r\nUnfortunately the browser you use is badly outdated and is not supported by this website. Please update your browser or install new one.\r\n\r\nDo you want to install Google Chrome?';
var conf = confirm(msg);
if (conf === true) {
  var link = 'https://www.google.com/chrome/browser/desktop/';
} else {
  var link = 'https://www.google.com/search?q=why+you+need+to+update+your+browser';
}
// window.open(link, '_blank');
location.assign(link);