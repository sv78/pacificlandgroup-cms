CKEDITOR.replace(
        'editor',
        {
          width: "99.5%",
          height: 400,
        }
);

CKEDITOR.config.allowedContent = true;
CKEDITOR.config.autoParagraph = false;
CKEDITOR.config.enterMode = 1; // 2 will create <br> instead of new <p>{text}</p>

CKEDITOR.config.language = "en";

// feeding actual css
CKEDITOR.config.contentsCss = "/assets/themes/default/css.css";
CKEDITOR.config.bodyClass = 'body theme-default';

// allow empty tags like <a></a>
CKEDITOR.config.protectedSource.push(/<a[^>]*><\/a>/g);
CKEDITOR.config.protectedSource.push(/<div[^>]*><\/div>/g);
CKEDITOR.config.protectedSource.push(/<span[^>]*><\/span>/g);


// Add styles for all headings inside editable contents.
// CKEDITOR.addCss('.cke_editable h1,.cke_editable h2,.cke_editable h3 { border-bottom: 1px dotted red }');
CKEDITOR.addCss('.cke_editable h1,.cke_editable h2,.cke_editable h3 { border-bottom: 1px dotted red }');

// CKEDITOR.config.autoUpdateElement = false;
// CKEDITOR.config.fullPage = false;


// My toolbar

CKEDITOR.config.toolbarCanCollapse = true;

CKEDITOR.config.toolbar_EditPage = [
  {
    name: 'a',
    items: [
      'Maximize', 'ShowBlocks', 'Source', 'Preview',
      '-',
      'NewPage', 'Templates', 'Save',
      '-',
      'Undo', 'Redo',
      '-',
      'Copy', 'Cut', 'Paste', 'PasteText',
      '-',
      'SelectAll',
      '-',
      'Find', 'Replace',
      '-',
      'CopyFormatting', 'RemoveFormat',
    ]
  },
  {
    name: 'b',
    items: [
      'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript',
      '-',
      'BulletedList', 'NumberedList',
      '-',
      'Indent', 'Outdent',
      '-',
      'Link', 'Unlink', 'Anchor',
      '-',
      'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
      '-',
      'Image', 'HorizontalRule', 'Table', 'SpecialChar',
      '-',
      'CreateDiv',
    ]
  },
  {
    name: 'c',
    items: [
      'Styles', 'Format', 'Font', 'FontSize',
      '-',
      'TextColor', 'BGColor',
    ]
  }
];

CKEDITOR.config.toolbar = "EditPage";

/**
 * Styles
 */

CKEDITOR.stylesSet.add('my_styles', [
  // Block-level styles:
  {name: 'H1 Compound Light', element: 'h1', attributes: {'class': "h1-compound h1-compound_light"}},
  {name: 'H1 Compound Dark', element: 'h1', attributes: {'class': "h1-compound h1-compound_dark"}},
  {name: 'H1 Big on BG', element: 'h1', attributes: {'class': "h1-very-big"}},
  {name: 'H1 on side Image', element: 'h1', attributes: {'class': "row-section__bg-header"}},
  {name: 'H1', element: 'h1', attributes: {'class': "h-simple"}},
  {name: 'H2', element: 'h2', attributes: {'class': "h-simple"}},
  {name: 'H2 Blue', element: 'h2', attributes: {'class': "h-simple color_blue"}},
  {name: 'H3', element: 'h3', attributes: {'class': "h-simple"}},
  {name: 'Staff Card Header', element: 'h5', attributes: {'class': "staff-card__header"}},
  {name: 'Blue Paragraph', element: 'p', attributes: {'class': "color_blue"}},
  {name: 'Image description', element: 'p', attributes: {'class': "image-description"}},
  {name: 'Peson name', element: 'p', attributes: {'class': "person-card__name"}},

  // Object styles:
  {name: 'Short HR', element: 'hr', attributes: {'class': "short-line"}},
  {name: 'Ul Blue', element: 'ul', attributes: {'class': "color_blue _bold"}},
  {name: 'Ul Darky Normal', element: 'ul', attributes: {'class': "color_darky _normal"}},
  {name: 'Ul Normal Blue', element: 'ul', attributes: {'class': "ul-normal color_blue"}},
  {name: 'Ul Bold', element: 'ul', attributes: {'class': "_bold"}},
  {name: 'Ul Extra Bold', element: 'ul', attributes: {'class': "ul-extra _bold"}},
  {name: 'Ul Bold Blue', element: 'ul', attributes: {'class': "ul-extra color_blue _bold"}},
  {name: 'Ul White No Dots', element: 'ul', attributes: {'class': "ul-list color_white"}},
  {name: 'Ul Dark No Dots', element: 'ul', attributes: {'class': "ul-list color_dark"}},
  {name: 'Ul White Green Dots', element: 'ul', attributes: {'class': "ul-normal ul_green-dots color_white"}},
  {name: 'Ul Nested', element: 'ul', attributes: {'class': "ul-nested"}},
  {name: 'Image Responsive', element: 'img', attributes: {'class': "img-full-width"}},
  {name: 'Staff card container', element: 'div', attributes: {'class': "staff-card"}},
  {name: 'Person card container', element: 'div', attributes: {'class': "person-card"}},
  {name: 'Link like button', element: 'a', attributes: {'class': "button-like"}},

  // Inline styles:
  {name: 'Bold', element: 'span', attributes: {'class': '_bold'}},
  {name: 'Blue', element: 'span', attributes: {'class': 'color_blue'}},
  {name: 'Blue Link', element: 'a', attributes: {'class': 'color_blue'}},
  {name: 'Mark', element: 'mark'},
  {name: 'Staff Card Link', element: 'a', attributes: {'class': 'color_a _bold _italic'}},
  {name: 'Person position', element: 'i', attributes: {'class': ''}},
]);


CKEDITOR.config.stylesSet = "my_styles";


/**
 * Format
 */

// There is no need to change Format now

// CKEDITOR.config.format_tags = 'p;h1;h2;h3;pre;div'; // elements in list

// CKEDITOR.config.format_h1 = {element: 'h1', attributes: {'class': 'some-class'}};


/**
 * Templates:
 * They are modified right in plugin folder 'plugins/templates/templates/default.js'
 */

CKEDITOR.config.forcePasteAsPlainText = true;

CKEDITOR.config.templates_replaceContent = false;