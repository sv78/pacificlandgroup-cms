window.onload = function () {

  /* Scrolling to top */
  /* requires `jquery.js` library for some small functions */

  (function () {
    function isScrolled() {
      return  ($(window).scrollTop() > 90) ? true : false;
    }

    window.addEventListener('scroll', scrollListener, false);

    function scrollListener(e) {
      toggleBTTMS();
    }

    function toggleBTTMS() {
      if (isScrolled()) {
        showBTTMS();
      } else {
        hideBTTMS();
      }
    }

    function showBTTMS() {
      $('div.menustarter-btn').removeClass('menustarter-btn_hidden');
      $('div.backtotop-btn').removeClass('backtotop-btn_hidden');
    }

    function hideBTTMS() {
      $('div.menustarter-btn').addClass('menustarter-btn_hidden');
      $('div.backtotop-btn').addClass('backtotop-btn_hidden');
    }

    function backToTop(e) {
      $('html, body').animate({
        scrollTop: $("#body").offset().top
      }, 800);
    }

    document.querySelector('.backtotop-btn').addEventListener('click', backToTop, false);
    toggleBTTMS();

  })();




  // staff cards phone icon handler
  (function () {
    var staffPhoneBtn = document.querySelector('.staff-card__icon_phone');
    var phoneNumberContainer = document.querySelector('.staff-card__icon-prone-number');
    if (staffPhoneBtn !== null && phoneNumberContainer !== null) {
      staffPhoneBtn.addEventListener('click', staffPhoneBtnClickListener, false);
    }
    function staffPhoneBtnClickListener(e) {
      e.preventDefault();
      if ($(phoneNumberContainer).hasClass('staff-card__icon-prone-number_hidden')) {
        $(phoneNumberContainer).removeClass('staff-card__icon-prone-number_hidden');
      } else {
        $(phoneNumberContainer).addClass('staff-card__icon-prone-number_hidden');
      }
    }
  })();


  /* Blue menu mobile devices behaviour */
  
  (function (){
    const menu = document.querySelector('.menu');
    if (menu !== null) {
      menu.addEventListener('click', menuClickListener, false);
    }
    function menuClickListener (e) {
      if (window.innerWidth > 1366) { // 1024
        return;
      }
      if (e.target.parentNode.tagName === 'DIV') {
        if (e.target.parentNode.nextElementSibling !== null) {
          e.preventDefault();
          if ($(e.target.parentNode.nextElementSibling).css('visibility') === 'visible') {
            $(e.target.parentNode.nextElementSibling).css('visibility','collapse');
          }
          else {
            $(e.target.parentNode.nextElementSibling).css('visibility', 'visible');
          }
        }
      }
    }
    
  })();


  /* Fabulous Menu */
  /* requires `jquery.js` library for some small functions */

  (function () {

    function foo() {
    }
    ;

    var menu = document.querySelector('.fab-menu');
    if (menu == null && menu == undefined) {
      throw "### Fab Menu component is not found. missing .fab-menu css class. Probably script was loaded earlier than DOM generated ready.";
    }
    var menuStateContainer = document.querySelector('.fab-menu__state-container');

    var openers = document.querySelectorAll('.fab-menu-open');

    for (var i = 0; i < openers.length; i++) {
      openers[i].addEventListener('click', menuOpenClickListener, false);
    }

    var closers = document.querySelectorAll('.fab-menu-close');

    for (var i = 0; i < closers.length; i++) {
      closers[i].addEventListener('click', menuCloseClickListener, false);
    }

    menu.addEventListener('click', menuClickListener, false);
    window.addEventListener('keydown', menuOpenKeyListener, false);

    // METHODS

    function getCategoryToUnfold() {
      var categories_links = document.querySelectorAll('.fab-menu__category > a');
      if (location.pathname === '/') {
        return categories_links[0].parentNode;
      }
      var catToUnfold;
      for (var i = 0; i < categories_links.length; i++) {
        var segment = categories_links[i].getAttribute('href').replace(/\//g, '');
        if (location.pathname.indexOf(segment) === 1) {
          catToUnfold = categories_links[i].parentNode;
          break;
        }
      }
      return catToUnfold;
    }

    function setCategoryActive(catNode) {
      if (catNode !== undefined) {
        $(catNode.querySelector('a')).addClass('fab-menu__a-active');
      }
    }

    function setCategoryItemLinkActive(catNode) {
      var links = catNode.querySelector('.fab-menu__category-items-container').querySelectorAll('a');
      var supposedlyActiveLinks = [];
      for (var i = 0; i < links.length; i++) {
        var fineUriPart = links[i].getAttribute('href').replace(/^(\/{1,})?(.+)(\/{1,})?$/, '$2');
        if (window.location.pathname.indexOf(fineUriPart) === 1) {
          supposedlyActiveLinks.push(links[i]);
        }
      }
      if (supposedlyActiveLinks.length > 1) {
        $(supposedlyActiveLinks[1]).addClass('fab-menu__a-active');
      } else {
        $(supposedlyActiveLinks[0]).addClass('fab-menu__a-active');
      }
    }

    function menuOpenClickListener(e) {
      menuOpen();
    }

    function menuCloseClickListener(e) {
      menuClose();
    }

    function menuCloseKeyListener(e) {
      if (e.keyCode === 27) {
        menuClose();
      }
    }

    function menuOpenKeyListener(e) {
      return;
      if (e.keyCode === 77) {
        menuOpen();
      }
    }

    function menuStateToggle() {
      isMenuOpen() ? menuClose() : menuOpen();
    }

    function menuOpen() {
      $(menu).addClass('fab-menu_open');
      menu.setAttribute('data-fab-menu-state', 'open');
      window.removeEventListener('keydown', menuOpenKeyListener, false);
      window.addEventListener('keydown', menuCloseKeyListener, false);
      var defaultUnfoldedCategory = getCategoryToUnfold();

      setCategoryActive(defaultUnfoldedCategory);

      if (defaultUnfoldedCategory !== undefined && defaultUnfoldedCategory.querySelector('.fab-menu__category-items-container') !== null) {
        setCategoryItemLinkActive(defaultUnfoldedCategory);
      }

      categoryUnfold(defaultUnfoldedCategory);
      setTimeout(function () {
        $(menuStateContainer).addClass('fab-menu__state-container_open');
      }, 20);
    }

    function menuClose() {
      $(menuStateContainer).removeClass('fab-menu__state-container_open');
      setTimeout(function () {
        $(menu).removeClass('fab-menu_open');
        menu.setAttribute('data-fab-menu-state', 'closed');
        window.removeEventListener('keydown', menuCloseKeyListener, false);
        window.addEventListener('keydown', menuOpenKeyListener, false);
        foldAllCategories();
      }, 500);
    }



    function isMenuOpen() {
      return menu.getAttribute('data-menu-fab-state') === 'open' ? true : false;
    }

    function followLink(str) {
      menuClose();
      setTimeout(function () {
        location.assign(str);
      }, 400);
      return;
    }

    function menuClickListener(e) {
      e.preventDefault();
      if ($(e.target.parentNode).hasClass("fab-menu__category")) {
        if (e.target.parentNode.children.length < 2) {
          followLink(e.target.getAttribute('href'));
        }
        toggleCategory(e.target.parentNode);
        // $(e.target.parentNode.children[0]).addClass("fab-menu__a-active");
      }

      if ($(e.target.parentNode).hasClass("fab-menu__category-item")) {
        followLink(e.target.getAttribute('href'));
      }
    }

    function isCategoryUnfolded(catNode) {
      if ($(catNode).hasClass('fab-menu__category_open')) {
        return true;
      }
      return false;
    }

    function categoryFold(catNode) {
      // $(catNode.children[0]).removeClass("fab-menu__a-active");
      $(catNode).removeClass('fab-menu__category_open');
    }

    function categoryUnfold(catNode) {
      if (catNode === undefined) {
        return;
      }
      $(catNode).addClass('fab-menu__category_open');
      // $(catNode.querySelector("a")).addClass("fab-menu__a-active");
    }

    function toggleCategory(catNode) {
      if (isCategoryUnfolded(catNode)) {
        return;
      }
      foldAllCategories();
      setTimeout(function () {
        categoryUnfold(catNode);
      }, 500)
    }

    function foldAllCategories() {
      var cats = menu.querySelectorAll('.fab-menu__category');
      for (var i = 0; i < cats.length; i++) {
        categoryFold(cats[i]);
      }
    }


    function menuTogglerClickListener(e) {
      if (e.target.hasAttribute('data-action') && e.target.getAttribute('data-action') == 'menu-mob-toggler') {
        menuStateToggle();
      }
    }

    // PUBLIC API

    foo.open = menuOpen;
    foo.close = menuClose;

    window.fabMenu = foo;

  })();




// fabMenu.open();

} // end of onload listener




