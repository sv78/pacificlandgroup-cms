;
function fileInputChangeListener(e) {
  filename_holder.innerHTML = e.target.files[0].name;
}

function submitBtnListener(e) {
  form.submit();
}

function submitForm(e) {
  e.preventDefault();
  if (file_input.files[0] === undefined) {
    filename_holder.innerHTML = 'Select a file';
    return;
  } else {
    form.submit();
  }
}

function iframeChanged(evt) {
  form.reset();
  filename_holder.innerHTML = evt.target.contentDocument.body.innerHTML;
}