<?php

function _debug($data, $title = null, $output = true){
	$data_size = '';
	if (is_array($data)) {
		$data_size = "<span class='array-size'> [ " . sizeof($data) . " ]</span>";
	}
	$style = "<style>"
		. "body{background-color: #222; color: #ccc;} "
		. "._debugga{display:block; position: relative; box-sizing:border-box; margin: 0 0 10px; padding: 10px; border-bottom: 1px dashed #555}"
		. "._debugga > .title{display: block; position: relative; box-sizing: border-box; margin: 0; padding: 0; font-family: monospace; font-weight: bold; font-size: 1.2em; color: darkorange; cursor: default;} "
		. "._debugga > .title > .array-size{font-weight: normal; font-size: .8em; color: #6da5c5;} "
		. "._debugga > pre {display: block; position: relative; box-sizing: border-box; margin: 10px 0 0; padding: 0; white-space: break-spaces;}";
	$style .= "</style>";

	$title_block = isset($title) ? "<div class='title'>$title $data_size</div>" : '';
	echo $style;
	echo "<div class='_debugga'>$title_block<pre>";
	print_r($data);
	echo "</pre></div>";
}
