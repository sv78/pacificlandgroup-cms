<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['login'] = '$2y$10$ZC9ticBMe13oQEzTiMTutO6eG9mzrAfBvYat0Sfvlz9C4Pznj9ccW';
$config['password'] = '$2y$10$bH2z6dlHqejZXGU8vI3hQOgP7uFxefiqLLnqcgxQTltoVQhGeRmJy';

$config['ss_projects_img_dir'] = FCPATH . "assets" . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "ss_projects" . DIRECTORY_SEPARATOR;


// $config['customer_requests_delivery_emails'] = 'igor.kotenko@seoacademy.ca, fatbody@yandex.ru, info@pacificgroup.ca';
$config['customer_requests_delivery_emails'] = 'info@pacificlandgroup.ca,oleg@pacificlandgroup.ca,Laura@pacificlandgroup.ca,igor.kotenko@seoacademy.ca,verbenkovoleg@gmail.com';
// $config['customer_requests_delivery_emails'] = 'fatbody@yandex.ru';
$config['customer_requests_from_email'] = 'info@pacificlandgroup.ca';

$config['phone_number'] = '6045011624';
$config['phone_number_view'] = '604-501-1624';
$config['fax_number_view'] = '604-501-1625';

$config['success_stories_header_file'] = APPPATH.'views'.DIRECTORY_SEPARATOR.'pages'.DIRECTORY_SEPARATOR.'success-stories'.DIRECTORY_SEPARATOR.'header_editable.php';

$config['success_stories_page_content_file'] = APPPATH.'views'.DIRECTORY_SEPARATOR.'pages'.DIRECTORY_SEPARATOR.'success-stories'.DIRECTORY_SEPARATOR.'index_editable.php';