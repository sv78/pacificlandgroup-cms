<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Routing */

$route['default_controller'] = 'index_controller';
$route['404_override'] = 'index_controller';
$route['translate_uri_dashes'] = FALSE;


$route['admin'] = 'administrator_controller/index';

$route['admin/file-manager'] = 'administrator_controller/file_manager';

$route['admin/pages'] = 'administrator_controller/pages';
$route['admin/pages/(:num)'] = 'administrator_controller/pages/$1';
$route['admin/pages/create'] = 'administrator_controller/create_page';
$route['admin/pages/create/(:num)'] = 'administrator_controller/create_page/$1';
$route['admin/pages/edit'] = 'administrator_controller/edit_page';
$route['admin/pages/edit/(:num)'] = 'administrator_controller/edit_page/$1';
$route['admin/pages/delete'] = 'administrator_controller/delete_page';
$route['admin/pages/delete/(:num)'] = 'administrator_controller/delete_page/$1';

$route['admin/customer-requests'] = 'administrator_controller/customer_requests';
$route['admin/customer-requests/(:num)'] = 'administrator_controller/customer_request/$1';

$route['admin/success-stories'] = 'administrator_controller/success_stories';
$route['admin/success-stories/(:num)'] = 'administrator_controller/success_stories_category/$1';
$route['admin/success-stories/create-project'] = 'administrator_controller/create_project';
$route['admin/success-stories/edit-project/(:num)'] = 'administrator_controller/edit_project/$1';
$route['admin/success-stories/delete-project/(:num)'] = 'administrator_controller/delete_project/$1';
$route['admin/success-stories/img-upload'] = 'administrator_controller/img_upload';

$route['admin/success-stories/(:any)'] = 'admin/success_stories/$1';

$route['admin/success-stories/ajax/del-img'] = 'administrator_ajax_controller/delete_img';




$route['login'] = 'login_controller/index';
$route['logout'] = 'login_controller/logout';


$route['contact-us'] = 'static_page_controller/contact_us';
$route['customer-request-done'] = 'static_page_controller/customer_request_done';


$route['success-stories'] = 'success_stories_controller/index';
$route['success-stories/(:any)'] = 'success_stories_controller/category/$1';
$route['success-stories/(:any)/(:any)'] = 'success_stories_controller/project/$1/$2';


/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/