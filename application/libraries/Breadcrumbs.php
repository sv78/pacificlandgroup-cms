<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Breadcrumbs {

  //private $ci;
  private $home = "Pacific Land Group";
  //private $home = '<svg viewBox="0 0 24 24"><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/><path d="M0 0h24v24H0z" fill="none"/></svg>';
  //private $home = '<svg viewBox="0 0 24 24"><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/><path d="M0 0h24v24H0z" fill="none"/></svg>';

  private $home_url = "/";
  private $home_title = "Pacific Land Group";
  private $wrapper = "div";
  private $separator = "<span class='breadcrumbs__separator'></span>";
  // private $separator_wrapper = "span";
  private $css_class_for_last_segment = "";
  private $max_segment_length = 54;
  private $after_cut_symbol = "…";

  public function __construct() {
    $this->CI = & get_instance();
    
    $this->CI->load->model('admin/pages_model');
    $db_bc = $this->CI->pages_model->get_page_by_id(1,'breadcrumb')['breadcrumb'];
    $this->home = $db_bc;
    $this->home_title = $db_bc;
  }

  public function create_breadcrumbs($arr) {


    $l = count($arr);
    $counter = 0;

    $f[] = "<{$this->wrapper} class='breadcrumbs__container'>";
    $f[] = "<a href='{$this->home_url}' title='{$this->home_title}'>{$this->home}</a>";
    //$f[] = "<span class='breadcrumbs__slash'>{$this->separator}</span>";
    $f[] = !empty($arr) ? $this->separator : '';

    foreach ($arr as $b) {
      $counter++;
      $f[] = "<a href='";
      $f[] = '/' . $b[0];
      $f[] = "'";
      if ($counter === $l) {
        $f[] = " class='{$this->css_class_for_last_segment}'";
        $f[] = " onclick='javascript: event.preventDefault();'";
      }
      $f[] = " title='{$b[1]}'";
      $f[] = ">";
      $f[] = $this->prepare_breadcrumb_segment($b[1]);
      $f[] = "</a>";

      if ($counter < $l) {
        //$f[] = "<{$this->separator_wrapper} class='breadcrumbs__slash'>{$this->separator}</{$this->separator_wrapper}>";
        $f[] = "{$this->separator}";
      }
    }
    $f[] = "</{$this->wrapper}>";

    return join('', $f);
    // end
  }

  private function prepare_breadcrumb_segment($str) {
    if (strlen($str) < $this->max_segment_length) {
      return $str;
    }
    $cut_str = mb_substr($str, 0, $this->max_segment_length);
    $new_str_arr = explode(" ", $cut_str);
    array_pop($new_str_arr);
    $new_str = implode(' ', $new_str_arr);
    return $new_str . $this->after_cut_symbol;

    //return strlen($str) > $this->max_segment_length ? mb_substr($str, 0, $this->max_segment_length) . $this->after_cut_symbol : $str;
  }

//end of class
}
