<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Utils {

  static function stop($obj = NULL) {
    self::debug($obj);
    die();
  }

  static function debug($obj = NULL, $prefix_str = NULL, $postfix_str = NULL) {
    if (is_array($obj) OR is_object($obj)) {
      echo "<b style='display: block; background-color: #222; color: #19ff00; padding: 1px 5px; margin: 10px 0;'>$prefix_str</b><pre>";
      print_r($obj);
      echo "</pre><b>$postfix_str</b><hr>";
      return;
    }
    echo "<b>$prefix_str</b><pre>$obj</pre><b>$postfix_str</b><hr>";
  }

  static function render_output($str, $show_dev_info = true) {
    echo (config_item('compress_output_code') === TRUE) ? self::compress_output_code($str) : $str;
    if ($show_dev_info && config_item('show_dev_info')) {
      require_once (VIEWPATH . 'partials' . DIRSEP . 'dev_info.php');
    }
  }

  /**
   * ------------------------------------------------------
   * Removes new lines, double spaces from source string.
   * ------------------------------------------------------
   * 
   * @static
   * @param String $str Raw string to compress
   * @return String Compressed html code
   */
  static function compress_output_code($str) {
    $str = str_ireplace(PHP_EOL, "", $str);
    $str = preg_replace('/\s+/', ' ', $str);
    $str = str_ireplace('> <', '><', $str);
    return $str;
  }

  /**
   * ------------------------------------------------------
   * Get filenames as params and returns only template files as an array
   * which has view: array([$filename, $name], ...)
   * ------------------------------------------------------
   */
  static function get_template_names_from_files($filenames) {
    $template_names = [];
    foreach ($filenames as $fn) {
      if (preg_match("/(template_)(.+).php/", $fn)) {
        $name = preg_replace("/(template_)(.+).php/", "$1$2", $fn);
        $template_names[] = $name;
      }
    }
    return $template_names;
  }
  
  // end of class
}
