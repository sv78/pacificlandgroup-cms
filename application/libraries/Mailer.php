<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mailer {
  /*
   * STATIC METHODS
   */

  /**
   * ------------------------------------------------------
   * Saves an email to a file
   * ------------------------------------------------------
   */
  static function save_mail_to_file($email, $sbj, $msg, $filename = NULL) {
    if (config_item('email')['save_to_files'] === FALSE) {
      return;
    }
    $dir = FCPATH . config_item('email')['mail_temp_files_dir'];
    if (!file_exists($dir)) {
      mkdir($dir, 0777);
    }

    $content = "To: <b>$email</b><hr>Subject: <b>$sbj</b><hr>Message: $msg";

    $fn = ($filename === NULL) ? "file" : $filename;
    $file_ext = 'html';

    $file = $dir . DIRSEP . $fn . "." . $file_ext;
    while (file_exists($file)) {
      $fn = increment_string($fn, "_", 1);
      $file = $dir . DIRSEP . $fn . "." . $file_ext;
    }

    $file = fopen($file, "w") or die("Unable to open file.");
    fwrite($file, $content);
    fclose($file);

    //end
  }

  /*
   * PROPERTIES
   */

  protected $ci;

  /*
   * CONSTRUCTOR
   */

  public function __construct() {
    $this->ci = & get_instance();
  }

  /*
   * PUBLIC METHODS
   */

  public function email_customer_request($msg) {
    $mail_config['mailtype'] = 'html';
    $this->ci->load->library('email');
    $this->ci->email->initialize($mail_config);
    $this->ci->email->from(config_item('customer_requests_from_email'), "Pacific Land Group");
    $this->ci->email->to(config_item('customer_requests_delivery_emails'));
    //$this->ci->email->cc('another@another-example.com');
    //$this->ci->email->bcc('them@their-example.com');
    $sbj = 'New Inquiry from PLG Website';
    $this->ci->email->subject($sbj);
    //$msg = 'message text';
    $this->ci->email->message($msg);
    if (!$this->ci->email->send()) {
      return false;
    }
    ###DEV: this is for development use only
    // self::save_mail_to_file($email, $sbj, $msg, 'registration_request');
    return true;

    // end
  }

}
