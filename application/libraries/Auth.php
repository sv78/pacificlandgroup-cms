<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth {

  private $CI;

  public function __construct() {
    $this->CI = & get_instance();

    if (is_cli()) {
      show_404();
    }

    // blocking not authorized users for account zone
    /*
      if ($this->CI->uri->segment(1) === 'account' && $this->is_authorized() === FALSE) {
      //redirect('login', 'location', 301);
      // log_message('error', __FILE__ . " - line " . __LINE__ . " - account unauthorized attempt");
      show_error("The page you requested was not found", 404, '404 Page Not Found');
      }
     * 
     */

    // blocking admin zone for non-admins
    /*
      if ($this->CI->uri->segment(1) === 'admin' && $this->is_admin() === false) {
      //show_404();
      // log_message('error', __FILE__ . " - line " . __LINE__ . " - admin access attempt");
      show_error("The page you requested was not found", 404, '404 Page Not Found');
      }
     * 
     */
  }

  public function is_admin() {
    return (bool) isset($_SESSION['user']['is_admin']) && $_SESSION['user']['is_admin'] === 1 ? true : false;
  }

//end of class
}
