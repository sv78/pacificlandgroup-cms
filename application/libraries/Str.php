<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Str {

  /**
   * 
   * @param String $str Raw string to encode
   * @return String Plain string without tags and spaces on sides. Quotes are preceded with slashes
   */
  public static function encode_plain_string($str) {
    return (string) htmlentities(addslashes(strip_tags(trim($str))));
  }

  public static function create_customer_request_mail_message($data) {
    $msg = [];
    $msg[] = "<h3>Customer's Information</h3>";
    $msg[] = "<div>";
    $msg[] = "<b>Name:</b> " . $data['name'] . "<br>";
    $msg[] = !empty($data['company']) ? "<b>Company:</b> " . $data['company'] . "<br>" : '';
    $msg[] = "<b>Email:</b> " . $data['email'] . "<br>";
    $msg[] = "<b>Phone:</b> " . $data['phone'] . "<br>";
    $msg[] = "<b>Message:</b><br><pre>" . $data['message'] . "</pre><br>";
    $interest_types = Str::get_the_type($data['type']);
    $msg[] = "<b>Area of interests:</b> " . (!empty($interest_types) ? $interest_types : '') . "<br>";
    $msg[] = "<b>Preferred form of communication:</b> " . Str::get_the_foc($data['foc']) . "<br>";
    $msg[] = "</div>";
    $msg[] = "<div style='margin-top: 30px; padding-left: 18px;'><small>Webmaster,<br>Pacific Land Group.</small></div>";
    $msg[] = "<div>";
    $msg[] = "<img src='" . site_url("/assets/themes/default/img/logo_email.png") . "' style='display:block; width:200px; margin-top: 20px;'>";
    $msg[] = "</div>";

    return implode('', $msg);
  }

  public static function get_the_type($str) {
    $arr = explode(',', $str);
    $real_types = array(
        'lup' => 'Land Use Planning',
        'es' => 'Environmental Services',
        'ersc' => 'Erosion And Sediment Control',
        'ce' => 'Civil Engineering',
        'ls' => 'Land Survey',
    );
    $new_arr = [];
    foreach ($arr as $key) {
      if (key_exists($key, $real_types)) {
        $new_arr[] = $real_types[$key];
      }
    }
    if (count($new_arr) < 1) {
      return NULL;
    }
    return implode(', ', $new_arr);
  }

  public static function get_the_foc($str) {
    $arr = explode(',', $str);
    $real_types = array(
        'phone' => 'Phone',
        'email' => 'Email',
    );
    $new_arr = [];
    foreach ($arr as $key) {
      if (key_exists($key, $real_types)) {

        $new_arr[] = $real_types[$key];
      }
    }
    if (count($new_arr) < 1) {
      return "not specified";
    }
    return implode(', ', $new_arr);
  }

  /**
   * 
   * @param type $str Processed string to decode
   * @return String String widthout preceded slashes before quotes
   */
  public static function decode_plain_string($str) {
    return (string) stripslashes($str);
  }

  public static function encode_html_string($str) {
    return (string) htmlentities(addslashes(trim($str)));
  }

  public static function decode_html_string($str) {
    return (string) html_entity_decode(stripslashes($str));
  }

  public static function create_slug_from_string($str = NULL) {
    $patterns[0] = '/[^A-Za-z0-9]/i';
    $patterns[1] = '/-+/i';
    $patterns[2] = '/^(-){1}/i';
    $patterns[3] = '/(-){1}$/i';
    $replacements[0] = '-';
    $replacements[1] = '-';
    $replacements[2] = '';
    $replacements[3] = '';
    $res = preg_replace($patterns, $replacements, $str);
    $res = mb_strtolower($res);
    return $res;
  }

  public static function prepare_for_set_type($arr) {
    return implode(',', $arr);
  }

  // end of class
}
