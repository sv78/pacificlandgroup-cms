<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// class
class MY_Form_validation extends CI_Form_validation {

  public function __construct($rules = array()) {
    parent::__construct($rules);
  }
  
  public function my_checkbox($str) {
    return (bool) (strtolower($str) === 'on') ? TRUE : FALSE;
  }

  /* public function my_submit($str) {
    return (bool) (strtolower($str) === config_item('_input_submit_value')) ? TRUE : FALSE;
    } */

  public function my_single_word($str) {
    return (bool) !preg_match("/[^A-Za-z]/i", $str);
  }

  public function my_position($str) {
    return (bool) !preg_match("/[^0-9A-Za-z\ \-\_\!\@\#\&\(\)\.]/i", $str);
  }

  public function my_password_chars($str) {
    return (bool) !preg_match("/[^0-9A-Za-z\-\_\!\@\#\$\%\^\&\*\(\)\~\+\?\.\[\]\{\}]/i", $str);
  }
  
  public function my_unique_page_slug($str, $page_id) {
    $this->CI->load->model('admin/pages_model');
    $this->CI->db->distinct();
    $this->CI->db->select('id');
    $this->CI->db->where('slug',$str);
    $query = $this->CI->db->get('pages');
    $found_id = $query->row_array()['id'];
    if (empty($found_id) OR $found_id == $page_id) {
      return true;
    }
    return false;
  }
  
  public function my_differs($parent_page_id, $page_id) {
    return $parent_page_id == $page_id ? false : true;
  }

  /*public function my_password_repeat($str) {
    return (bool) ($str === filter_input(INPUT_POST, 'password'));
  }*/

  /*public function my_is_registered_user($eml = NULL) {
    $this->CI->load->model('auth_model');
    return $this->CI->auth_model->get_user_by_email($eml) ? (bool) TRUE : (bool) FALSE;
  }*/

  public function my_forbidden_character_combinations($str) {
    // regexp patterns array
    $fcc = array(
        '\d *\= *\d', // 1=1 or 1   = 1 or etc
        '[\"\'] *\= *[\"\']', // "=" or '=' or etc
        '\<\?', // <?php or <?=
        '\?\>',
        '\<script\>',
        '\<\/script\>',
        '\'\'',
        '\"\"',
        '\"\'',
        '\'\"',
        //'^[\"\']true',
        //'\ +[\"\']*true',
        //'^[\"\']false',
        //'\ +[\"\']*false',
        //'^[\"\']null',
        //'\ +[\"\']*null',
        ' or ',
        ' and ',
        'drop ',
        'delete ',
        'update ',
        'select ',
        'insert ',
        'table ',
        '\$_SERVER',
        '\$_POST',
        '\$_GET',
        '\$_REQUEST',
        '\$_SESSION',
    );
    foreach ($fcc as $combination) {
      if (preg_match('/' . $combination . '/i', $str)) {
        return (bool) FALSE;
      }
    }
    return TRUE;
  }

}

// end of class