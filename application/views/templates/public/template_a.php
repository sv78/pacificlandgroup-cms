<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('chunks/head');
?>
<div class="top-nav-bar">
  <?php
  $this->load->view('chunks/tnb-top');
  $this->load->view('chunks/tnb-menu');
  $this->load->view('chunks/breadcrumbs');
  ?>
</div>
<div class="content">
  <?php echo empty($content) ? "<p style='color: red;'>warning: no content given on this page..." : $content; ?>
</div>

<?php
$this->load->view('chunks/footer');
$this->load->view('chunks/fab-menu');
$this->load->view('chunks/end');
