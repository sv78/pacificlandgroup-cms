<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('chunks/head');
?>
<div class="top-nav-bar">
  <?php
  $this->load->view('chunks/tnb-top');
  $this->load->view('chunks/tnb-menu');
  $this->load->view('chunks/breadcrumbs');
  ?>
</div>

<div class="bg-cover-top-full bg_env-glossary top-img-bordered-blue">

  <div class="row-section row-section_dark bg-color_transparent">

    <div class="row-section__txt row-section__txt_100 row-section__txt_right">
      <div class="row-section__content-container">

        <h1 class="h1-very-big"><?php echo empty($header) ? '' : $header; ?></h1>
        <?php
        if (!empty($header_description)):
          ?>
          <hr class="short-line">
          <?php
        endif;
        ?>
        <p><?php echo empty($header_description) ? '' : $header_description; ?></p>

      </div>
    </div>

    <!-- <div class="row-section__bg row-section__bg_50 row-section__bg_left"></div> -->

  </div>

</div>


<div class="row-section bg-color_white">

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">

      <div class="content">
        <?= empty($content) ? "<p style='color: red;'>warning: no content given on this page..." : $content; ?>
      </div>

    </div>
  </div>

  <div class="bg-fixed row-section__bg row-section__bg_33 row-section__bg_left bg_env-glossary-static"></div>

</div>

<?php
$this->load->view('chunks/footer');
$this->load->view('chunks/fab-menu');
$this->load->view('chunks/end');
