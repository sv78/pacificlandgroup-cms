<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('chunks/head');
?>
<div class="top-nav-bar">
  <?php
  $this->load->view('chunks/tnb-top');
  $this->load->view('chunks/tnb-menu');
  $this->load->view('chunks/breadcrumbs');
  ?>
</div>

<div class="bg-cover-top-full bg_env-glossary top-img-bordered-blue">

  <div class="row-section row-section_dark bg-color_transparent">

    <div class="row-section__txt row-section__txt_100 row-section__txt_right">
      <div class="row-section__content-container">

        <h1 class="h1-very-big">Pacific Land Use Success Project</h1>
        <hr class="short-line">
        <p>"I am writing to formally record our appreciation for the very good work on the land use analysis. It was a complicated issue requiring considerable professional expertise."<br><small><i>- Barry Moss, President<br>&nbsp;&nbsp;Mayfair Resources Inc.</i></small></p>

      </div>
    </div>

    <!-- <div class="row-section__bg row-section__bg_50 row-section__bg_left"></div> -->

  </div>

</div>


<div class="row-section bg-color_white">

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">

      <div class="content">
        <h2 class="h-simple"><?= Str::decode_plain_string($project['name']); ?></h2>
        <hr class="short-line">
        <p><?= Str::decode_plain_string($project['lead']); ?></p>

        <?php
        $img_file_name = $project['id'] . "." . $project['img_extension'];
        $img_file_link = "/assets/uploads/ss_projects/" . $img_file_name;
        $img_file = config_item('ss_projects_img_dir') . $img_file_name;
        //echo $img_file;
        if (file_exists($img_file)) :
          ?>
          <img class="success-stories-project-main-img" alt="<?= Str::decode_plain_string($project['title']); ?>" src="<?= $img_file_link; ?>">
          <?php
        endif;
        ?>


        <?= $content; ?>
      </div>

    </div>
  </div>

  <div class="bg-fixed row-section__bg row-section__bg_33 row-section__bg_left bg_env-glossary-static"></div>

</div>

<?php
$this->load->view('chunks/footer');
$this->load->view('chunks/fab-menu');
$this->load->view('chunks/end');
