<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<p>Since our founding in 1996, Pacific Land Group has worked with many municipalities and communities across the Lower Mainland and Fraser Valley bringing an unparalleled depth of experience and delivering cohesive solutions to complex land planning, development and environmental issues.</p>


<!-- COMMERCIAL & INDUSTRIAL DEVELOPMENT -->

<h2 class="h-simple">Commercial &&nbsp;Industrial Development</h2>
<hr class="short-line">
<p>Our experience with commercial and industrial development encompasses a wide-range of projects including: warehouse uses, business parks, food distribution facilities, retail and commercial plazas, and gas stations.</p>

{{commercial}}



<!-- RESIDENTIAL DEVELOPMENT -->

<h2 class="h-simple">Residential Development</h2>
<hr class="short-line">
<p>Our work on residential projects includes single-family subdivisions, and multi-family developments; with knowledge on how to accommodate for challenging topographic conditions.</p>
<p>Numerous Townhouse Developments.</p>

{{residential_development}}



<!-- AGRICULTURAL LAND -->

<h2 class="h-simple">Agricultural Land</h2>
<hr class="short-line">
<p>Our experience with development on agricultural land includes: Transportation Corridor Applications, ALR exclusions, and non-farm use projects and subdivisions within the ALR.</p>

{{agricultural_land}}



<!-- EXPROPRIATION IMPACT -->

<h2 class="h-simple">Expropriation Impact Assessment / Highest & Best Land Use Analysis</h2>
<hr class="short-line">
<p>PLG has been retained as a lead consultant in the performance of highest and best use analyses, in order to determine the optimal development potential for unique and/ or challenging properties. We are also experienced in conducting development and financial impact assessments of partial land takings for public uses, such as right-of-ways.</p>

{{expropriation_impact}}




<!-- SUSTAINABILITY -->

<h2 class="h-simple">Sustainability</h2>
<hr class="short-line">
<p>PLG strives to incorporate sustainable design and technology into our diverse range of land development services. We have successfully completed a number of commercial and industrial buildings that meet LEED™, Built Green™ specifications.</p>

{{sustainability}}
