<h1 class="h1-very-big">Pacific Land Group Success Stories</h1>

<hr class="short-line" />
<p>&quot;I am writing to&nbsp;formally&nbsp;record our appreciation&nbsp;for the very good work on the land use analysis. It was a complicated issue requiring considerable professional expertise.&quot;<br />
<small><i>- Barry Moss, President<br />
&nbsp;&nbsp;Mayfair Resources Inc.</i></small></p>