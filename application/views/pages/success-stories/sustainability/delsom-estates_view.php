<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>
<h3>Client</h3>
<ul>
  <li>Delsom Estate Ltd.</li>
</ul>

<h3>Key Features</h3>
<ul>
  <li>Land Area: 107 acres (43 hectares)</li>
  <li>Probate and Development Concept Planning</li>
  <li>Public consultation</li>
  <li>Rezone the subject property from Residential 1 (R-1) Zone to Comprehensive Development (CD) Zone</li>
  <li>No. of Units: 850</li>
  <li>Zoning includes use for commercial, multi-family, single family, and park and open spaces</li>
  <li>Incorporation of sustainability features throughout the entire development</li>
</ul>