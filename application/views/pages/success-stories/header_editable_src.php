<h1 class="h1-very-big">Pacific Land Group Success Stories</h1>
<hr class="short-line">
<p>"I am writing to formally record our appreciation for the very good work on the land use analysis. It was a complicated issue requiring considerable professional expertise."<br><small><i>- Barry Moss, President<br>&nbsp;&nbsp;Mayfair Resources Inc.</i></small></p>
