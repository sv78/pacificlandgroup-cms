<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Thank You!</h1>
<hr class="short-line">
<p>Your request was successfully sent. We will contact you as soon as possible.<br>Thank you for your attention.</p>

<p>
  <small>
    <a href="/" class="color_blue">Go to main page</a>
  </small> |
  <small>
    <a href="/contact-us" class="color_blue">Contact Us</a>
  </small>
</p>


<a class="button-like" href="/contact-us">Back</a>
