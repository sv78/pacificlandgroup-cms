<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Logged In</h1>
<hr class="short-line">
<p>You are already logged in. No more authentification is required.</p>
<a href="/" class="button-like">Site</a>
<a href="/admin" class="button-like">Administrator Zone</a>
<a href="/logout" class="button-like">Logout</a>

