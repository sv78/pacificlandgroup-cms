<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Pacific Land Group</h1>
<hr class="short-line">
<p class="color_black">
  <b>Address: </b>212 - 12992 76 Avenue, Surrey, British Columbia V3W 2V6<br>
  <b>Phone: </b><?= $this->config->item('phone_number_view'); ?><br>
  <b>Fax: </b><?= $this->config->item('fax_number_view'); ?><br>
</p>

<p>Ask an expert how we can help. Pacific Land Group is here to provide you with more information, answer any question you may have and create an effective solution for your challenging project.</p>

<h2 class="color_a">Contact Us</h2>

<?php
if (!empty($login_error_msg)) :
  ?>
  <div style="color: red;"><?= $login_error_msg; ?></div>
  <?php
endif;


/*
 * specifying form attributes
 */
$form_attributes = array(
    'class' => 'standard-form',
    'id' => 'contacts_form',
    'style' => 'max-width: 520px;',
    // 'onsubmit' => 'validateThisForm(event)',
    'method' => 'post',
    'enctype' => 'multipart/form-data',
    'accept-charset' => 'utf-8'
);

/*
 * creating form with specified attributes
 */
echo form_open('', $form_attributes);
?>

<div class="standard-form__field-wrapper">
  <label for="name" class="_bold">Name *</label>
  <input type="text" name="name" value="<?= set_value('name') ?>" title="Please enter your name" id="name" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('name'); ?></span>
</div>

<div class="standard-form__field-wrapper">
  <label for="company" class="_bold">Company</label>
  <input type="text" name="company" value="<?= set_value('company') ?>" title="Please enter your company" id="company" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('company'); ?></span>
</div>

<div class="standard-form__field-wrapper">
  <label for="email" class="_bold">Email *</label>
  <input type="text" name="email" value="<?= set_value('email') ?>" title="Please enter your email" id="email" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('email'); ?></span>
</div>

<div class="standard-form__field-wrapper">
  <label for="phone" class="_bold">Phone *</label>
  <input type="text" name="phone" value="<?= set_value('phone') ?>" title="Please enter your phone" id="phone" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('phone'); ?></span>
</div>

<div class="standard-form__field-wrapper">
  <label for="message" class="_bold">Message *</label>
  <textarea name="message" title="Please enter your message" id="message" class="standard-form__field" autocomplete="off"><?= set_value('message') ?></textarea>
  <span class="standard-form__field-error-msg"><?= form_error('message'); ?></span>
</div>

<div class="standard-form__field-wrapper">
  <label class="_bold">Please help us to better direct your inquiry by indicating the area of your interests:</label>
  <!-- <input type="hidden" name="types[]" value="empty"> -->
  <input type="checkbox" name="types[]" value="lup" id="types1" <?= set_checkbox('types[]', 'lup'); ?>> <label class="label-radio" for="types1">Land Use Planning</label><br>
  <input type="checkbox" name="types[]" value="es" id="types2" <?= set_checkbox('types[]', 'es'); ?>> <label class="label-radio" for="types2">Environmental Services</label><br>
  <input type="checkbox" name="types[]" value="ersc" id="types3" <?= set_checkbox('types[]', 'ersc'); ?>> <label class="label-radio" for="types3">Erosion And Sediment Control</label><br>
  <input type="checkbox" name="types[]" value="ce" id="types4" <?= set_checkbox('types[]', 'ce'); ?>> <label class="label-radio" for="types4">Civil Engineering</label><br>
  <input type="checkbox" name="types[]" value="ls" id="types5" <?= set_checkbox('types[]', 'ls'); ?>> <label class="label-radio" for="types5">Land Survey</label><br>
  <input type="checkbox" name="dummy_type" value="dummy" id="dummy_type" <?= set_checkbox('dummy_type', 'dummy'); ?>> <label class="label-radio" for="dummy_type">Other</label><br>
  <span class="standard-form__field-error-msg"><?= form_error('types[]'); ?></span>
</div>


<div id="prefered_form_of_communication_group" class="standard-form__field-wrapper">
  <label class="_bold">Preferred form of communication</label>
  <!-- <input type="hidden" name="foc[]" value="empty"> -->
  <input type="checkbox" name="foc[]" value="phone" id="foc1" <?= set_checkbox('foc[]', 'phone', true); ?>> <label class="label-radio" for="foc1">Phone</label><br>
  <input type="checkbox" name="foc[]" value="email" id="foc2" <?= set_checkbox('foc[]', 'email'); ?>> <label class="label-radio" for="foc2">Email</label><br>
  <span class="standard-form__field-error-msg"><?= form_error('foc[]'); ?></span>
</div>


<div class="standard-form__field-wrapper">
  <input type="submit" value="Send" class="button-like">
</div>

<p class="_sm">Fields marked with asterisks (*) are required to fill.</p>

<?php
form_close();
?>

<script>

  // Prefered form of comunication checkboxes handler
  (function () {
    var g = document.querySelector('#prefered_form_of_communication_group');
    var checkboxes = g.querySelectorAll('input[type=checkbox]');
    for (var i = 0; i < checkboxes.length; i++) {
      checkboxes[i].addEventListener('change', checkboxChangeListener, false);
    }

    function checkboxChangeListener(e) {
      var isAnyChecked = false;
      for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
          isAnyChecked = true;
          break;
        }
      }
      if (!isAnyChecked) {
        e.target.checked = true;
      }
    }

  })();
</script>