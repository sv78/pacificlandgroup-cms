<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Engineering Design & Construction -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_civil-engineering_construction-cost-estimate">
  </div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h1 class="h-simple">Development Feasibility Study &&nbsp;Construction Cost Estimate</h1>
      <hr class="short-line">

      <ul class="ul-extra color_blue _bold">
        <li>Preliminary Site planning;</li>
        <li>Overall development planning;</li>
        <li>Lot layouts and site layouts;</li>
        <li>Preliminary servicing design; and</li>
        <li>Preliminary opinion of development costs.</li>
      </ul>

      <?= $staff_card; ?>

    </div>
  </div>

</div>
