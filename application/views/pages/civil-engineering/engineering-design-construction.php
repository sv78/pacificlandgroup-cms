<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Engineering Design & Construction -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_civil-engineering_engineering-design-construction">
  </div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h1 class="h-simple">Engineering Design Process &&nbsp;Construction Design</h1>
      <hr class="short-line">

      <div class="row-flex">
        <ul class="ul-extra color_blue _bold">
          <li>Onsite and offsite works;</li>
          <li>Roadworks and underground services;</li>
          <li>Site grading and lot grading;</li>
          <li>Waterworks, sanitary sewer and storm sewer services;</li>
          <li>Drainage studies, storm water management, and drainage works;</li>
          <li>Storm water detention/retention systems;</li>
        </ul>

        <ul class="ul-extra color_blue _bold">
          <li>Erosion and sediment control works;</li>
          <li>Sustainable drainage works including rain gardens, bioswales and storm water infiltration systems;</li>
          <li>Engineering design including computer modeling;</li>
          <li>Preparation of working drawings using computer software such as AutoCAD and Civil 3D;</li>
          <li>Directing and co-ordinating with other consultants involved with the project; and</li>
          <li>Submissions to and processing approvals through regulatory authorities.</li>
        </ul>
      </div>

      <?= $staff_card; ?>

    </div>
  </div>

</div>



<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h4 class="text-align-center">Typical Engineering Department Approval Process:</h4>
      <img class="img-full-width" src="/assets/themes/default/img/pages/civil-engineering/engineering-design-construction/approval-process.jpg" alt="Typical Engineering Department Approval Process">

      <p class="offset-top-20">For more information please download <a href="https://www.surrey.ca/files/Engineering_Land_Development_Customers_Manual2.docx.docx" class="color_blue" target="_blank">Engineering Land Development User Customer Manual</a> or visit City of Surry <a href="https://www.surrey.ca/city-government/642.aspx" class="color_blue" target="_blank">Land Development Division</a> website.</p>


    </div>
  </div>

</div>

