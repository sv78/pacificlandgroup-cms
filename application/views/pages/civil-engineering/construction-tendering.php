<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Construction Tendering -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_civil-engineering_construction-tendering">
  </div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h1 class="h-simple">Construction Tendering</h1>
      <hr class="short-line">

      <ul class="ul-extra color_blue _bold">
        <li>Quantity takeoffs and construction cost estimates;</li>
        <li>Construction tendering, tender evaluations and contract documents;</li>
        <li>Management, supervision and inspection of construction;</li>
        <li>Design and Monitoring of erosion and sediment control works;</li>
        <li>Testing and reporting of erosion and sediment control;</li>
        <li>Contract administration; and</li>
        <li>As-built drawing preparation.</li>
      </ul>

      <?= $staff_card; ?>

    </div>
  </div>

</div>
