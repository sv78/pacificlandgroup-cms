<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<!-- Civil Engineering -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_civil-engineering"></div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h1 class="h-simple">Civil Engineering Services – Pacific Land Group</h1>
      <hr class="short-line">
      <p>Our civil engineering group offers a broad range of civil engineering services for municipal land development and building projects. Civil engineering services include but are not limited to providing help and support in tendering process, construction cost estimate to municipal engineering design & planning, construction design, working drawing, construction project management, construction inspection & supervision.</p>

      <?= $staff_card; ?>

    </div>
  </div>

</div>



<div class="row-section row-section_dark bg-color_noble-blue">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Engineering Project Management</h2>
      <hr class="short-line">

      <p>Our engineering group offers project management services to facilitate various types of development projects, including: industrial, commercial/high-tech, residential and institutional.</p>

      <ul class="ul-normal color_blue">
        <li>Preliminary engineering planning</li>
        <li>Preliminary servicing design</li>
        <li>Coordination of required sub consultants such as, mechanical, structural and architectural</li>
        <li>Submissions to and processing approvals through regulatory authorities</li>
      </ul>

    </div>
  </div>

</div>


<?= $tile_menu; ?>


<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Engineering Design &&nbsp;Construction</h2>
      <hr class="short-line">

      <p>Our detailed engineering design and construction works include: preparation of site layouts for an array of project types, site and lot grading plans, on-site and off-site servicing works, and road works.  Our team also provides construction supervision, and inspection services to ensure all works being done are up to standards.</p>

    </div>
  </div>

</div>



<div class="row-section row-section_dark bg-color_noble-blue">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Construction Tendering</h2>
      <hr class="short-line">
      <p>Our team of Professional Engineers and drafters facilitate and manage various types of development projects, including: industrial, commercial/high-tech, residential, and institutional. The management process includes consultation with municipal staff, to achieve the most efficient, and cost-effective option for our clients. We have successfully completed projects of various magnitudes, in numerous municipalities throughout the Province of BC.</p>

    </div>
  </div>

</div>




<div class="row-section row-section_dark bg-color_darky">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Feasibility Studies &&nbsp;Construction Cost Estimates</h2>
      <hr class="short-line">
      <p>Feasibility studies, including construction cost estimates are typically provided prior to the start of a project. This report identifies the potential costs (including construction costs, municipal charges, and consulting fees) of a project, and presents the most feasible options. Based on our experience and knowledge of municipal standards and regulations, our feasibility studies present an accurate description of the works, and fees required to complete a project.</p>

    </div>
  </div>

</div>
