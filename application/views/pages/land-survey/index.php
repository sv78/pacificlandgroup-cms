<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Land Survey -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_land-survey">
  </div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h1 class="h-simple">Land Survey</h1>
      <hr class="short-line">
      <p>Our surveying group is led by a qualified British Columbia Land Surveyor (BCLS), and our survey team has over 105 years of combined professional land survey experience in the Lower Mainland and British Columbia.</p>

      <?= $staff_card; ?>

    </div>
  </div>

</div>



<div class="row-section row-section_dark bg-color_noble-blue">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Topographic Survey</h2>
      <hr class="short-line">
      <p>Topographic surveys are essential in determining the existing natural features on site, and are used to aid the planning and development process. On-site gathering of information, through the use of modern and up-to-date surveying tools and computers, ensure efficiency and accuracy.  Our topographic survey services include: field investigations of natural features (including watercourse and top-of-bank locations, tree survey, elevations, etc.), establishing of natural boundaries (eg. watercourse), inventory of existing buildings/building foundations on site, cross-sectioning of intersections and roads, and consultation & management services.</p>

    </div>
  </div>

</div>




<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Legal Survey</h2>
      <hr class="short-line">
      <p>Our survey team is experienced in the preparation and filing of a variety of plans (Right-of-way, covenant, easement, road dedication, subdivision, consolidation and reference plans, etc.), relating to land development, at the Land Title Office. Other survey services include: buildings floor area ratios, common and bare land strata, building location surveys, environmental features and tree surveys.</p>

    </div>
  </div>
</div>