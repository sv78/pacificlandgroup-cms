<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Bill 15: Proposed Amendments to the Agricultural Land Commission Act</h1>
<p><a href="https://www.leg.bc.ca/parliamentary-business/legislation-debates-proceedings/41st-parliament/4th-session/bills/first-reading/gov15-1" class="color_blue" target="_blank">Bill 15, the Agricultural Land Commission Amendment Act 2019</a>, was presented for first reading at the BC Legislature and continues to be a subject of provincial debate. The Bill proposes administrative changes to the ALC decision making structure. Changes to how (and who) can submit an exclusion application are also notable.</p>

<h4>Shift from “Panel Regions” to “Administrative Regions”</h4>


<p>Bill 15 proposes the removal of regional panels and replaces them with administrative panels. By replacing the regional panels with administrative regions, decision making will is essentially centralized to one entity with regional representation to make up an 11-member commission. At least one member, but no more than three members, can be appointed from each of the six regions.</p>

<p>The Chair of the ALC may also establish panels consisting of two or more members of the commission. The Chair can consider the expertise of commission members to establish expert panels to assess different applications to the ALC (e.g., inclusion, exclusion, non-adhering residential uses).</p>

<h4>Changes to Exclusion Applications in the ALR</h4>

<p>Another significant change proposed in Bill 15 would require Exclusion applications in the ALR to be solely submitted by the area’s local government or First Nation government rather than the individual landowners. The rationale behind this approach is that local governments or First Nation governments can integrate exclusion applications into long range planning projects and policies.</p>

<p>Applications to remove land from the ALR can also come from the ALC itself.</p>


<h4>Additional purposes of the commission</h4>

<p>In addition to the ALC’s purpose to preserve agricultural land, encourage farming of agricultural land, and to encourage local governments and First Nations to enable and accommodate farm uses, Bill 15 introduces two additional purposes to the commission;</p>

<ul>
  <li>“The Commission, to fulfill its purposes under subsection (1), must give priority to protecting and enhancing all of the following in exercising its powers and performing its duties under this Act:

    <ul>
      <br>
      <li>The size, integrity and continuity of the land base of the agricultural land reserve;</li>
      <li>The use of the agricultural land reserve for farm use.</li>
    </ul>

  </li>
</ul>

<p>Bill 15 is currently under debate by Members of the Legislative Assembly. If the Bill receives second reading, it will proceed to the Committee and Report stages prior to third reading and Royal Assent.</p>

<p>Pacific Land Group is actively monitoring the progress of Bill 15 and local government responses to Bill 52. <span class="color_dark">Please feel free to contact us at <a class="color_blue" href="mailto:info@pacificlandgroup.ca">info@pacificlandgroup.ca</a> or <tel class="_bold">604-501-1624</tel> for any inquiries.</span></p>
