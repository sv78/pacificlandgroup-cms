<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Bill 52: Changes to the ALC Act and Regulations</h1>
<h4 class="_italic">Residences in the ALR and the Placement of Fill</h4>

<p>On February 22, 2019, B.C.'s Lieutenant Governor in Council approved the Agricultural Land Reserve (ALR) Regulations which brings into force changes to the <i>Agricultural Land Commission Act</i> under Bill 52. The main changes to these pieces of legislation pertain to limits on house sizes of residential uses and the placement of fill for properties in the ALR.</p>

<p>Through Bill 52, <strong>the principal residence of a parcel in the ALR is limited to 500 m2 (5,400 ft<sup>2</sup>) or less. Only one residence per parcel is permitted</strong>. Any residential use proposing more than one residential use and/or a floor area greater than 500 m<sup>2</sup> is now required to submit an application for a "non-adhering residential use".</p>

<p>While the <i>ALC Act</i> and Regulations have previously contained provisions facilitating the construction of additional dwellings for farm help, manufactured homes for immediate family members, and accommodation above an existing farm building, these provisions have been repealed. Any request for these uses also require an application for a “non-adhering residential use”.</p>

<p>Non-adhering residential use applications are first reviewed by the local government and authorization must be given by the Council or Board for the application to proceed to the ALC.</p>

<h4>Grandfathering Provisions</h4>

<p class="_underline">Completion Residential Construction / Alterations Initiated by February 22, 2019</p>

<p>The ALR Regulation provides some grandfathering allowances for a new residential structure over 500 m<sup>2</sup> <span class="_underline">OR</span> alterations to an existing structure that causes the total floor area to be greater than 500&nbsp;m<sup>2</sup>.</p>

<p>The current interpretation is that all building permits and authorizations must be granted before February 22, 2019 for any residential structures or alterations over 500 m<sup>2</sup>. Construction of the foundation of the new residential structure must substantially begin on or before November 5, 2019.</p>

<p>Further, the ALR provisions expect the construction or alteration to be carried out without interruption, other than work stoppages considered reasonable in the building industry.</p>

<p class="_underline">New Alterations Initiated After February 22, 2019</p>

<p>The ALR Regulations further permit alterations initiated after February 22, 2019 to be undertaken without application to the ALC in some instances. The alteration is permitted as long as it does not lead to further contravention of the <i>ALC Act</i> or regulations. Some examples include:</p>

<ul>
  <li>
    You currently have a second residence on a parcel in ALR that is greater than 500&nbsp;m<sup>2</sup> and would like to make some alterations to the structure.
    <br>
    <br>
    You may make alterations to the second residence <span class="_underline">as long as it does not increase the total floor area of what was existing</span> (i.e., the alteration does not worsen any pre-existing contravention of the ALC Act or Regulations).
  </li>
  <br>
  <li>
    You currently have one principal residence on a parcel in the ALR that is less than 500&nbsp;m<sup>2</sup>.
    <br>
    <br>
    You may alter or construct a new residence as long as it does not exceed 500&nbsp;m<sup>2</sup>, or the prescribed floor area in the municipal zoning bylaw. For example, the City of Delta and the City of Richmond currently have zoning bylaws that limit the total floor area of the principal residence to less than 500&nbsp;m<sup>2</sup>.
  </li>
</ul>

<p class="_underline">Replacing a Residence</p>

<ul>
  <li>For ALR parcels with only one residence, the new residence must not be more than 500&nbsp;m<sup>2</sup>.</li>
  <br>
  <li>
    If the parcel currently has more than one residence, an application to the Commission is required if replacement is proposed for:
    <ul>
      <br>
      <li>
        Residences which pre-date the ALR (residences constructed prior to December 21, 1972)
      </li>
      <li>
        Residences approved by local government without application to the Commission (under the former section 18 of the <i>ALC Act</i>)
      </li>
      <li>
        Residences constructed in contravention of local government zoning bylaws, the <i>ALC Act</i>, or Regulations
      </li>
    </ul>
  </li>
</ul>

<p>If there is prior approval from the ALC (e.g., from a non-farm use), replacement of the residence is dependent on the terms of the approval and may require confirmation from the ALC.</p>

<h4>Soil or Fill for Residential Construction</h4>

<p>The ALR Regulations has now limited the area from soil is removed or on which soil is placed to 1,000 m<sup>2</sup> or less. If the area is located in a floodplain, the resulting elevation level must be consistent with applicable local government requirements for flood protection.</p>

<p>Any soil removal or placement exceeding 1,000 m<sup>2</sup> in connection with other residential uses is not permitted. <strong>A person who intends to place fill or remove soil for specified farm uses or specified non-farm uses must file a Notice of Intent with the ALC at least 60 days before the activity</strong>.</p>

<p>The ALR Regulation has also limited the placement or removal of fill for a structure for farm use to 1,000 m<sup>2</sup>.</p>

<p class="_bold">For further information:</p>

<p>
  <a class="color_blue" href="https://www.alc.gov.bc.ca/assets/alc/assets/legislation-and-regulation/information-bulletins/information_bulletin_5_residences_in_the_alr.pdf">ALC Information Bulletin 05</a>
  <br>
  <a class="color_blue" href="https://www.alc.gov.bc.ca/alc/content/home">ALC - Agricultural Land Commission</a>
</p>

<h4>PLG’s Expertise</h4>

<p>The new changes through Bill 52 and the ALR Regulations have changed considerably. PLG has a team of planners who can provide you with information pertaining to your property and help you make a decision on your next steps.</p>

<p class="color_dark">Please feel free to contact us at <a class="color_blue" href="mailto:info@pacificlandgroup.ca">info@pacificlandgroup.ca</a> or <tel class="_bold">604-501-1624</tel> for further information.</p>