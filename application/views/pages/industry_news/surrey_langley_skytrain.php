<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Surrey–Langley SkyTrain (“SLS”) Project and the upcoming public engagement in April 2019</h1>

<p>A <a href="https://www.surrey.ca/bylawsandcouncillibrary/CR_2019-R049.pdf" class="color_blue" title="CR_2019-R049.pdf" target="_blank">report</a> outlining the upcoming public engagement on the Surrey-Langley SkyTrain (“SLS”) Project was delivered to City of Surrey Council on April 1, 2019.  City staff will now begin work on a planning areas review with TransLink to confirm land use planning along 104 Avenue and King George Boulevard. Staff will review population and employment projections, and participate in the public engagement process together with TransLink.</p>

<p>Councillor Brenda Locke proposed a motion to place an interim moratorium on development along the SLS route until Council has had time to review and approve a new land use plan for the corridor. This motion will be considered at the April 15, 2019 Council meeting.</p>

<p>In November 2018, The City of Surrey’s Council decided to stop the (Surrey-Newton-Guildford Light Rail Transit (SNG-LRT) project and immediately start working with TransLink on a SkyTrain extension from the existing King George SkyTrain Station to Langley City.</p>

<p>$3.5 billion of funding was identified for 27 km of rapid transit along the three corridors (104 Avenue, King George Boulevard, and Fraser Highway) and $1.6 billion of approved funding is currently allocated. The timing of the Fraser Highway corridor precedes rapid transit along 104 Avenue and King George Boulevard.</p>

<p>TransLink’s proposed work plan for the SLS project has a proposed duration of 15-months of project development and four years of construction. <strong>It is estimated that the line could be operational by 2025.</strong></p>

<img class="img-full-width" alt="Surrey–Langley SkyTrain (“SLS”) Project" src="/assets/uploads/industry-news/Surrey-Langley-SkyTrain.jpg">
<p class="image-description">TransLink’s 2017 concept alignment and station locations</p>

<p>The first round of public engagement will take place between April 8 and April 26, 2019. Engagement will take the form of an online survey, open houses, and pop-up events in Surrey City Centre, Newton, Fleetwood, Guildford, and Langley. An online survey is now available for the SLS Project <a href="https://surreylangleyskytrain.ca/community?utm_source=Surrey+Langley+SkyTrain&utm_campaign=5e43dc6fe3-Surrey_Langley_SkyTrain_April_8&utm_medium=email&utm_term=0_c90303e1d8-5e43dc6fe3-382360889" class="color_blue" target="_blank">here</a>.</p>

<p>Pacific Land Group will be monitoring the progress of the Surrey-Langley SkyTrain project and provide updates on new information.</p>
