<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Login</h1>
<hr class="short-line">
<p>Please fill out all fields to sign in</p>

<?php
if (!empty($login_error_msg)) :
  ?>
  <div style="color: red;"><?= $login_error_msg; ?></div>
  <?php
endif;


/*
 * specifying form attributes
 */
$form_attributes = array(
    'class' => 'standard-form',
    'style' => 'max-width: 400px;',
    // 'onsubmit' => 'validateThisForm(event)',
    'method' => 'post',
    'enctype' => 'multipart/form-data',
    'accept-charset' => 'utf-8'
);

/*
 * creating form with specified attributes
 */
echo form_open('', $form_attributes);
?>

<div class="standard-form__field-wrapper">
  <label for="login">Login *</label>
  <input type="text" name="login" value="<?= set_value('login') ?>" title="Please enter your login" id="login" class="standard-form__field" autocomplete="off" autofocus>
  <span class="standard-form__field-error-msg"><?= form_error('login'); ?></span>
</div>

<div class="standard-form__field-wrapper">
  <label for="pwd">Password *</label>
  <input type="password" name="pwd" title="Please enter your password" id="pwd" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('pwd'); ?></span>
</div>

<div class="standard-form__field-wrapper">
  <input type="submit" value="Enter" class="button-like">
</div>


<?php
form_close();
// echo password_hash('123', PASSWORD_DEFAULT);
//echo password_verify("123", config_item('password'));
//echo config_item('login');
?>

