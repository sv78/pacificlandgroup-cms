<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Editing Success Stories Page<br>Header Block:</h1>
<hr class="short-line">


<?php
/* if (!empty($edit_error_msg)) :
  ?>
  <div style="color: red;"><?= $edit_error_msg; ?></div>
  <?php
  endif; */
?>


<?php
/*
 * specifying form attributes
 */
$form_attributes = array(
    'class' => 'standard-form',
// 'onsubmit' => 'validateThisForm(event)',
    'method' => 'post',
    'enctype' => 'multipart/form-data',
    'accept-charset' => 'utf-8'
);

/*
 * creating form with specified attributes
 */

$this->load->helper('form');
echo form_open('', $form_attributes);
?>

<!-- Page Name -->


<!-- CONTENT HTML, JS, PHP, etc -->

<div class="standard-form__field-wrapper">
  <label for="editor">HTML content:</label>
  <textarea name="html" id="editor" class="rte-textarea"><?= empty(set_value('html')) ? $html : set_value('html') ?></textarea>
  <span class="standard-form__field-error-msg"><?= form_error('html'); ?></span>
</div>

<!--
<div><small>Be careful! In editing mode some elements may seem invisible because of the same color as editor background.</small></div>
-->

<!-- <script src="//cdn.ckeditor.com/4.11.4/full/ckeditor.js"></script> -->
<script src="/assets/ckeditor_4.11.4_full/ckeditor.js"></script>
<script src="/assets/themes/default/js/ckeditor4_my_config.js"></script>

<!--
<script src="https://cloud.tinymce.com/5/tinymce.min.js"></script>
<script src="/assets/themes/default/js/my_tinymce_config.js"></script>
-->

<!-- 
<script src="/assets/ckeditor5-build-classic-11.0.1/ckeditor5-build-classic/ckeditor.js"></script>
<script src="/assets/ckeditor5-build-classic-11.0.1/ckeditor5-build-classic/ckeditor_my_config.js"></script>

-->





<!-- Buttons : Quit, Reload, Save, etc -->

<a href="/admin/success-stories" class="button-like">Quit</a>
<a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="button-like">Reload</a>
<input type='submit' class="button-like" value='Save'>

<?php
form_close();
?>



<p>
  <small>
    <a href="/admin/success-stories" class="color_blue">Back to Success Stories</a></small> | <small><a href="/admin" class="color_blue">Administrator Zone</a>
  </small>
</p>




<script>

  document.getElementById('deletePageButton').addEventListener('click', deletePage, false);
  function deletePage(e) {
    e.preventDefault();
    var conf = confirm("Do you want to delete this page and all it's data? Operation cannot be undone.");
    if (!conf) {
      return;
    }
    location.assign(e.target.href);
  }

  var slug_input = document.getElementById('slug_input');
  slug_input.addEventListener("change", replaceSpaces, false);
  function replaceSpaces(e) {
    console.log(e.target);
    var str = e.target.value;
    str = str.trim().toLowerCase();
    str = str.replace(/ +/g, "-");
    e.target.value = str;
  }
</script>