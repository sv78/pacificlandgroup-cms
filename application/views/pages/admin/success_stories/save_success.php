<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Saved</h1>
<hr class="short-line">
<p>Your data was saved successfully</p>

<p class="_sm">You can edit this data again or go to other sections</p>


<p>
  <small><a href="" class="color_blue">Edit This Again</a></small> | 
  <small><a href="/admin/success-stories" class="color_blue">Success Stories</a></small> | 
  <small><a href="/admin" class="color_blue">Administrator Zone</a></small>
</p>