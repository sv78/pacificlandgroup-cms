<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Saved</h1>
<hr class="short-line">
<p>Your data was saved successfully</p>

<p class="_sm">You can edit this project or create new one</p>

<a href="/admin/success-stories/edit-project/<?= $insert_id; ?>" class="button-like">Edit now</a>
<a href="/admin/success-stories/create-project?cat=<?= $this->input->post('category'); ?>" class="button-like">Create New +</a>

<p>
  <small>
    <a href="/admin/success-stories" class="color_blue">Back to Success Stories</a></small> | <small><a href="/admin" class="color_blue">Administrator Zone</a>
  </small>
</p>



