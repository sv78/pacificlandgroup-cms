<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">File manager</h1>
<hr class="short-line">
<!-- <p class="_sm">Use this tool to manipulate files for this website. <b>Important:</b> be aware of intrusion of malware to server side, keep it safe!</p> -->

<!-- Require JS (REQUIRED) -->
<!-- Rename "main.default.js" to "main.js" and edit it if you need configure elFInder options or any things -->
<script data-main="/filemanager/main.js" src="/filemanager/js/require.js"></script>
<!-- Element where elFinder will be created (REQUIRED) -->
  <div id="elfinder" style="min-width: 100%;max-width: 100%; min-height: 480px; max-height: 480px;"></div>

<p><small><a href="/admin" class="color_blue">Back to administrator zone</a></small></p>