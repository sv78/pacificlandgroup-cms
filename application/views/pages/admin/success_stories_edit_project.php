<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Editing Project</h1>
<hr class="short-line">

<h4><?= Str::decode_plain_string($project['name']); ?></h4>






<!-- Image -->

<?php
if (file_exists(config_item('ss_projects_img_dir') . $project['id'] . '.' . $project['img_extension'])) {
  $img_path = "/assets/uploads/ss_projects/{$project['id']}.{$project['img_extension']}";
} else {
  $img_path = "/assets/image_not_found.gif";
}
?>
<img id="project_img" src="<?= $img_path; ?>" class="success-stories-project-main-img">


<form class="standard-form" id="img_form" target="my_iframe" name="img_form" action="/admin/success-stories/img-upload" method="POST" enctype="multipart/form-data">
  <label style="display: inline-block;">
    <span class="button-like">Choose an image</span>
    <input name="fileUpload" id="img_file" type="file" style="display: none;">
  </label>
  <input class="button-like hidden" type="submit" value="Upload" id="upload_submit">
  <span id="img_deleter" class="button-like" data-project-id="<?= $project['id']; ?>">Delete Image</span>
  <input name="project_id" type="hidden" value="<?= $project['id']; ?>">
  <iframe id='my_iframe' name='my_iframe' src="" style="display: none; width: 100%; box-sizing: border-box;"></iframe>
  <div class="_sm _bold color_blue" id="img_name_viewer"></div>
</form>



<script>

  document.getElementById('img_file').addEventListener('change', imgFileChanged, false);
  document.getElementById('my_iframe').addEventListener('load', iframeLoaded, false);
  document.getElementById('img_deleter').addEventListener('click', deleteProjectImage, false);

  function deleteProjectImage(e) {
    var conf = confirm('Do you want to delete image? Operation cannot be undone.');
    if (!conf) {
      return;
    }
    var projectId = e.target.getAttribute('data-project-id');

    $.ajax({
      method: "post",
      url: "/admin/success-stories/ajax/del-img",
      cache: false,
      data: {
        project_id: projectId,
        uid: Math.random(74895)
      }
    }).fail(function (msg) {
      alert('Error');
      console.log(msg);
      console.log(msg.statusText);
    }).done(function (msg) {
      alert(msg);
      document.getElementById('project_img').src = "/assets/image_not_found.gif";
    });

    // 
  }

  function imgFileChanged(e) {
    if (e.target.value != '') {
      $('#upload_submit').removeClass('hidden');
    } else {
      $('#upload_submit').addClass('hidden');
    }
    document.getElementById('img_name_viewer').innerHTML = e.target.value;
  }

  function iframeLoaded(e) {
    if (e.target.contentWindow.document.body.innerHTML == "") {
      return;
    } else if (e.target.contentWindow.document.body.innerHTML == "error") {
      alert("Error occured while trying to upload a file!");
    } else {
      $('#upload_submit').addClass('hidden');
      document.getElementById('project_img').src = "/assets/uploads/ss_projects/" + e.target.contentWindow.document.body.innerHTML + "?uid=" + Math.random(1000000);
      alert("File uploaded successfully!");
    }
  }
</script>













<?php
if (!empty($edit_error_msg)) :
  ?>
  <div style="color: red;"><?= $edit_error_msg; ?></div>
  <?php
endif;
?>


<?php
/*
 * specifying form attributes
 */
$form_attributes = array(
    'class' => 'standard-form',
// 'onsubmit' => 'validateThisForm(event)',
    'method' => 'post',
    'enctype' => 'multipart/form-data',
    'accept-charset' => 'utf-8'
);

/*
 * creating form with specified attributes
 */

$this->load->helper('form');
echo form_open('', $form_attributes);
?>



<!-- URI-slug -->

<div class="standard-form__field-wrapper _sm">
  <span>Project URI-slug: </span><span class="color_dark _bold"><?= $project['slug']; ?></span>
</div>


<!-- Category -->

<div class="standard-form__field-wrapper">
  <label for="category_select">Category *</label>
  <select name='category' id='category_select' class="standard-form__field">
    <?php
    foreach ($cats as $c) :
      echo "<option value={$c['id']}";
      if ($c['id'] == $cat['id']) {
        echo " selected";
      }
      echo ">";
      echo $c['name'];
      echo "</option>";
    endforeach;
    ?>
  </select>
  <span class="standard-form__field-error-msg"><?= form_error('category'); ?></span>
</div>




<!-- Project Name -->

<div class="standard-form__field-wrapper">
  <label for="name_input">Project Name *</label>
  <input type="text" name="name" value="<?= !isset($_POST['name']) ? Str::decode_plain_string($project['name']) : set_value('name') ?>" id="name_input" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('name'); ?></span>
</div>



<!-- Title -->

<div class="standard-form__field-wrapper">
  <label for="title_input">Title *</label>
  <input type="text" name="title" value="<?= !isset($_POST['title']) ? Str::decode_plain_string($project['title']) : set_value('title') ?>" id="title_input" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('title'); ?></span>
</div>



<!-- Meta Description -->

<div class="standard-form__field-wrapper">
  <label for="meta_description_input">Meta Description *</label>
  <input type="text" name="meta_description" value="<?= !isset($_POST['meta_description']) ? Str::decode_plain_string($project['meta_description']) : set_value('meta_description'); ?>" id="meta_description_input" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('meta_description'); ?></span>
</div>



<!-- Lead -->

<div class="standard-form__field-wrapper">
  <label for="lead_input">Lead *</label>
  <input type="text" name="lead" value="<?= !isset($_POST['lead']) ? Str::decode_plain_string($project['lead']) : set_value('lead'); ?>" id="lead_input" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('lead'); ?></span>
</div>



<!-- HTML -->

<textarea name="html" id="editor"><?= empty(set_value('html')) ? Str::decode_html_string($project['html']) : set_value('html') ?></textarea>
<!-- <script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script> -->
<script src="/assets/ckeditor5-build-classic-11.0.1/ckeditor5-build-classic/ckeditor.js"></script>
<script>
  ClassicEditor
          .create(document.querySelector('#editor'),
                  {
                    heading: {
                      options: [
                        /* {model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1'}, */
                        {model: 'heading3', view: 'h3', title: 'Heading 3', class: ''},
                        {model: 'heading4', view: 'h4', title: 'Heading 4', class: ''}
                      ]
                    },
                    toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList' /*, 'numberedList', 'blockQuote' */],
                  }
          )
          .catch(error => {
            console.error(error);
          });
</script>
<span class="standard-form__field-error-msg"><?= form_error('html'); ?></span>



<!-- Published -->

<div class="standard-form__field-wrapper">
  <label for="published_checkbox">
    <input value="1" type="checkbox" name="published" id="published_checkbox" <?= set_checkbox('published', '1', boolval($project['published'])); ?>>
    Publish
  </label>
  <span class="standard-form__field-error-msg"><?= form_error('published'); ?></span>
</div>




<!-- Buttons : Quit, Reload, Save, etc -->

<a href="/admin/success-stories/<?= empty($cat['id']) ? "" : $cat['id']; ?>" class="button-like">Quit</a>
<a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="button-like">Reload</a>
<input type='submit' class="button-like" value='Save'>

<?php
form_close();
?>











<p>
  <small>
    <a href="/admin/success-stories" class="color_blue">Back to Success Stories</a></small> | <small><a href="/admin" class="color_blue">Administrator Zone</a>
  </small>
</p>




<div class="text-align-right">
  <small>
    <a id="deleteProjectButton" href="/admin/success-stories/delete-project/<?= $project['id']; ?>" class="color_blue" style="text-decoration: underline;">Delete this project</a>
  </small>
</div>

<script>

  document.getElementById('deleteProjectButton').addEventListener('click', deleteProject, false);
  function deleteProject(e) {
    e.preventDefault();
    var conf = confirm("Do you want to delete this project and all it's data? Operation cannot be undone.");
    if (!conf) {
      return;
    }
    location.assign(e.target.href);
  }
</script>