<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Deleted</h1>
<hr class="short-line">
<p>The project was deleted successfully</p>

<p class="_sm">You can create new one</p>

<a href="/admin/success-stories/create-project?cat=<?= $project['success_stories_cat_id']; ?>" class="button-like">Create New +</a>

<p>
  <small>
    <a href="/admin/success-stories/<?= $project['success_stories_cat_id']; ?>" class="color_blue">Back to deleted project related category</a>
  </small> |
  <small>
    <a href="/admin/success-stories" class="color_blue">Back to Success Stories</a>
  </small> |
  <small>
    <a href="/admin" class="color_blue">Administrator Zone</a>
  </small>
</p>



