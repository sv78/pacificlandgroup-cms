<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Create New Project:</h1>
<hr class="short-line">

<?php
if (!empty($slug_error_msg)) :
  ?>
  <div style="color: red;"><?= $slug_error_msg; ?></div>
  <?php
endif;
?>


<?php
/*
 * specifying form attributes
 */
$form_attributes = array(
    'class' => 'standard-form',
// 'onsubmit' => 'validateThisForm(event)',
    'method' => 'post',
    'enctype' => 'multipart/form-data',
    'accept-charset' => 'utf-8'
);

/*
 * creating form with specified attributes
 */

$this->load->helper('form');
echo form_open('', $form_attributes);
?>


<!-- Category -->

<div class="standard-form__field-wrapper">
  <label for="category_select">Select category *</label>
  <select name='category' id='category_select' class="standard-form__field">
    <?php
    foreach ($cats as $c) :
      echo "<option value='{$c['id']}' ";

      if (!empty($this->input->get('cat')) && $this->input->get('cat') == $c['id']) {
        echo " selected";
      } else {
        echo set_select('category', $c['id'], false);
      }

      echo ">";
      echo $c['name'];
      echo "</option>";
    endforeach;
    ?>
  </select>
  <span class="standard-form__field-error-msg"><?= form_error('category'); ?></span>
</div>





<!-- Project Name -->

<div class="standard-form__field-wrapper">
  <label for="name_input">Project Name *</label>
  <input type="text" name="name" value="<?= set_value('name') ?>" id="name_input" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('name'); ?></span>
</div>



<!-- Buttons : Quit, Reload, Save, etc -->

<a href="/admin/success-stories/<?= empty($cat['id']) ? "" : $cat['id']; ?>" class="button-like">Quit</a>
<input type='submit' class="button-like" value='Save'>

<?php
form_close();
?>


<div><span class="_sm">Important: project name value is used to configure automatically an unique URI-segment for the project. URI-segment is an important parameter for SEO and can not be changed for the project after it has been saved even in editing mode.</span></div>



<p><small>
    <a href="/admin/success-stories" class="color_blue">Back to Success Stories</a></small> | <small><a href="/admin" class="color_blue">Administrator Zone</a>
  </small>
</p>



