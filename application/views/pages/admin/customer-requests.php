<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Customer Requests</h1>
<hr class="short-line">

<?php
if (count($requests) === 0) {
  echo "<p><small>There are no requests yet…</small></p>";
}

foreach ($requests as $r):
  $arr = [];
  $arr[] .= "<p>";
  $arr[] .= "<small>";
  $arr[] .= "<a href='/admin/customer-requests/{$r['id']}' class='color_a'>";
  $arr[] .= Date("Y/m/d", $r['time']);
  $arr[] .= " - <b>" . $r['name'] . '</b>';
  $arr[] .= "</a>";
  $arr[] .= "</small>";
  $arr[] .= "</p>";
  echo implode('', $arr);
endforeach;
?>
