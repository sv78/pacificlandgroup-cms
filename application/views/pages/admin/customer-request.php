<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Customer Request</h1>
<hr class="short-line">

<p>Name: <b class="color_a"><?= Str::decode_plain_string($request['name']); ?></b></p>
<p>Company: <?= Str::decode_plain_string($request['company']); ?></p>
<p>Email: <a href="mailto:<?= Str::decode_plain_string($request['email']); ?>"><?= Str::decode_plain_string($request['email']); ?></a></p>
<p>Phone: <?= Str::decode_plain_string($request['phone']); ?></p>
<pre class="pre-customer-message">
  <?= Str::decode_plain_string($request['message']); ?>
</pre>
<p><small>Area(s) of interest: <b><?= Str::get_the_type($request['type']); ?></b></small></p>
<p><small>Preferred communication: <?= Str::get_the_foc($request['foc']); ?></small></p>
<p><small>Was sent on: <b><?= Date("Y F d", $request['time']); ?></b> at <b><?= Date("H:i:s", $request['time']); ?></b></small></p>
