<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Administrator Zone</h1>
<hr class="short-line">
<p>Welcome to administrator zone. Here you can manage acceptable content. Please select desired section from the list below.</p>
<h4>Available Sections</h4>

<ul>
  <li>
    <a href="#" class="color_darky _bold">Settings</a>
  </li>
  <li>
    <a href="/admin/pages" class="color_blue _bold">Pages</a>
  </li>
  <li>
    <a href="#" class="color_darky _bold">Menus</a>
  </li>
  <li>
    <a href="/admin/file-manager" class="color_blue _bold">File manager</a>
  </li>
</ul>
<ul>
  <li>
    <a href="/admin/customer-requests" class="color_blue _bold">Customer Requests</a>
  </li>
  <li>
    <a href="/admin/success-stories" class="color_blue _bold">Success Stories Section</a>
  </li>
</ul>

<div>
  <a href="/logout" class="button-like">Logout</a>
</div>

