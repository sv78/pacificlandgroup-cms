<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Success Stories Section</h1>
<hr class="short-line">

<p>Click corresponding links to edit `Success Stories` page parts:</p>

<p>
  <a class="small color_blue" href="/admin/success-stories/header_edit" class="color_blue _bold">Header</a>
  <span>&</span>
  <a class="small color_blue" href="/admin/success-stories/content_edit" class="color_blue _bold">Content</a>
</p>

<hr>


<p>Select desired category to get the list of related projects or click button below to create new project into specified category.</p>

<h4>Categories</h4>
<ul>
  <?php
  foreach ($cats as $cat) :
    ?>
    <li>
      <a href="/admin/success-stories/<?= $cat['id']; ?>" class="color_blue _bold"><?= $cat['name']; ?></a>
    </li>
    <?php
  endforeach;
  ?>
</ul>


<a href="/admin/success-stories/create-project" class="button-like">Create New Project +</a>


<p><small><a href="/admin" class="color_blue">Back to administrator zone</a></small></p>


