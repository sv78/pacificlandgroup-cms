<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Deleted</h1>
<hr class="short-line">
<p>The page was deleted successfully</p>

<p class="_sm">You can create new one</p>

<a href="/admin/pages/create/<?= $page['parent_id']; ?>" class="button-like">Create New +</a>

<p>
  <small>
    <a href="/admin/pages/<?= $page['parent_id']; ?>" class="color_blue">Back to deleted page parent page</a>
  </small> |
  <small>
    <a href="/admin/pages" class="color_blue">Back to Pages</a>
  </small> |
  <small>
    <a href="/admin" class="color_blue">Administrator Zone</a>
  </small>
</p>



