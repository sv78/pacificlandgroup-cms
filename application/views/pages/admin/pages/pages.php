<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Pages</h1>
<hr class="short-line">
<?php
if (isset($count_all_pages)) :
  ?>
  <div class="_sm color_black">
      <?php
      echo "<b>Pages total: " . $count_all_pages . '</b> (`Contact Us`, `Success Stories`and all inner pages of `Success Stories` section are not included)';
      if (isset($count_published_pages)) :
        echo "; <b>published: " . $count_published_pages . "</b>";
      endif;
      ?>
    
  </div>
  <?php
endif;
?>


<?php
if (!empty($parent_page_link)) :
  ?>
  <h4>
    <span class='bg-color_darky color_white'>Level: <?php echo $base_page['name']; ?></span>
  </h4>
  <h5>
    <a href="<?php echo $parent_page_link[0]; ?>" class='color_blue'>
      ▲ Go to upper level:
      <?php echo $parent_page_link[1]; ?>
    </a>
  </h5>
  <?php
endif;
?>



<?php
if (!empty($nested_pages)) :
  ?>
  <ul>
    <?php
    $arr = [];
    foreach ($nested_pages as $page):
      $arr = [];
      $arr[] .= "<li class='_underline'>";
      $arr[] .= "<a href='/admin/pages/{$page['id']}' class='color_a' title='Show inner pages'>";
      $arr[] .= $page['name'] . '</b>';
      $arr[] .= "</a>";
      $arr[] .= "<a href='/admin/pages/edit/{$page['id']}' class='admin-icon' title='Edit page'>";
      $arr[] .= '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/><path d="M0 0h24v24H0z" fill="none"/></svg>';
      $arr[] .= "</a>";
      $arr[] .= "</li>";
      echo implode('', $arr);
    endforeach;
    ?>
  </ul>
  <?php
else :
  ?>
  <p><small>0 pages belongs to this page level</small></p>
<?php
endif;
?>

<a href="/admin/pages/create/<?php echo $base_page['id']; ?>" class="button-like" title="Create a new page at this level">Create new page +</a>

<p><small><a href="/admin" class="color_blue">Back to administrator zone</a></small></p>