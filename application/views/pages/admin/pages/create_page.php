<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Create New Page:</h1>
<hr class="short-line">

<?php
if (!empty($slug_error_msg)) :
  ?>
  <div style="color: red;"><?= $slug_error_msg; ?></div>
  <?php
endif;
?>


<?php
/*
 * specifying form attributes
 */
$form_attributes = array(
    'class' => 'standard-form',
// 'onsubmit' => 'validateThisForm(event)',
    'method' => 'post',
    'enctype' => 'multipart/form-data',
    'accept-charset' => 'utf-8'
);

/*
 * creating form with specified attributes
 */

$this->load->helper('form');
echo form_open('', $form_attributes);
?>




<!-- Parent page select -->

<div class="standard-form__field-wrapper">
  <label for="parent_page_select">Parent Page *</label>
  <select name='parent_page_id' id='parent_page_select' class="standard-form__field">
    <?php
    foreach ($pages as $page) :
      echo "<option value='{$page['id']}' ";

      if (!empty($this->uri->segment(4)) && $this->uri->segment(4) == $page['id']) {
        echo set_select('parent_page_id', $page['id'], true);
      } else {
        echo set_select('parent_page_id', $page['id'], false);
      }

      echo ">";
      echo $page['name'];
      echo "</option>";
    endforeach;
    ?>
  </select>
  <span class="standard-form__field-error-msg"><?= form_error('parent_page_id'); ?></span>
</div>



<!-- Page Name -->

<div class="standard-form__field-wrapper">
  <label for="name_input">Page Name *</label>
  <input type="text" name="name" value="<?= set_value('name') ?>" id="name_input" class="standard-form__field" autocomplete="off" autofocus>
  <span class="standard-form__field-error-msg"><?= form_error('name'); ?></span>
</div>


<!-- Slug -->

<div class="standard-form__field-wrapper">
  <label for="name_input">Slug *</label>
  <input type="text" name="slug" value="<?= set_value('slug') ?>" id="slug_input" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('slug'); ?></span>
</div>





<!-- Buttons : Quit, Reload, Save, etc -->

<a href="/admin/pages/<?= !empty($level_page_id) ? $level_page_id : ""; ?>" class="button-like">Quit</a>
<input type='submit' class="button-like" value='Save'>

<?php
form_close();
?>


<div><span class="_sm">Important: <b>`Page name`</b> value is used just for inner purpose to distinguish one page from another even if their titles are the same. <b>`Slug`</b> value is URI-segment for current page that is important parameter for SEO, it is strongly recomended to use only latin letters, digits and dashes only. Both parameters can be changes in `editing mode`, but it is not recommended to change slug after search engine indexing finished.</span></div>



<p><small>
    <a href="/admin/pages" class="color_blue">Back to Pages</a></small> | <small><a href="/admin" class="color_blue">Administrator Zone</a>
  </small>
</p>


<script>
  var slug_input = document.getElementById('slug_input');
  slug_input.addEventListener("change", replaceSpaces, false);
  function replaceSpaces(e) {
    console.log(e.target);
    var str = e.target.value;
    str = str.trim().toLowerCase();
    str = str.replace(/ +/g, "-");
    e.target.value = str;
  }
</script>