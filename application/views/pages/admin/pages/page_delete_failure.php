<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Failure</h1>
<hr class="short-line">
<p>Can not delete this page because this page is a parent page for some other pages.<br>You must delete those nested pages before.</p>

<p>
  <small>
    <a href="/admin/pages" class="color_blue">Back to Pages</a>
  </small> |
  <small>
    <a href="/admin" class="color_blue">Administrator Zone</a>
  </small>
</p>



