<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Editing Page: <span class="color_blue"><?= Str::decode_plain_string($page['name']); ?></span></h1>
<hr class="short-line">


<?php
/* if (!empty($edit_error_msg)) :
  ?>
  <div style="color: red;"><?= $edit_error_msg; ?></div>
  <?php
  endif; */
?>


<?php
/*
 * specifying form attributes
 */
$form_attributes = array(
    'class' => 'standard-form',
// 'onsubmit' => 'validateThisForm(event)',
    'method' => 'post',
    'enctype' => 'multipart/form-data',
    'accept-charset' => 'utf-8'
);

/*
 * creating form with specified attributes
 */

$this->load->helper('form');
echo form_open('', $form_attributes);
?>

<!-- Page Name -->


<div class="standard-form__field-wrapper">
  <label for="name_input">Page Name *</label>
  <input type="text" name="name" value="<?= !isset($_POST['name']) ? Str::decode_plain_string($page['name']) : set_value('name') ?>" id="name_input" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('name'); ?></span>
</div>



<!-- CONTENT HTML, JS, PHP, etc -->

<div class="standard-form__field-wrapper">
  <label for="editor">Page content:</label>
  <textarea name="content" id="editor" class="rte-textarea"><?= empty(set_value('content')) ? Str::decode_html_string($page['content']) : set_value('content') ?></textarea>
  <span class="standard-form__field-error-msg"><?= form_error('content'); ?></span>
</div>


<!-- <script src="//cdn.ckeditor.com/4.11.4/full/ckeditor.js"></script> -->
<script src="/assets/ckeditor_4.11.4_full/ckeditor.js"></script>
<script src="/assets/themes/default/js/ckeditor4_my_config.js"></script>

<!--
<script src="https://cloud.tinymce.com/5/tinymce.min.js"></script>
<script src="/assets/themes/default/js/my_tinymce_config.js"></script>
-->

<!-- 
<script src="/assets/ckeditor5-build-classic-11.0.1/ckeditor5-build-classic/ckeditor.js"></script>
<script src="/assets/ckeditor5-build-classic-11.0.1/ckeditor5-build-classic/ckeditor_my_config.js"></script>

-->







<!-- Parent Page from All Pages -->

<?php
if ($page['id'] == 1) :
  ?>
  <div class="standard-form__field-wrapper">
    <label for="parent_id_input">Parent Page: *</label>
    <p class="_sm"><b class="color_black">none</b> | [<?= $page['name']; ?>] page has no parent page</p>
    <input type="hidden" name="parent_id" value="<?= $page['parent_id']; ?>">
    <span class="standard-form__field-error-msg"><?= form_error('parent_id'); ?></span>
  </div>
  <?php
else :
  ?>
  <div class="standard-form__field-wrapper">
    <label for="parent_page_select">Parent Page: *</label>
    <select name='parent_id' id='parent_page_select' class="standard-form__field">
      <?php
      foreach ($all_pages as $p) :
        if ($p['id'] == $page['id']) :
          continue;
        endif;
        echo "<option value={$p['id']}";
        if ($p['id'] == $page['parent_id']) {
          echo " selected";
        }
        echo ">";
        echo $p['name'];
        echo "</option>";
      endforeach;
      ?>
    </select>
    <span class="standard-form__field-error-msg"><?= form_error('parent_id'); ?></span>
  </div>
<?php
endif;
?>




<!-- Template -->
<div class="hidden">
  <div class="standard-form__field-wrapper">
    <label for="template_select">Template: *</label>
    <select name='template' id='template_select' class="standard-form__field">
      <?php
      foreach ($template_names as $t) :
        echo "<option value={$t}";
        if ($t == $page['template']) {
          echo " selected";
        }
        echo ">";
        echo $t;
        echo "</option>";
      endforeach;
      ?>
    </select>
    <span class="standard-form__field-error-msg"><?= form_error('template'); ?></span>
  </div>
</div>




<!-- URI-slug -->

<?php
if ($page['id'] == 1) :
  ?>
  <div class="standard-form__field-wrapper">
    <label for="slug_input">Page URI-slug: *</label>
    <p class="_sm"><b class="color_black"><?= $page['slug']; ?></b> | [<?php echo $page['name']; ?>] page slug is abstract and can not be modified</p>
    <input type="hidden" name="slug" value="<?= $page['slug']; ?>" id="slug_input" class="standard-form__field" autocomplete="off">
    <span class="standard-form__field-error-msg"><?= form_error('slug'); ?></span>
  </div>
  <?php
else :
  ?>
  <div class="standard-form__field-wrapper">
    <label for="slug_input">Page URI-slug: *</label>
    <input type="text" name="slug" value="<?= !isset($_POST['slug']) ? Str::decode_plain_string($page['slug']) : set_value('slug') ?>" id="slug_input" class="standard-form__field" autocomplete="off">
    <span class="standard-form__field-error-msg"><?= form_error('slug'); ?></span>
  </div>
<?php
endif;
?>






<!-- Title -->

<div class="standard-form__field-wrapper">
  <label for="title_input">Title:</label>
  <input type="text" name="title" value="<?= !isset($_POST['title']) ? Str::decode_plain_string($page['title']) : set_value('title') ?>" id="title_input" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('title'); ?></span>
</div>


<!-- Meta Description -->

<div class="standard-form__field-wrapper">
  <label for="meta_description_input">Meta description:</label>
  <input type="text" name="meta_description" value="<?= !isset($_POST['meta_description']) ? Str::decode_plain_string($page['meta_description']) : set_value('meta_description') ?>" id="meta_description_input" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('meta_description'); ?></span>
</div>



<!-- Meta Keywords -->

<div class="standard-form__field-wrapper">
  <label for="meta_keywords_input">Meta keywords:</label>
  <input type="text" name="meta_keywords" value="<?= !isset($_POST['meta_keywords']) ? Str::decode_plain_string($page['meta_keywords']) : set_value('meta_keywords') ?>" id="meta_keywords_input" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('meta_keywords'); ?></span>
</div>



<!-- Meta Robots -->
<div class="hidden">
  <div class="standard-form__field-wrapper">
    <label for="meta_robots_select">Meta robots: *</label>
    <select name='meta_robots' id='meta_robots_select' class="standard-form__field">
      <?php
      foreach ($meta_robots_values as $mr) :
        echo "<option value='{$mr}'";
        if ($mr == $page['meta_robots']) {
          echo " selected";
        }
        echo ">";
        echo $mr;
        echo "</option>";
      endforeach;
      unset($mr);
      ?>
    </select>
    <span class="standard-form__field-error-msg"><?= form_error('meta_robots'); ?></span>
  </div>
</div>



<!-- Breadcrumb -->

<div class="standard-form__field-wrapper">
  <label for="breadcrumb_input">Breadcrumb: *</label>
  <input type="text" name="breadcrumb" value="<?= !isset($_POST['breadcrumb']) ? Str::decode_plain_string($page['breadcrumb']) : set_value('breadcrumb') ?>" id="breadcrumb_input" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('breadcrumb'); ?></span>
</div>



<!-- Structured data by Schema.org -->

<div class="hidden">
  <label for="structured_data_input">Structured data:</label>
  <textarea name="structured_data" id="structured_data_input" class="standard-form__field"><?= !isset($_POST['structured_data']) ? Str::decode_plain_string($page['structured_data']) : set_value('structured_data') ?></textarea>
  <span class="standard-form__field-error-msg"><?= form_error('structured_data'); ?></span>
</div>



<!-- Header -->

<div class="standard-form__field-wrapper">
  <label for="header_input">Header:</label>
  <input type="text" name="header" value="<?= !isset($_POST['header']) ? Str::decode_plain_string($page['header']) : set_value('header') ?>" id="header_input" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('header'); ?></span>
</div>


<!-- Header Description -->

<div class="standard-form__field-wrapper">
  <label for="header_description_input">Header description:</label>
  <input type="text" name="header_description" value="<?= !isset($_POST['header_description']) ? Str::decode_plain_string($page['header_description']) : set_value('header_description') ?>" id="header_description_input" class="standard-form__field" autocomplete="off">
  <span class="standard-form__field-error-msg"><?= form_error('header_description'); ?></span>
</div>



<!-- Is Published, Is In Menu -->

<div class="standard-form__field-wrapper">

  <!-- Is Published -->

  <label for="is_published_checkbox">
    <input value="1" type="checkbox" name="is_published" id="is_published_checkbox" <?= set_checkbox('is_published', '1', boolval($page['is_published'])); ?>>
    Publish
  </label>
  <span class="standard-form__field-error-msg"><?= form_error('is_published'); ?></span>

  <!-- Is In Menu -->

  <div class="hidden">
    <label for="is_in_menu_checkbox">
      <input value="1" type="checkbox" name="is_in_menu" id="is_in_menu_checkbox" <?= set_checkbox('is_in_menu', '1', boolval($page['is_in_menu'])); ?>>
      Assign to menu
    </label>
    <span class="standard-form__field-error-msg"><?= form_error('is_in_menu'); ?></span>
  </div>

</div>



<!-- Redirect 301 URL -->

<div class="hidden">
  <div class="standard-form__field-wrapper">
    <label for="redirect_301_url_input">Redirect (301) URL:</label>
    <input type="text" name="redirect_301_url" value="<?= !isset($_POST['redirect_301_url']) ? Str::decode_plain_string($page['redirect_301_url']) : set_value('redirect_301_url') ?>" id="redirect_301_url_input" class="standard-form__field" autocomplete="off">
    <span class="standard-form__field-error-msg"><?= form_error('redirect_301_url'); ?></span>
  </div>
</div>




<!-- Buttons : Quit, Reload, Save, etc -->

<a href="/admin/pages/<?= empty($page['parent_id']) ? "" : $page['parent_id']; ?>" class="button-like">Quit</a>
<a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="button-like">Reload</a>
<input type='submit' class="button-like" value='Save'>

<?php
form_close();
?>

<!-- Last time modified -->

<p class="_sm">Last time was modified: <?= date("Y F d ", $page['time_modified']) . " at " . date("H:i", $page['time_modified']); ?></p>





<p>
  <small>
    <a href="/admin/pages" class="color_blue">Back to Pages</a></small> | <small><a href="/admin" class="color_blue">Administrator Zone</a>
  </small>
</p>




<div class="text-align-right">
  <small>
    <a id="deletePageButton" href="/admin/pages/delete/<?= $page['id']; ?>" class="color_blue" style="text-decoration: underline;">Delete this page</a>
  </small>
</div>

<script>

  document.getElementById('deletePageButton').addEventListener('click', deletePage, false);
  function deletePage(e) {
    e.preventDefault();
    var conf = confirm("Do you want to delete this page and all it's data? Operation cannot be undone.");
    if (!conf) {
      return;
    }
    location.assign(e.target.href);
  }

  var slug_input = document.getElementById('slug_input');
  slug_input.addEventListener("change", replaceSpaces, false);
  function replaceSpaces(e) {
    console.log(e.target);
    var str = e.target.value;
    str = str.trim().toLowerCase();
    str = str.replace(/ +/g, "-");
    e.target.value = str;
  }
</script>