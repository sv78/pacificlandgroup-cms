<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Failure</h1>
<hr class="short-line">
<p>Root page can not be deleted but you can unpublish it.</p>

<p>
  <small>
    <a href="/admin/pages" class="color_blue">Back to Pages</a>
  </small> |
  <small>
    <a href="/admin" class="color_blue">Administrator Zone</a>
  </small>
</p>



