<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Saved</h1>
<hr class="short-line">
<p>Your data was saved successfully</p>

<p class="_sm">You can edit this page again or go back to current page level</p>

<a href="/admin/pages/edit/<?= $insert_id; ?>" class="button-like">Edit again</a>
<a href="/admin/pages/<?= $page['parent_id']; ?>" class="button-like">To current page level</a>

<p>
  <small>
    <a href="/admin/pages" class="color_blue">Back to Pages</a></small> | <small><a href="/admin" class="color_blue">Administrator Zone</a>
  </small>
</p>



