<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple"><?= empty($cat['name']) ? "Unknown" : $cat['name']; ?></h1>
<hr class="short-line">
<p>Please select desired project to edit it or click button below to create new project into this category.</p>

<h4>Projects</h4>

<?php
if (count($projects) === 0) {
  echo "<p><small>There are no projects in this category yet…</small></p>";
}
?>

<ul>
  <?php
  //print_r($projects);
  //print_r($cat);

  foreach ($projects as $project) :
    ?>
    <li>
      <a href="/admin/success-stories/edit-project/<?= $project['id']; ?>" class="color_blue _bold"><?= $project['project_name']; ?></a>
    </li>
    <?php
  endforeach;
  ?>
</ul>


<a href="/admin/success-stories/create-project?cat=<?= $cat['id']; ?>" class="button-like">Create New Project +</a>


<p><small><a href="/admin/success-stories" class="color_blue">Back to Success Stories</a></small> • <small><a href="/admin" class="color_blue">Administrator Zone</a></small></p>


