<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2 class="h1-compound h1-compound_dark"><span>Working towards one goal </span><span>Effective Land Use Planning &&nbsp;Environmental Solutions</span></h2>
<hr class="short-line">

<h4>Who are we?</h4>

<ul class="_bold">
  <li>Land use and development consulting firm</li>
  <li>Founded in 1996</li>
  <li>Core team of 10 members</li>
  <li>Specialize in the solving complex land use and related environmental issues</li>
  <li>Planners, urban designers, project managers and environmental specialists</li>
</ul>

<h4>What do we do?</h4>

<ul class="_bold">
  <li>Work for both private and public clients</li>
  <li>Typically involves tough land use issues, we specialize in highest and best land use studies hired by property appraisal firms, lawyers, local government, developers and private property owners</li>
  <li>Development approvals- managing development projects through municipal approval process which can involve extensive public consultation programs and putting together design teams with the right sub consultants to address the specific project needs</li>
</ul>

<h4>What makes us stand out-</h4>

<ul class="_bold">
  <li>Innovative solutions to complex land issues</li>
  <li>We think out of the box and identify how to achieve a solution and then look at how to achieve this within the existing guiding policies and legislations applicable to the project</li>
</ul>

<h4>What we are passionate about:</h4>

<ul class="_bold">
  <li>Cost effective unique solutions</li>
  <li>Bringing businesses to our region</li>
  <li>Creating a positive work environment for our staff- supporting our team`s interests</li>
</ul>

<hr class="short-line">



<h2 class="h-simple">Land Use &&nbsp;Development Planning</h2>


<div id="plg_staff_oleg_verbenkov" class="person-card">
  <img src="/assets/uploads/persons/oleg-verbenkov.jpg" alt="Oleg Verbenkov">
  <p class="person-card__name">Oleg Verbenkov, B.A., C.S.P., MCIP, RPP<br><i>Principal</i></p>
  <p>Oleg is an expert in land use and development planning with over 30 years of combined public and private sector experience. As senior planner and senior project manager, he leads our teams in multi-disciplinary assignments involving various professional services, including engineering, environmental, urban design, landscape architecture, survey, appraisal and land economics. Oleg is a skilled facilitator and is often called upon to handle challenging assignments that typically involve difficult site conditions, complex negotiations or competing visions.</p>
</div>

<div id="plg_staff_laura_jones" class="person-card">
  <img src="/assets/uploads/persons/laura-jones.jpg" alt="Laura Jones">
  <p class="person-card__name">Laura Jones, B.A., C.S.B.A., MCIP, RPP<br><i>Senior Development Planner</i></p>
  <p>Laura is an experienced development Planner with over 11 years in the private sector. With a background that includes local government experience, Laura excels at preparing and managing development applications. She is skilled in project coordination with local government agencies, engineers, surveyors, architects, landscape architects and project managers. With a wide range of experiences in the development field, Laura is an expert at understanding big picture planning principles, breaking them down and identifying the key tasks to create a successful development strategy.</p>
</div>

<div id="plg_staff_eric_wang" class="person-card">
  <img src="/assets/uploads/persons/eric-wang.jpg" alt="Eric Wang">
  <p class="person-card__name">Eric Wang, MSc., MRTPI, MCIP, RPP<br><i>Senior Urban Design Planner</i></p>
  <p>Eric is a registered professional planner in both Canada and United Kingdom. His education combines architectural design, and city and regional planning. Eric specializes in community and neighbourhood land use planning, urban design, master planning, land assembly and development, municipal approval and project management. He has engaged with local developers and international investors on a variety of residential, industrial, commercial developments, and is up to date with the most recent land development trends, development cost, municipal policies, neighbourhood concept plans and major infrastructure improvements.</p>
</div>

<div id="plg_staff_rosa_shih" class="person-card">
  <img src="/assets/uploads/persons/rosa-shih.jpg" alt="Rosa Shih">
  <p class="person-card__name">Rosa Shih, B.E.S.,M.A (Planning)<br><i>Planning Analyst</i></p>
  <p>Rosa is a planning analyst with a comprehensive skill set drawn from private and public-sector planning experience in British Columbia and Ontario. She is responsible for coordinating and managing municipal approval processes including public consultation. Rosa is also responsible for conducting detailed land use research, producing urban design briefs, preparing highest and best land use assessments, and preparing planning rationales. Rosa's educational and professional background combines international development, urban design, environmental management, and city and regional planning.</p>
</div>

<div id="plg_staff_alrooz" class="person-card">
  <img src="/assets/uploads/persons/afrooz.jpg" alt="Afrooz Fallah">
  <p class="person-card__name">Afrooz Fallah, M.A. (Urban Planning & Urban Design)<br><i>Planning Assistant</i></p>
  <p>Afrooz is a planning assistant with a diverse background in urban planning and urban design. Being proficient in ArcGIS and AutoCAD, as well as, SketchUp, Photoshop, Microsoft Office, and InDesign, Afrooz is supporting the planning division and her work involves a full range of project-related tasks such as data collection and analysis, graphics and spatial analysis, best practices research, land-use research, proposal preparation, technical report writing, prepare drawings and presentations.</p>
</div>

<div id="plg_staff_tyler" class="person-card">
  <img src="/assets/uploads/persons/tyler.jpg" alt="Tyler">
  <p class="person-card__name">Tyler Erickson, M.A. (Planning)<br><i>Planning Assistant</i></p>
  <p>Tyler is a recent planning graduate who joins Pacific Land Group as a Planning Assistant. His experience includes work with local and provincial governments, private consultants, as well as volunteer opportunities provided throughout his education. He has a background in urban geography and community planning and has a developed skillset in interpreting regulation, analyzing development constraints/opportunities, and drafting development concepts.</p>
</div>





<h2 class="h-simple">Qualified Environmental Professionals</h2>


<div id="plg_staff_kyla" class="person-card">
  <img src="/assets/uploads/persons/kyla.jpg" alt="Kyla">
  <p class="person-card__name">Kyla Bryant-Milne, R.P. Bio., QEP<br><i>Environmental Biologist</i></p>
  <p>Kyla is PLG’s head Qualified Environmental Professional (QEP) and has a background in terrestrial and aquatic ecology. She has significant experience conducting watercourse assessments, environmental monitoring, and other field assessments (e.g., bird nest surveys, RAR). Kyla is certified to prepare and sign off on Sensitive Ecosystem Development Plans (SEDP) and Construction Environmental Management Plans (CEMP). She is well versed in local government approvals (Water Sustainability Act, Scientific Fish Collection Permits, Species at Risk Inventories).</p>
</div>


<div id="plg_staff_melissa" class="person-card">
  <img src="/assets/uploads/persons/melissa.jpg" alt="Melissa">
  <p class="person-card__name">Melissa Englouen, B.I.T., BC-CESCL<br><i>Junior Biologist</i></p>
  <p>Melissa is a Biologist in Training (BIT) with a background in biology and ecological restoration. Her environmental industry experience sets the foundation for her specialization in invasive vegetation management, riparian habitat restoration, environmental and watercourse assessments, and water quality sampling and monitoring. Melissa is skilled in the preparation of environmental reports including Sensitive Ecosystem Development Plans (SEDP), Watercourse Classification Confirmation Assessment reports, Erosion and Sediment Control (ESC) Monitoring reports, and Habitat Restoration/Compensation Plans.</p>
</div>

<div id="plg_staff_andrew" class="person-card">
  <img src="/assets/uploads/persons/andrew.jpg" alt="Andrew Otto-Artavia">
  <p class="person-card__name">Andrew Otto-Artavia, B.I.T., BC-CESCL<br><i>Environmental/ESC Technician</i></p>
  <p>Andrew is a Biologist in Training (BIT) with 2 years of experience in the environmental field, including stream assessments, bird nest surveys, erosion and sediment control (ESC) monitoring, and fish salvage work. Andrew is also involved with Floc Systems Inc. where he is responsible for the distribution and maintenance of Tigerfloc products for ESC purposes.</p>
</div>


<div id="plg_staff_chau" class="person-card">
  <img src="/assets/uploads/persons/chau.jpg" alt="Chau Le">
  <p class="person-card__name">Chau Le, BC-CESCL<br><i>Civil Technologist/Floc Systems Specialist</i></p>
  <p>Chau is a Civil Technologist who works in collaboration with Pacific Land Group's Planning and Environmental Divisions to create custom design solutions specific to each project. Chau also collaborates with Hub Engineering and Floc Systems to assist with engineering design plans and Erosion and Sediment Control (ESC) and water quality solutions.</p>
</div>



<h2 class="h-simple">Administration</h2>


<div id="plg_staff_melody" class="person-card">
  <img src="/assets/uploads/persons/melody.jpg" alt="Melody">
  <p class="person-card__name">Melody Brodie, PCP<br><i>Payroll and Accounting Administrator</i></p>
  <p>Melody has worked for Pacific Land Group's Administration since 2014 and is the face of our administration team. She has many years of business administration experience and an education background in business management, accounting and payroll.</p>
</div>


<div id="plg_staff_jolene" class="person-card">
  <img src="/assets/uploads/persons/jolene.jpg" alt="Jolene Connelly-Pitts">
  <p class="person-card__name">Jolene Connelly-Pitts<br><i>Administrative Assistant</i></p>
  <p>Jolene joined our team in February 2018 as an Administrative Assistant. She has over 2 years of experience working in similar roles, and is responsible for accounts payable and receivable.</p>
</div>

<div id="plg_staff_valerie" class="person-card">
  <img src="/assets/uploads/persons/valerie.jpg" alt="Valerie">
  <p class="person-card__name">Valerie Hollman<br><i>Office Receptionist</i></p>
  <p>Valerie has been our Receptionist since 2017. Her experience stems from 15 years in a customer service position, and her responsibilities at PLG include switchboard operation, office letter preparation, and courier/mail service coordination.</p>
</div>
