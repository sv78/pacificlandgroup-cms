<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2 class="h-simple">PLG Industry News</h2>
<hr class="short-line">


<h4>April 2019</h4>
<p>
  <a href="land-planner-corner/surrey-langley-skytrain" class="color_blue">
    <span class="_bold">Surrey–Langley SkyTrain (“SLS”) Project and the upcoming public engagement in April 2019</span>
  </a>
</p>

<p>A report outlining the upcoming public engagement on the Surrey-Langley SkyTrain (“SLS”) Project was delivered to City of Surrey Council on April 1, 2019.  City staff will now begin work on a planning areas review with TransLink to confirm land use planning along 104 Avenue and King George Boulevard. Staff will review population and employment projections, and participate in the public engagement process together with TransLink.</p>


<p><a class="color_blue" href="land-planner-corner/surrey-langley-skytrain">Read more</a></p>



<h4>April 2019</h4>
<p>
  <a href="/land-planner-corner/bill-15" class="color_blue">
    <span class="_bold">Bill 15: Proposed Amendments to the <i>Agricultural Land Commission Act</i></span>
  </a>
</p>

<p>Bill 15, the Agricultural Land Commission Amendment Act 2019, was presented for first reading at the BC Legislature and continues to be a subject of provincial debate. The Bill proposes administrative changes to the ALC decision making structure. Changes to how (and who) can submit an exclusion application are also notable.</p>

<p><a class="color_blue" href="/land-planner-corner/bill-15">Read more</a></p>





<h4>March 2019</h4>
<p>
  <a href="/land-planner-corner/bill-52" class="color_blue">
    <span class="_bold">Bill 52: Changes to the ALC Act and Regulations</span>
  </a>
  <br>
  <span class="_italic">Residences in the ALR and the Placement of Fill</span>
</p>

<p>On February 22, 2019, B.C.'s Lieutenant Governor in Council approved the Agricultural Land Reserve (ALR) Regulations which brings into force changes to the <i>Agricultural Land Commission Act</i> under Bill 52. The main changes to these pieces of legislation pertain to limits on house sizes of residential uses and the placement of fill for properties in the ALR.</p>

<p>Through Bill 52, <strong>the principal residence of a parcel in the ALR is limited to 500 m2 (5,400 ft<sup>2</sup>) or less. Only one residence per parcel is permitted</strong>. Any residential use proposing more than one residential use and/or a floor area greater than 500 m<sup>2</sup> is now required to submit an application for a "non-adhering residential use".</p>

<p>While the <i>ALC Act</i> and Regulations have previously contained provisions facilitating the construction of additional dwellings for farm help, manufactured homes for immediate family members, and accommodation above an existing farm building, these provisions have been repealed. Any request for these uses also require an application for a “non-adhering residential use”.</p>

<p><a class="color_blue" href="/land-planner-corner/bill-52">Read more</a></p>


<h4>October 17, 2016</h4>
<p>The City of Surrey recently adopted new by-law requirements called Ecosystem Protection Measures, including a new Sensitive Ecosystem Development Permit Area and new setbacks for Streamside Protection. This changes affect all properties within 50 m of the Green Infrastructure Area and within 50 m of the top of bank of any Class A, A/0, or B stream.</p>
<p>To meet the requirements, each application must include an Ecosystem Development Plan (based on accompany reports) to demonstrate how the Protection Areas (either a Green Infrastructure Area or Streamside Area) will be protected within the proposed development, based on an assessment of the current conditions. In cases where variances are needed for setback requirements, an Impact Mitigation Plan is required to detail how the proposed variance will not affect the health of the Ecosystem.</p>
<p>A Qualified Environmental Professional (QEP) is required to prepare these plans. Pacific Land Group regularly works with Surrey’s regulations and can assist you with these requirements.</p>



<h4>November 7, 2014</h4>
<p>Pacific Land Group was recently recognized by the Surrey Board of Trade as one of Surrey's best businesses.  The Surrey Business Excellence Award was awarded to Pacific Land Group within the 1-10 employees category.</p>


<h4>October 28, 2014</h4>
<p>Pacific Land Group's	Christopher Correia was	recently featured in a Vancouver Sun article focusing on the contraints to, and costs of, commercial office development in Metro Vancouver.</p>


<h4>October 24, 2014</h4>
<p>NAIOP Vancouver released their second phase of the Long Term Forecast and Analysis of Metro Vancouver`s Industrial Lands. The report was prepared by Pacific Land Group and showcases the limited availability of industrial land across the region, specifically that Metro Vancouver has 2,919 acres of land or just over eleven years of supply left.</p>


<h4>October 1, 2014</h4>
<p>Pacific Land Group's newest project in Campbell Heights is highlighted in a recent <a href="https://biv.com/article/2014/09/campbell-heights-industrial-oasis-developers" class="color_blue">Business in Vancouver</a> article.</p>