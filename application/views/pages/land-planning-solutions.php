<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2 class="h1-compound h1-compound_dark"><span>Unique cost-effective solutions </span><span>Pacific Land Group&nbsp;- Land Use Planning Services</span></h2>
<hr class="short-line">

<h3 class="h-simple">What land planning solutions<br>are our clients looking for?</h3>

<ul class="ul-nested">

  <li>
    Inexperienced Developers
    <ul>
      <li>
        Private landowners (Subject property is their primary residence)
        <ul>
          <li>Looking to subdivide</li>
          <li>Looking for advice as they are being approached by developers</li>
          <li>Looking for advice as the City is doing a new area plan</li>
          <li>Looking for advice on what can be done on their property in the ALR (Agricultural Land Reserve)</li>
        </ul>
      </li>
      <li>
        First Time Developers (subject property is not their residence)
        <ul>
          <li>Require project management through the entire phases of development</li>
          <li>Require design team recommendations (Architect, Civil Engineer, Landscape Architect, Arborist etc.)</li>
          <li>Require information on each step of the process</li>
          <li>Rely on our foresight to reduce unnecessary delays and costs</li>
        </ul>
      </li>
    </ul>
  </li>

  <li>
    Independent Developers
    <ul>
      <li>
        Developers who have completed a couple of projects
        <ul>
          <li>May have their own sub consultants (Architects, Landscape Architects etc). Require PLG to coordinate the sub-consultants, staff comments, outside agency approvals, and committee approvals</li>
        </ul>
      </li>
    </ul>
  </li>

  <li>
    Experienced Developers/Corporations and Organizations
    <ul>
      <li>
        <ul>
          <li>More complex projects</li>
          <li>Rely on us for our specialized services, local knowledge and relationships</li>
        </ul>
      </li>
    </ul>
  </li>

  <li>
    Municipalities
    <ul>
      <li>
        Real Estate departments
        <ul>
          <li>Rely on our development industry knowledge</li>
        </ul>
      </li>
      <li>
        Development Wings
        <ul>
          <li>Rely on our development industry knowledge to determine Land Use Potential</li>
        </ul>
      </li>
    </ul>
  </li>

  <li>
    Other professionals
    <ul>
      <li>
        Lawyers
        <ul>
          <li>Rely on our development industry knowledge</li>
          <li>Rely on our research and land assessment skills</li>
          <li>Professional reports including highest and best land use, Forensic planning, and expert witness services</li>
        </ul>
      </li>
      <li>
        Appraisers
        <ul>
          <li>Rely on us for Planning and Land Use assessments for input into highest and best use appraisal reports</li>
        </ul>
      </li>
    </ul>
  </li>

</ul>





<h3 class="h-simple">What value does Pacific Land Group deliver?</h3>




<ul class="ul-nested">

  <li>
    Corporate Clients
    <ul>
      <li>
        What did we do?
        <ul>
          <li>Assessed site selection for Branch Offices</li>
        </ul>
      </li>
      <li>
        Benefits to Client’s organization
        <ul>
          <li>Client was able to understand all issues related to acquiring the land, including permit approvals and costs of developing the chosen property</li>
        </ul>
      </li>
    </ul>
  </li>


  <li>
    Land Development Clients
    <ul>
      <li>
        What did we do?
        <ul>
          <li>Assessed site acquisition and development issues</li>
          <li>Prepared development approval strategy</li>
        </ul>
      </li>
      <li>
        Benefits to the organization
        <ul>
          <li>Client was able to understand all issues related to acquiring the land, permit approvals and costs of developing the chosen property</li>
        </ul>
      </li>
    </ul>
  </li>


  <li>
    Crown Corporations/Agencies
    <ul>
      <li>
        What did we do?
        <ul>
          <li>Assisted with planning for acquisition or disposal of property, and de-risking land use issues</li>
        </ul>
      </li>
      <li>
        Benefits to the organization
        <ul>
          <li>Allowed for quicker sale or purchase of land</li>
          <li>Created more certainty for purchaser and supported land value</li>
        </ul>
      </li>
    </ul>
  </li>


  <li>
    Professional Clients
    <ul>
      <li>
        What did we do?
        <ul>
          <li>Supported informed appraisal</li>
          <li>Provided factual information and professional opinion for litigation</li>
        </ul>
      </li>
      <li>
        Benefits to the organization
        <ul>
          <li>Analyzed property and impact of land taking by senior government</li>
          <li>Provided historical forensic land approval potential</li>
        </ul>
      </li>
    </ul>
  </li>



  <li>
    Municipalities/Government
    <ul>
      <li>
        What did we do?
        <ul>
          <li>Assisted with planning for acquisition or disposal of property, and de-risking land use issues</li>
        </ul>
      </li>
      <li>
        Benefits to the organization
        <ul>
          <li>Allowed for quicker sale or purchase of land</li>
        </ul>
      </li>
    </ul>
  </li>


  <li>
    Business Clients
    <ul>
      <li>
        What did we do?
        <ul>
          <li>Assessed site selection for Branch Offices</li>
        </ul>
      </li>
      <li>
        Benefits to the organization
        <ul>
          <li>Client was able to understand all issues related to acquiring the land, permit approvals and the costs for developing the chosen property</li>
        </ul>
      </li>
    </ul>
  </li>

</ul>


