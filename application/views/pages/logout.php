<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h1 class="h-simple">Logged Out</h1>
<hr class="short-line">
<p>You have logged out</p>
<a href="/login" class="button-like">Login</a>
<a href="/" class="button-like">Site</a>

