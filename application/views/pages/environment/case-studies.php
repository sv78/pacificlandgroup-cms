<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<!-- Case Studies -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_environment-case-studies"></div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h1 class="h-simple">Fergus Creek Success Story</h1>
      <hr class="short-line">

      <p>PLG provides clients with various environmental services including on-site assessments, environmental permitting assistance, Sensitive Ecosystem Development Plan preparation, and <i>Water Sustainability Act</i> Section 11 application preparation and management. The Qualified Environmental Professionals (QEPs) at PLG identify, assess and incorporate environmental practices and procedures, and mitigation measures as part of land use planning and development projects.</p>

      <?= $staff_card; ?>

    </div>
  </div>

</div>




<div id="fergus-creek" class="row-section row-section_dark bg-color_dark">

  <div class="row-section__txt row-section__txt_75 row-section__txt_left">
    <div class="row-section__content-container">

      <p>PLG submitted an Official Community Plan (OCP) amendment and rezoning application for four (4) properties located at 1083 168 Street, 1109 168 Street, 1177 168 Street, and 1068 No. 99 Highway (Subject Property). The Subject Property is situated at the southern end of the City of Surrey, accessed via 168 Street with connections to 8 Avenue and 16 Avenue, allowing for access to Highway 99 and 176 Street. The specific intent of the application was to amend the OCP designation of the Subject Property to “Urban” and to rezone to “Comprehensive Development Zone” (“CD”), in order to accommodate multi-family residential land uses.</p>

      <p>The development footprint of the Subject Property has the potential to result in affecting the biological attributes of the surrounding area. Based on the client’s request, PLG was responsible for a full suite of planning services and environmental services, including environmental permitting, an on-site biophysical and watercourse assessment, <i>Water Sustainability Act (WSA)</i> Section 11 Change Approval application for proposed watercourse closures. PLG also provided the client with an SEDP report, including a watercourse setback plan which helped determine the potential development footprint area based on regulatory setbacks for fish and fish habitat, and wildlife Species at Risk (SAR). Post construction reclamation services, including habitat restoration and a detailed planting plan, were also completed as part of the environmental services for this project.</p>

    </div>
  </div>

  <div class="row-section__bg row-section__bg_25 row-section__bg_right bg_environment-case-studies_fergus-creek"></div>

</div>










<div id="wildlife-salvage" class="row-section row-section_light bg-color_light-gray">
  <div class="row-section__txt row-section__txt_75 row-section__txt_right">
    <div class="row-section__content-container">

      <h1 class="h1-compound h1-compound_dark"><span>Wildlife Salvage </span><span>Success Story</span></h1>
      <hr class="short-line">
      
      <p>PLG supports clients in Species at Risk wildlife salvage activities including permit application, trap installation and inspection, wildlife capture documentation, and report submission.</p>
      <p>In July 2017, the Ministry of Transportation and Infrastructure (MOTI) proposed a highway interchange upgrade in North Vancouver. As part of this project, a Section 11 <i>Water Sustainability Act</i> Change Approval application was submitted and approved, which authorized instream work and stream relocation of Keith Creek. As part of highway widening works, there was the risk for Pacific Water Shrew habitat to be lost. PLG was contracted by the client to perform a Pacific Water Shrew <i>(Sorex bendirii)</i> salvage within the project limits. PLG was in charge of applying for salvage permits, conducting on-site trap inspection and maintenance, and documenting all the wildlife captures. When the salvage was completed, PLG prepared a salvage summary report and submitted it to the Ministry of Forests, Lands and Natural Resource Operations & Rural Development (MFLNRORD) online database.</p>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_25 row-section__bg_left bg_environment-case-studies_wildlife-salvage"></div>
</div>









<div class="row-section row-section_dark bg-color_grayblue">

  <div class="row-section__txt row-section__txt_75 row-section__txt_left">
    <div class="row-section__content-container">
      
      <h1 class="h1-compound h1-compound_light"><span>Spill Response </span><span>Success Story</span></h1>
      <hr class="short-line">

      <p>PLG assists clients in spill response clean-up activities including third party environmental monitoring, waterbody classification, identification of the impacts of contamination, surficial water quality sampling, and environmental report preparation.</p>

      <p>In March 2017, PLG responded to a truck spill that happened on a linear transmission project in Alberta. Deviation from a pre-planned access route resulted in the accidental release of approximately 10 litres of engine oil into a waterbody. Approximately 23,000 L of hydrocarbon contaminated water from the ponded areas was recovered with a vacuum truck, while 30 containment booms, 200 absorption pads, and 40 absorption pillows were used to contain the spill. A local company conducting infrastructure maintenance retained PLG to assist in the full clean-up of the spill, including emergency spill response activities, third party environmental monitoring, waterbody classification, contamination impact identification, surface water quality sampling, and an environmental assessment including report preparation of the contamination and remediation strategies.</p>

    </div>
  </div>

  <div class="row-section__bg row-section__bg_25 row-section__bg_right bg_environment-case-studies_spill-response"></div>

</div>