<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<!-- Environmental Monitoring -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_environment-environmental-monitoring"></div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h1 class="h-simple">Other Environmental Services – Post-construction Monitoring</h1>
      <hr class="short-line">

      <p>Following the completion of the project, PLG’s QEPs perform post-construction monitoring to check the functionality of the newly installed features (e.g. riparian vegetation, ponds) and to ensure effectiveness and compliance with the regulatory guidelines.</p>

      <?= $staff_card; ?>

    </div>
  </div>

</div>




<div class="row-section row-section_dark bg-color_grayblue">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <p>PLG’s QEPs also conduct post-construction assessments to inspect the integrity of the newly established ecological communities (e.g. watercourse or riparian habitat) follow prescribed riparian/landscape planting plans and to certify the release of project bonding.</p>

    </div>
  </div>

</div>





<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Other Environmental Services&nbsp;– P-15&nbsp;Monitoring</h2>
      <hr class="short-line">

      <p>As part of the City of Surrey’s policy number P-15, all riparian planting completed as part of habitat compensation works for proposed development are required to adhere to a P-15 Agreement. The P-15 Agreement requires yearly monitoring of plantings and planting area by a QEP for a project-specific duration defined by the City (typically a 5-year term).</p>

    </div>
  </div>

</div>



<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Erosion and Sediment Control &&nbsp;Construction Monitoring</h2>
      <hr class="short-line">

      <p>Environmental construction monitoring ensures site specific designs are implemented on the ground and allows for modifications to address changing onsite conditions. As an environmental construction monitor, PLG’s QEPs mitigate the impacts of construction activity and provide innovative and cost effective solutions. <a href="/environment/erosion-sediment-control#esccm" class="color_blue">Read more about Construction Monitoring</a>.</p>

    </div>
  </div>

</div>

