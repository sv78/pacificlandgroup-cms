<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<!-- Sediment & Erosion Control Service -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_sediment-and-erosion-control"></div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h1 class="h-simple">Erosion and Sediment Control</h1>
      <hr class="short-line">

      <p>Erosion and Sediment Control (ESC) is the practice of preventing stormwater contamination by sediment through controlling the amount of exposed soils during construction. Contaminated stormwater from sediment pollution can result in a large number of environmental issues such as:</p>

      <div class="offset-top-20">
        <ul class="ul-extra _bold">
          <li>Increased flooding potential from sediment filled storm drains and catch basins;</li>
          <li>Disrupted natural food chain due to sediment destruction of small stream organism habitat;</li>
          <li>Clogged fish/aquatic species gills and smothered eggs from high sediment levels; and</li>
          <li>Altered water flow and reduced water depth from sediment build-up.</li>
        </ul>
      </div>

      <?= $staff_card; ?>

    </div>
  </div>

</div>




<div class="row-section row-section_dark bg-color_grayblue">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <p>PLG works with numerous municipalities throughout Metro Vancouver to create practical and attainable solutions to ESC regulations. Each municipality must follow their own ESC bylaw which outlines mandatory site ESC planning standards, including the adoption of Best Management Practices (BMPs) to follow during active construction. BMPs are always a requirement on construction sites and help guide construction activities to ensure works are in compliance with ESC standards.</p>

      <p>Many municipalities in British Columbia are increasingly looking to Qualified Professionals like QEPs and BC Certified Erosion and Sediment Control Leads (BC-CESCL) to manage and monitor construction sites. We have qualified individuals on our team who have successfully completed the <a href="https://escabc.com/page/Course_CESCL" class="color_green" target="_blank">BC-CESCL training course</a> and are knowledgeable in local ESC legislation, site inspections, and ESC mitigation measures. Please contact us today to learn more about how our BC-CESCL’s can assist you with ESC implementation and monitoring at your construction site!</p>

    </div>
  </div>

</div>





<div id="esccm" class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Erosion and Sediment Control &&nbsp;Construction Monitoring</h2>
      <hr class="short-line">

      <p>Environmental construction monitoring ensures site specific designs are implemented on the ground and allows for modifications to address changing onsite conditions. As an environmental construction monitor, PLG’s QEPs mitigate the impacts of construction activity and provide innovative and cost effect solutions.</p>

      <p>PLG QEP’s are available to conduct ESC monitoring, as required by construction works for each proposed development. ESC monitoring can include on-site water sampling at ESC sites and lab analysis to determine if site discharge from construction is in compliance with ESC discharge standards.</p>

      <p>Turbidity (cloudiness) testing of water from a construction site can be monitored in the field using a hand-held turbidity meter. If turbidity is measured greater than 60 nephelometric turbidity units (NTU) in the field, the water sample must be sent to the lab for analysis to determine total suspended solids (TSS). Each municipality’s ESC Bylaw has a different threshold requirement for TSS which discharged water from a construction site must not exceed. For example, the City of Surrey ESC Bylaw specifies that construction sites are not allowed to discharge water above 75mg/L TSS. Water samples collected by a QEP from an ESC site can also be sent to a lab for analysis of various other water quality parameters (e.g., dissolved oxygen, phosphorous, copper). Lab results will show all tested parameters and indicate the level of each parameter found in the sample.</p>

      <p>Each parameter has a standard threshold, set by the BC Water Quality Guidelines (BCWQG), which should not be exceeded if ESC measures have been properly followed at a construction site. A sample measuring greater than the set BCWQG for any given parameter indicates that the minimum acceptable level of that parameter has been exceeded in the location the sample was taken from. This information can help inform the ESC monitor of the location where ESC compliance may not be met and allow the ESC monitor to focus their attention on this area during inspections.</p>

      <p>All ESC inspections are documented by an ESC supervisor using an online ESC reporting system. The ESC reporting system allowed qualified supervisors to self-manage their monitoring obligations under their ESC permits.</p>

    </div>
  </div>

</div>



<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Other Environmental Services – Floc Systems Inc. &&nbsp;Products</h2>
      <hr class="short-line">

      <p>Often times, ESC for a proposed development requires the use of special ESC products that are designed to address such requirements. Our affiliate group at Floc Systems Inc. specializes in treatment systems and flocculants (i.e., Tigerfloc products) to remove turbidity, heavy metals, and hydrocarbons from waste water. Our Floc Systems specialists are available to assist you with all of your ESC needs by providing efficient, cost effective solutions that meet ESC standards.</p>

    </div>
  </div>

</div>
