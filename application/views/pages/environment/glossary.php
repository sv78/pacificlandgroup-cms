<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<div class="glossary">

  <div class="glossary-symbols-collection">
    <!-- <a href="#a">a</a> -->
  </div>

  <hr class="short-line">


  <!-- A -->
  <div id="a" class="glossary-symbol">a</div>


  <h4 id="agricultural-land-commission">Agricultural Land Commission (ALC)</h4>
  <p>An independent tribunal dedicated to preserving agricultural land and encouraging farming in British Columbia.</p>

  <h4 id="agricultural-land-reserve">Agricultural Land Reserve (ALR)</h4>
  <p>A collection of agricultural land in British Columbia in which agriculture is recognized as the priority use and includes private and public lands that may be farmed, forested, or are vacant.</p>

  <h4 id="authorization">Authorization</h4>
  <p>A document giving permission or authority to conduct requested works relevant to a proposed development/project.</p>



  <!-- B -->
  <div id="b" class="glossary-symbol">b</div>


  <h4 id="bank-stabilization-erosion-protection">Bank Stabilization/Erosion Protection</h4>
  <p>Works undertaken to protect or armour a bank or shore from erosion and includes:</p>
  <ul>
    <li>Existing dike or erosion protection;</li>
    <li>Stream channel restoration or maintenance; and/or,</li>
    <li>Emergency erosion of flood protection works.</li>
  </ul>

  <h4 id="british-columbia-water-quality-guidelines">British Columbia Water Quality Guidelines (BCWQG)</h4>
  <p>A set of principles (approved, working and draft) developed to assess and manage the health, safety and sustainability of British Columbia’s aquatic resources, and for the protection of aquatic life, wildlife, agriculture, drinking water sources and recreation.</p>

  <h4 id="biodiversity-conservation-strategy">Biodiversity Conservation Strategy (BCS)</h4>
  <p>City of Surrey legislation adopted in 2016 aims to preserve, protect, and enhance Surrey’s biodiversity in the long-term by identifying sensitive areas [e.g., <a href="#green-infrastructure-network">Green Infrastructure Network</a> (GIN)] and habitat resources, and setting conservation and management criteria and recommending policy, procedures and monitoring programs that will support the initiatives outlined in the Strategy.</p>

  <h4 id="biofiltration">Biofiltration</h4>
  <p>Pollution control technique using a device containing living material to capture and biologically degrade pollutants.</p>

  <h4 id="bird-nest-survey">Bird/Nest survey</h4>
  <p>Birds and their nests protected under the Provincial <a href="#wildlife-act">Wildlife Act</a> (WA) and the Federal <a href="#migratory-birds-convention-act">Migratory Birds Convention Act</a> (MBCA) require a bird nest survey to be conducted by a <a href="#qualified-environmental-professional">Qualified Environmental Professional</a> (QEP) in advance of proposed vegetation clearing.</p>

  <h4 id="breeding-bird-season-bird-nesting-window">Breeding Bird Season/Bird Nesting Window</h4>
  <p>In BC, it is typically between March 1 and August 31.</p>



  <!-- C -->
  <div id="c" class="glossary-symbol">c</div>


  <h4 id="change-approval">Change Approval</h4>
  <p>Under Section 11 of the <a href="#water-sustainability-act">Water Sustainability Act</a> (WSA), a change approval is written authorization to make complex changes in and about a stream.</p>

  <!-- <h4 id="change-approval">College of Applied Biology (CAB)</h4>
  
  <p>An accreditation association with professionals members who meet high standards for entry into the College and the right to one of seven (7) membership categories including <a href="#registered-professional-biologist">Registered Professional Biologist (R.P.Bio.)</a>, Biologist in Training (B.I.T.), Registered Biology Technologist (R.B.Tech.), and Applied Biology Technician (A.B.T.). Members are the practitioners who take the theoretical knowledge of biology and apply it in a wide variety of fields to help manage and protect our natural resources to the benefit of the public.</p>
  
  -->

  <h4 id="compensation">Compensation</h4>
  <p>The replacement of natural habitat, typically 1:1 on-site and 2:1 off-site, to increase the productivity of existing habitat, or maintenance of fish production.</p>

  <h4 id="conservation-restrictive-covenant">Conservation/Restrictive Covenant (RC)</h4>
  <p>A covenant imposing a restriction on the use of land so that the value and enjoyment of adjoining land will be preserved.</p>

  <h4 id="construction-environmental-management-plan">Construction Environmental Management Plan (CEMP)</h4>
  <p>A document designed specific to a project, that outlines how a construction will avoid, minimize or mitigate effects on the environment and surrounding area.</p>



  <!-- D -->
  <div id="d" class="glossary-symbol">d</div>


  <h4 id="development-variance-permit">Development Variance Permit (DVP)</h4>
  <p>A legal document that allows the applicant to request a variance to a bylaw regulation (e.g., zoning bylaw, sign bylaw, subdivision and development servicing bylaw) for development purposes.</p>


  <!-- E -->
  <div id="e" class="glossary-symbol">e</div>


  <h4 id="ecogift-dedication">Ecogift Dedication</h4>
  <p>A portion of land within a property that is gifted to a qualified recipient (e.g., City), often to satisfy the <a href="#maximum-safeguarding">maximum safeguarding</a> requirements associated with an <a href="#ecosystem-development-plan">Ecosystem Development Plan</a> (EDP)/<a href="#sensitive-ecosystem-development-plan">Sensitive Ecosystem Development Plan</a> (SEDP).</p>

  <h4 id="ecological-baseline-assessment">Ecological Baseline Assessment</h4>
  <p>An initial site assessment that involves the collection of baseline environmental information (e.g., vegetation, watercourses).</p>

  <h4 id="ecosystem-development-plan">Ecosystem Development Plan (EDP)</h4>
  <p>A compilation of detailed reports as prepared by a <a href="#qualified-environmental-professional">Qualified Environmental Professional</a> (QEP) which evaluates the appropriateness of a development proposal. An EDP is required in order to request a development permit (DP), building permit (BP), etc., when development is occurring in an environmentally sensitive area.</p>

  <h4 id="ecosystem-protection-measures">Ecosystem Protection Measures</h4>
  <p>City of Surrey measures that support streamside protection as part of newly adopted <a href="#sensitive-ecosystem-development-permit">Sensitive Ecosystem Development Permit</a> (DP3) areas.</p>

  <h4 id="electrofishing">Electrofishing</h4>
  <p>Scientific survey method that uses an electrical current to safely sample fish populations. Electrofishing is typically a first step of instream work projects, to ensure a work area is safely isolated from fish in advance of instream work.</p>

  <h4 id="environmental-construction-monitoring-erosion-and-sediment-control-monitoring">Environmental Construction Monitoring/Erosion and Sediment Control (ESC) Monitoring</h4>
  <p>An environmental management practice implemented on construction projects for the purpose of monitoring construction works to ensure compliance with environmental regulations associated with sensitive areas.</p>

  <h4 id="environmental-assessment-environmental-impact-assessment">Environmental Assessment/Environmental Impact Assessment (EIA)</h4>
  <p>A process to evaluate the likely environmental consequences (positive and negative) of a proposed plan, policy, program or actual project prior to the decision to move forward with proposed action(s).</p>

  <h4 id="environmental-monitoring">Environmental Monitoring</h4>
  <p>Processes and activities that are carried out by an environmental technician or qualified professional and are required to monitor the quality of the environment throughout a project, or phase of a project.</p>

  <h4 id="environmental-protection-plan">Environmental Protection Plan (EPP)</h4>
  <p>A document that aims to ensure works associated with proposed development are in compliance with current environmental legislation and standards, for the protection of the environment.</p>

  <h4 id="erosion-and-sediment-control">Erosion and Sediment Control (ESC)</h4>
  <p>The practice of preventing stormwater contamination by sediment through controlling the amount of exposed soils during construction.</p>

  <h4 id="erosion-and-sediment-control-plan">Erosion and Sediment Control Plan (ESCP)</h4>
  <p>A document prepared in association with construction works which outlines specific measures to be taken to reduce the impacts of construction on water quality.</p>



  <!-- F -->
  <div id="f" class="glossary-symbol">f</div>


  <h4 id="farm-practices-protection-act">Farm Practices Protection (Right to Farm) Act (FPPA)</h4>
  <p>A Provincial piece of legislation enacted in British Columbia in 1996 which underpins efforts to protect current farm practices and protects a farmer’s right to farm within lands designated under the <a href="#agricultural-land-reserve">Agricultural Land Reserve</a> (ALR).</p>

  <h4 id="fisheries-act">Fisheries Act (FA)</h4>
  <p>A Federal piece of legislation enacted in Canada in 1985 and amended in April 2016 that aims to protect provincial fisheries. Recently proposed amendments (February 2018) aim to restore lost protections and incorporate modern safeguards for the protection of fish and fish habitat.</p>

  <h4 id="fisheries-and-oceans-canada">Fisheries and Oceans Canada (DFO)</h4>
  <p>A department within the government of Canada that is responsible for developing and implementing policies and programs in support of Canada’s economic, ecological and scientific interests in oceans and inland waters.</p>

  <h4 id="fish-and-fish-habitat-assessment">Fish and Fish Habitat Assessment</h4>
  <p>An environmental assessment that involves evaluation of fish and fish habitat to obtain an inventory of current fish species and determine habitat suitability for fish survival.</p>



  <!-- G -->
  <div id="g" class="glossary-symbol">g</div>


  <h4 id="green-infrastructure-areas">Green Infrastructure Areas (GIA)</h4>
  <p>Existing or potential areas of environmentally sensitive and/or unique natural areas that are consistent with the Biodiversity Management Areas and <a href="#green-infrastructure-network">Green Infrastructure Network</a> (GIN) and corresponding conditions and recommendations required for management as identified in Surrey’s <a href="#biodiversity-conservation-strategy">Biodiversity Conservation Strategy</a> (BCS). GIA include the GIN, which is comprised of hubs, sites, and corridors.</p>

  <h4 id="green-infrastructure-network">Green Infrastructure Network (GIN)</h4>
  <p>Interconnected system of natural areas and open space that conserves ecosystems and functions, while providing benefits to both wildlife and people. The GIN is comprised of hubs, sites and corridors.</p>



  <!-- H -->
  <div id="h" class="glossary-symbol">h</div>


  <h4 id="habitat-compensation-plan">Habitat Compensation Plan</h4>
  <p>An approved plan, which is designed to guide specific restoration and monitoring works associated with a project.</p>

  <h4 id="high-water-mark">High Water Mark (HWM)</h4>
  <p>The visible high water mark of a stream where the presence and action of the water are so common and usual, and so long continued in all ordinary years, as to mark on the soil of the bed of the stream a character distinct from that of its banks, in vegetation, as well as in the nature of the soil itself, and includes the active floodplain.</p>



  <!-- I -->
  <div id="i" class="glossary-symbol">i</div>


  <h4 id="impact-mitigation-plan">Impact Mitigation Plan (IMP)</h4>
  <p>A plan associated with a requested variance for a proposed development, which demonstrates the requested offset and outlines proposed improvement to adjacent areas in exchange for the reduced setback (i.e., variance).</p>

  <h4 id="instream-works">Instream Works</h4>
  <p>As defined in the <a href="#water-sustainability-act">Water Sustainability Act</a> (WSA), any changes in and about a stream that include:</p>
  <ul>
    <li>Any modification to the nature of a stream, including any modification to the land, vegetation and natural environment of a stream or the flow of water in a stream, or;</li>
    <li>Any activity or construction within a stream channel that has or may have an impact on a stream or stream channel.</li>
  </ul>


  <h4 id="instream-work-window">Instream Work Window</h4>
  <p>The best period(s) of time in the year to carry out <a href="#instream-works">instream works</a> in order to minimize the impact on species and habitats in each region.</p>

  <h4 id="integrated-stormwater-management-plan">Integrated Stormwater Management Plan (ISMP)</h4>
  <p>A document that describes a comprehensive, ecosystem-based approach to rainwater management by providing direction for future development plans and identifying infrastructure needs.</p>

  <h4 id="invasive-species-management-plan">Invasive Species Management Plan (ISMP)</h4>
  <p>A document focused on providing ecosystem specific objectives and strategies to aid in the management and monitoring of invasive species.</p>

  <h4 id="invasive-species-management">Invasive Species Management</h4>
  <p>Follows the Invasive Species Strategy for British Columbia and involves specialized weed management strategies to suppress invasive plant species on grazing land, conservation land and biologically diverse natural areas.</p>



  <!-- L -->
  <div id="l" class="glossary-symbol">l</div>


  <h4 id="local-government-act">Local Government Act (LGA)</h4>
  <p>A Provincial piece of legislation enacted in British Columbia in 2015 that aims to provide a legal framework and foundation for the establishment and continuation of local governments to represent the interests and respond to the needs of their communities.</p>



  <!-- M -->
  <div id="m" class="glossary-symbol">m</div>


  <h4 id="maximum-safeguarding">Maximum Safeguarding</h4>
  <p>Conveyance of a Protection Area (as outlined in a <a href="#sensitive-ecosystem-development-plan">Sensitive Ecosystem Development Plan</a>) to the City of Surrey. Where conveyance is chosen, the <a href="#sensitive-ecosystem-development-permit">Sensitive Ecosystem Development Permit</a> (DP3) applicant is not responsible for the additional ecological restoration or on-going maintenance of the Protection Area as described in the <a href="#minimum-safeguarding">minimum safeguarding</a> option.</p>

  <h4 id="migratory-birds-convention-act">Migratory Birds Convention Act (MBCA)</h4>
  <p>A Federal piece of legislation enacted in Canada in 1917 and significantly updated in June 1994 that contains regulations to protect migratory birds, their eggs, and their nests from hunting, trafficking and commercialization. As such, a permit is required to engage in any of these activities.</p>

  <h4 id="minimum-safeguarding">Minimum Safeguarding</h4>
  <p>Registration of a combined <a href="#restrictive-covenant">Restrictive Covenant</a> (RC)/<a href="#right-of-way">Right of Way</a> (ROW) against the property to ensure safeguarding and maintenance of the Protection Area in perpetuity. The Applicant is typically responsible for maintenance in an RC area for a pre-determine timeline.</p>

  <h4 id="ministry-of-forests-lands-natural-resource-operations-rural-development">Ministry of Forests Lands Natural Resource Operations & Rural Development (MFLNROD)</h4>
  <p>A branch within the government of British Columbia that is responsible for stewardship of <a href="#provincial-crown-land">Provincial Crown Land</a> and natural resources, and the protection of British Columbia’s archaeological and heritage resources.</p>



  <!-- N -->
  <div id="n" class="glossary-symbol">n</div>


  <h4 id="natural-environment-development-permit">Natural Environment Development Permit (NEDP)</h4>
  <p>A City of Abbotsford bylaw requirement that applies to all projects with proposed development within 50 metres of streams or land classified as sensitive ecosystems (as per Metro Vancouver’s Sensitive Ecosystem Inventory).</p>

  <h4 id="notification">Notification</h4>
  <p>Under Section 11 of the <a href="#water-sustainability-act">Water Sustainability Act</a> (WSA), a Notification is used for specified low risk changes in and about a stream that have minimal impact on the environment or third parties.</p>

  <h4 id="nephelometric-turbidity-unit">Nephelometric Turbidity Unit (NTU)</h4>
  <p>A unit measuring the lack of clarity of water (i.e., turbidity), often used in freshwater studies with a “turbidimeter” to analyze samples of water taken from the assessment area.</p>



  <!-- P -->
  <div id="p" class="glossary-symbol">p</div>


  <h4 id="p-15-agreement">P-15 Agreement</h4>
  <p>As part of the City of Surrey’s policy number P-15, all riparian planting completed as part of habitat compensation works for a proposed development are required to adhere to a P-15 Agreement, which includes <a href="#p-15-monitoring">P-15 monitoring</a>.</p>

  <h4 id="p-15-monitoring">P-15 Monitoring</h4>
  <p>Yearly monitoring, as a requirement of a <a href="#p-15-agreement">P-15 Agreement</a>, of riparian plantings and planting area by a QEP for a project-specific duration defined by the City (typically 5 year term).</p>

  <h4 id="permit">Permit</h4>
  <p>An official document that provides the applicant with authorization or consent from the authorizing body to do something/carry out requested work (e.g., Building Permit, Erosion and Sediment Control Permit, Tree Cutting Permit).</p>

  <h4 id="post-construction-monitoring">Post-construction Monitoring</h4>
  <p>One or more site visits completed by a <a href="#qualified-environmental-professional">Qualified Environmental Professional</a> (QEP) following the completion of a project to check the integrity and functionality of newly installed/established features (e.g., riparian vegetation) and to ensure effectiveness and compliance with regulatory guidelines. Assessments are completed to ensure accordance with prescribed riparian/landscape planting plans and to certify the release of project bonding.</p>

  <h4 id="provincial-crown-land">Provincial Crown Land</h4>
  <p>Land (including land covered by water, like rivers or lakes) that is owned by the provincial government and is available to the public for many different purposes (e.g., industry, recreation, research).</p>



  <!-- Q -->
  <div id="q" class="glossary-symbol">q</div>


  <h4 id="qualified-environmental-professional">Qualified Environmental Professional (QEP)</h4>
  <p>An applied scientist or technologist who is registered and in good standing with an appropriate B.C. professional organization constituted under an Act. The QEP must be acting under that association’s code of ethics, and subject to the organization's disciplinary action. A qualified environmental professional could be a <a href="#registered-professional-biologist">Registered Professional Biologist</a>, Agrologist, Forester, Geoscientist, Engineer, or Technologist.</p>



  <!-- R -->
  <div id="r" class="glossary-symbol">r</div>


  <h4 id="registered-professional-biologist">Registered Professional Biologist (R.P.Bio.)</h4>
  <p>A specific <a href="#qualified-environmental-professional">Qualified Environmental Professional</a> (QEP) designation acting and conducting professional works in accordance with the code of ethics set by the College of Applied Biology.</p>

  <h4 id="remediation">Remediation</h4>
  <p>The action of remedying lost, destroyed or impacted environmental areas, and reversing or preventing additional environmental damage.</p>

  <h4 id="right-of-way">Right of Way (ROW)</h4>
  <p>The legal right, established by usage or grant, to pass along a specific route through grounds or property belonging to another (e.g., BC Hydro ROW).</p>

  <h4 id="riparian-areas-regulation">Riparian Areas Regulation (RAR)</h4>
  <p>A provincial legislation that requires local governments in applicable areas to protect riparian areas during residential, commercial, and industrial development.</p>

  <h4 id="riparian-areas-regulation-assessment">Riparian Areas Regulation (RAR) Assessment</h4>
  <p>An environmental procedure carried out in the field by a <a href="#qualified-environmental-professional">Qualified Environmental Professional</a> (QEP) to determine the applicable <a href="#streamside-area-streamside-protection-and-enhancement-area">Streamside Protection and Enhancement Area</a> (SPEA) within a property, which represents the development setback required to prevent degradation of on-site fish habitat.</p>

  <h4 id="riparian-areas-regulation-notification-system">Riparian Areas Regulation Notification System (RARNS)</h4>
  <p>An online database for <a href="#qualified-environmental-professional">Qualified Environmental Professionals</a> (QEPs) to submit RAR Assessment Reports as per section 4(2) (b) of the <a href="#riparian-areas-regulation">Riparian Areas Regulation</a> (RAR).</p>

  <h4 id="riparian-landscape-planting-plan">Riparian/Landscape Planting Plan</h4>
  <p>A detailed figure that outlines a list of recommended native plant species for riparian habitat and depicts recommended location and spacing of each within the designated riparian/landscape planting area.</p>



  <!-- S -->
  <div id="s" class="glossary-symbol">s</div>


  <h4 id="sensitive-ecosystem-areas">Sensitive Ecosystem Areas</h4>
  <p>Comprised of riparian or <a href="#green-infrastructure-areas">Green Infrastructure Area</a> (GIA) that are environmentally sensitive or unique natural areas such as streams, watercourses, cliffs, escarpments, forests, geological formations, wildlife habitats and wetlands.</p>

  <h4 id="sensitive-ecosystem-development-permit">Sensitive Ecosystem Development Permit (DP3)</h4>
  <p>An application is required for a property (within Surrey) which involves any of the following proposed activities within 50 metres of mapped and <a href="#qualified-environmental-professional">Qualified Environmental Professional</a> (QEP) confirmed sensitive ecosystems:</p>
  <ul>
    <li>Subdivision of land</li>
    <li>Construction, addition or alteration of a building or structure</li>
    <li>Construction of roads and trails</li>
    <li>Disturbance of soils, land alterations or land clearing</li>
    <li>Installation of non-structural surfaces with semi-pervious or impervious materials</li>
  </ul>

  <h4 id="sensitive-ecosystem-development-plan">Sensitive Ecosystem Development Plan (SEDP)</h4>
  <p>A compilation of detailed reports as prepared by a <a href="#qualified-environmental-professional">Qualified Environmental Professional</a> (QEP) which evaluates the appropriateness of a development.</p>

  <h4 id="species-at-risk">Species at Risk</h4>
  <p>A list of Federally-protected flora or fauna as determined by the government.</p>

  <h4 id="species-at-risk-act">Species at Risk Act (SARA)</h4>
  <p>A Federal piece of legislation enacted in Canada in 2002 with the purposes of preventing wildlife species in Canada from disappearing, providing the recovery of wildlife species that are extirpated (no longer exist in the wild in Canada), endangered, or threatened as a result of human activity, and managing species of special concern to prevent them from becoming endangered or threatened. This Act covers all wildlife species listed as being at risk nationally (and their critical habitats).</p>

  <h4 id="stormwater-management">Stormwater Management</h4>
  <p>Consists of detaining, retaining, or providing a discharge point for stormwater to be reused or infiltrated into the groundwater, typically associated with detailed design plans prepared by a qualified civil engineer.</p>

  <h4 id="streamside-area-streamside-protection-and-enhancement-area">Streamside Area/Streamside Protection and Enhancement Area (SPEA)</h4>
  <p>An area</p>
  <ul>
    <li>Adjacent to a stream that links aquatic to terrestrial ecosystems and includes both existing and potential riparian vegetation and existing and potential adjacent upland vegetation that exerts an influence on the stream, and</li>
    <li>The size of which is determined according to the <a href="#riparian-areas-regulation">Riparian Areas Regulation</a> (RAR) on the basis of an assessment report provided by a <a href="#qualified-environmental-professional">Qualified Environmental Professional</a> (QEP) in respect of a development proposal.</li>
  </ul>

  <h4 id="streamside-setback-area">Streamside Setback Area</h4>
  <p>A minimum required distance from <a href="#top-of-bank">Top of Bank</a> (TOB) of a stream, confirmed by a <a href="#qualified-environmental-professional">Qualified Environmental Professional</a> (QEP), where proposed development and any associated works must not occur in.</p>



  <!-- T -->
  <div id="t" class="glossary-symbol">t</div>


  <h4 id="top-of-bank">Top of Bank (TOB)</h4>
  <p>A field measurement from:</p>
  <ul>
    <li>The point closest to the boundary of the active floodplain of a stream where a break in the slope of the land occurs such that the grade beyond the break is flatter than 3:1 at any point for a minimum distance of 15 metres measured perpendicularly from the break, and</li>
    <li>The edge of the active floodplain of a stream (for a floodplain not contained in a ravine) where the slope of the land beyond the edge is flatter than 3:1 at any point for a minimum distance of 15 metres measured perpendicularly from the edge.</li>
  </ul>

  <h4 id="total-suspended-solid">Total Suspended Solid (TSS)</h4>
  <p>Solid materials that are suspended in water (i.e., not dissolved) and can be trapped by a filter and analyzed using a filtration apparatus.</p>



  <!-- W -->
  <div id="w" class="glossary-symbol">w</div>

  <h4 id="watercourse-assessment">Watercourse Assessment</h4>
  <p>A field procedure carried out by a <a href="#qualified-environmental-professional">Qualified Environmental Professional</a> (QEP) to confirm existing and identify any new on-site watercourse locations and confirm applicable <a href="#watercourse-stream-classification">watercourse classifications</a> and associated developments setbacks.</p>

  <h4 id="watercourse-stream-classification">Watercourse/Stream Classification</h4>
  <p>A procedure carried out by a <a href="#qualified-environmental-professional">Qualified Environmental Professional</a> (QEP) which assigns a specific letter code to an assessed stream, based on specific environmental characteristics (e.g., overhanging vegetation type, substrate type) for the purposes of determining the <a href="#streamside-setback-area">streamside setback area</a>.</p>

  <h4 id="watercourse-stream-de-classification">Watercourse/Stream De-classification</h4>
  <p>A formal assessment that involves an application to change the existing mapped stream classification to a lower classification level (e.g., Class B to Class C) based on visual inspection by a <a href="#qualified-environmental-professional">Qualified Environmental Professional</a> (QEP) of specific environmental characteristics (e.g., overhanging riparian vegetation, substrate type).</p>

  <h4 id="water-quality">Water Quality</h4>
  <p>The chemical, physical, biological and radiological characteristics of water. It is a measure of the condition of water relative to the requirements of one or more biotic species and or to any human need or purpose.</p>

  <h4 id="water-sustainability-act">Water Sustainability Act (WSA)</h4>
  <p>A Provincial piece of legislation enacted in British Columbia in 2016 to ensure a sustainable supply of fresh, clean water is available that meets the needs of British Columbia residents today and in the future.</p>

  <h4 id="weed-control-act">Weed Control Act</h4>
  <p>A Provincial piece of legislation enacted in British Columbia in 1996 that outlines regulations and guidelines to control noxious weeds.</p>

  <h4 id="wildlife-act">Wildlife Act (WA)</h4>
  <p>A Provincial piece of legislation enacted in British Columbia in 1996 that protects virtually all vertebrate animals from direct harm, except as allowed by regulation (e.g., hunting or trapping). This Act can also be used to authorize direct management of wildlife (e.g., translocation, predator control) or human activities (e.g., recreational vehicle closures) where it is necessary to achieve recovery objectives.</p>

  <h4 id="wildlife-salvage">Wildlife Salvage</h4>
  <p>Removal of certain protected wildlife species (e.g., amphibians, fish, small mammals) from an area prior to construction works. Under Section 29 of the <a href="#wildlife-act">Wildlife Act</a> (WA), wildlife salvage requires a permit from the Ministry of Environment (MOE).</p>

  <h4 id="wildlife-survey">Wildlife Survey</h4>
  <p>A field procedure carried out by a <a href="#qualified-environmental-professional">Qualified Environmental Professional</a> (QEP) that involves the capturing and sampling (e.g., tagging) of wildlife species, in accordance with proper capture and sampling guidelines, to determine species diversity and abundance in a sampling area.</p>

</div>

<script>
  var glossaryLetters = document.querySelectorAll('.glossary-symbol');
  for (var i = 0; i < glossaryLetters.length; i++) {
    var a = document.createElement('a');
    a.innerHTML = glossaryLetters[i].id;
    a.setAttribute('href', "#" + glossaryLetters[i].id);
    document.querySelector('.glossary-symbols-collection').appendChild(a);
  }
</script>
