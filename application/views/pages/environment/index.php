<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Qualified Environmental Professionals -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_environment-a">
    <h1 class="row-section__bg-header">Environment</h1>
  </div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h2 class="h-simple">Qualified Environmental Professionals</h2>
      <hr class="short-line">
      <p>A Qualified Environmental Professional (QEP) refers to an applied scientist or technologist, including a registered professional Biologist, Agrologist, Forester, Geoscientist, Engineer, or Technologist, who must provide services within their registered area of expertise. The QEP must be registered and in good standing with an appropriate B.C. professional organization constituted under an Act. The QEP must also adhere to their association’s code of ethics, and is subject to their organization’s disciplinary action.</p>
      <p>PLG’s QEP staff are dedicated to help clients finish their jobs efficiently while being in compliance with all the mandated requirements.</p>


      <?= $staff_card; ?>

    </div>
  </div>

</div>



<div class="row-section row-section_dark bg-color_grayblue">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Construction and Development Environmental Services/Regulatory Compliance</h2>
      <hr class="short-line">

      <h4>Construction Environmental Management Plans</h4>
      <p>Our team of skilled environmental scientists have prepared more than 300 Construction Environmental Management Plans (CEMPs), for a variety of clients (infrastructure, mining, development, linear construction). A CEMP is typically a requirement for subcontractors reporting to a prime contractor or consultant to outline how they plan to maintain compliance with environmental regulations during construction activities.</p>

      <h4>Environmental Construction Monitoring</h4>
      <p>Environmental construction monitoring ensures site specific designs are implemented on the ground and allows for modifications to address changing onsite conditions. As an environmental construction monitor, we mitigate the impacts of construction activity and provide innovative and cost effect solutions.</p>

      <h4>Erosion and Sediment Control &&nbsp;Monitoring</h4>
      <p>PLG works with numerous municipalities throughout Metro Vancouver to create practical and attainable solutions to sediment control regulations. We provide detailed, staged, and comprehensive storm water management plans for development of all sizes, for the purposes of biofiltration and stormwater control. PLG QEP’s are specialized to conduct Erosion and Sediment Control (ESC) monitoring, as required by construction works for each proposed development.</p>
      <p>Our affiliate group at Floc Systems Inc. specializes in treatment systems and flocculants (i.e., Tigerfloc products) to remove turbidity, heavy metals, and hydrocarbons from waste water.</p>

      <h4>P-15 Monitoring</h4>
      <p>As part of the City of Surrey’s policy number P-15, all riparian planting completed as part of habitat compensation works for proposed development are required to adhere to a P-15 Agreement. The P-15 Agreement requires yearly monitoring of plantings and planting area by a QEP for a project-specific duration defined by the City (typically a 5 year term).</p>

    </div>
  </div>

</div>


<?= $tile_menu; ?>




<div class="row-section row-section_dark bg-color_grayblue">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Covenants/Dedications</h2>
      <hr class="short-line">

      <h4>Conservation/Restrictive Covenant or Right-of-Way</h4>
      <p>To fulfill the minimum safeguarding requirements of an SEDP, registration of a combined Conservation/Restrictive Covenant or Right-of-Way against the property can be completed to ensure safeguarding and maintenance of the Protection Area in perpetuity. The RC/ROW will identify “no disturbance” and “maintenance access” areas within the property, provisions for post-construction management and monitoring (e.g., P-15 Agreement), and a compensation plan to determine landscape bonding and security requirements for installation and maintenance purposes.</p>

      <h4>Ecogift Dedication</h4>
      <p>To fulfill the maximum safeguarding requirements of an SEDP, a portion of land within the Subject Property may be dedicated to a qualified recipient as an ecological gift. PLG’s QEPs are able to determine whether an area of land qualifies as ecologically sensitive and help the client determine a qualified recipient for the gifted land. Ecologically sensitive lands are areas or sites that currently or could, at some point in the future, contribute significantly to the conservation of Canada’s biodiversity and environmental heritage. Where ecologically sensitive land is a significant part of a larger parcel of land, the entire property donated usually qualifies as an ecological gift. Any land that is given as an ecological gift is fully protected from future development.</p>

    </div>
  </div>

</div>



<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">



      <h2 class="h-simple">Ecosystem Development Plans / Sensitive Ecosystem Development Plans (EDP/SEDP)</h2>
      <hr class="short-line">

      <p>Local governments have adopted new bylaws which affect the way projects are allowed to be developed within 50 m of a regulated watercourse (or ditch). These new bylaws will consider not only potential fish habitat, but will consider additional features such as wildlife corridors, public access routes, and other natural features.</p>

      <p>Our QEPs are able to take a client through the environmental development process which includes a combination of a slope stability assessment, a review of Biodiversity Conservation Strategies and protected areas, a review of topographic and arborist survey reports, and consideration of flood plains, drainage access routes and trail access.</p>

      <h4>Example&nbsp;– <span class="color_blue">City of Surrey Ecosystem Protection Measures</span></h4>

      <p>The City of Surrey adopted new Ecosystem Protection Measures (i.e. Sensitive Ecosystem Development Permit (DP3) Areas and Streamside Areas Protection Setback Zoning By-law) on September 12, 2016. For developers, this means that for any proposed development within 50 m of the City of Surrey’s GIN or any regulated watercourse, completion of a DP3 will be required in advance of proposed development works. According to the new Ecosystem Protection Measures, sensitive ecosystems consist of two distinct areas:</p>
      <ul>
        <li><strong>Green Infrastructure Areas</strong> (identified in the Biodiversity Conservation Strategy as Green Infrastructure Network (GIN) comprised of hubs, sites, and corridors)</li>
        <li><strong>Streamside Areas</strong> (identified in the BCS and defined in Bylaw No. 12000 7A) adjacent to and setbacks from a stream</li>
      </ul>


      <h4>Sensitive Ecosystem Development Permits (DP3)</h4>

      <p>A Sensitive Ecosystem Development Permit (DP3) Application is required for a property (within Surrey) involves any of the following proposed activities within 50 meters of mapped and QEP confirmed sensitive ecosystems:</p>

      <ul>
        <li>Subdivision of Land</li>
        <li>Construction, addition or alteration of a building or structure</li>
        <li>Construction of roads and trails</li>
        <li>Disturbance of soils, land alteration or land clearing</li>
        <li>Installing non-structural surfaces with semi-previous or impervious materials</li>
      </ul>

      <p>Through development permit guidelines, as described in the OCP and Section 7A of the Zoning By-law, a Development Variance Permit (DVP) will be required if the proposed activity involves varying the By-law’s recommended setbacks for a watercourse or Green Infrastructure Area (GIA). To obtain a DVP approval, additional reports, including an Impact Mitigation Plan (IMP) will be required to provide options and mitigation measures to minimize project effects to the satisfaction of City staff and council. Both DP3 and IMPs are required to be prepared and approved by a QEP (e.g. R.P.Bio), and PLG is available to provide appropriate government liaising as part of the DP3 application process.</p>

    </div>
  </div>

</div>





<div class="row-section row-section_dark bg-color_grayblue">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Environmental Assessments, Salvages &&nbsp;Surveys</h2>
      <hr class="short-line">

      <h4>Ecological Baseline Assessments/Environmental Impact Assessments</h4>
      <p>PLG is committed to supporting existing natural systems, by providing opportunities for habitat retention or enhancement. Habitat and ecological baseline assessments of fish and wildlife are fundamental in sustainable planning. Our assessment work focuses on the prevention of negative impacts to aquatic and terrestrial species and their habitats. Our Environmental Resource Division has the expertise to conduct thorough field analyses [e.g., Environmental Impact Assessments (EIA)], in accordance with governmental regulations and land use planning requirements.</p>

      <h4>Riparian Areas Regulation (RAR) Assessments</h4>
      <p>Watercourses and wetlands are subject to federal and provincial government regulations (e.g., Streamside Protection and Enhancement Area Regulation). PLG conducts watercourse and wetland setback assessments to determine setbacks that are regulated by different levels of government protection standards. Our QEPs are able to assess and determine appropriate setbacks and provide a written assessment summary to our clients. A copy of the RAR report is also electronically filed by the QEP into the provincial Riparian Areas Regulation Notification System (RARNS), an online reference database for each professionally assessed watercourse/waterbody.</p>

      <h4>Fish and Fish Habitat/Watercourse Assessments</h4>
      <p>PLG QEP’s are experienced in conducting watercourse assessments throughout western Canada, in accordance with local regulatory requirements and guidelines (e.g. RAR or Forest Practice Code). Our biologists and technicians are also certified and experienced to perform fish, amphibian and wildlife (e.g., Pacific Water Shrew) salvages and relocation using various techniques (e.g., electrofishing, minnow traps, fencing), in accordance with applicable federal/provincial legislation (e.g., Federal Fisheries Act).</p>

      <h4>Bird/Nest Surveys</h4>
      <p>The BC <span class="_italic">Wildlife Act (WA)</span> and the Federal <span class="_italic">Migratory Birds Convention Act (MBCA)</span> provide protection for native birds, their nests and young from disturbance or harm during the recognized bird breeding season. The nests of some birds (e.g., heron, bald eagle, osprey) are protected year-round, whether active or not.</p>

      <p>Please note, the provincially recognized bird nesting period for most birds, from nest-building through to fledging, is <span class="color_blue">March 1 to September 30</span>, as indicated by the BC Develop with Care Best Management Practices Guidelines.</p>

      <p>Our professional biologists and QEPs are able to conduct bird nests surveys and determine whether nests are active/inactive and will provide a detailed report outlining buffers (if required), exclusion zones, etc.</p>


      <h4>Terrestrial and Wildlife Surveys</h4>
      <p>A terrestrial or wildlife habitat survey may be a requirement of your project. For projects in the initial planning stages, our team can conduct baseline desktop surveys and provide guidance in advance of any project planning. At the forefront of a project, our team can conduct ground truthing surveys utilizing the results of desktop research to assist our clients in determining next step approaches to confirm wildlife protection measures are met and maintained for a project.</p>

    </div>
  </div>

</div>





<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Natural Environment Development Permit (NEDP)</h2>
      <hr class="short-line">
      <p>Similar to the City of Surrey, the City of Abbotsford government has also adopted new bylaws which affect the way projects are allowed to be developed within 50m of streams or land classified as sensitive ecosystems (as per Metro Vancouver’s Sensitive Ecosystem Inventory). These new bylaws (outlined in the NEDP guidelines) will consider not only potential fish habitat, but will consider additional features such as wildlife corridors, public access routes, and other natural features within the City of Abbotsford.</p>

    </div>
  </div>
</div>










<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Permitting &&nbsp;Approvals</h2>
      <hr class="short-line">
      <p>Our biologists are aware that any delay to our clients can have significant impacts on project budgets and timelines. We help our clients determine which specific permits and approvals may be required for a proposed development, in order to allow seamless transition through each phase of the project. Some proposed development activities that may require permits include, but are not limited to:</p>

      <ul class="color_blue _bold">
        <li>Instream works
          <ul class="color_darky _normal">
            <li>Bank stabilization</li>
            <li>Fish and/or wildlife salvage</li>
            <li>Watercourse crossing construction (e.g., bridge)</li>
            <li>Watercourse relocation/infilling</li>
          </ul>
        </li>
        <li>Soil excavation</li>
        <li>New construction (e.g., site grading)</li>
        <li>Tree removal</li>
      </ul>


      <h4>Example&nbsp;– <span class="color_blue">Water Sustainability Act Approval and Notification</span></h4>

      <p>The <span class="_italic">Water Sustainability Act (WSA)</span> was brought into effect on February 29, 2016, as an update to the Water Act, to ensure a sustainable supply of fresh, clean water that meets the needs of B.C. residents is available today and in the future.</p>

      <p>Under Section 11 of the <i>WSA</i>, a Change Approval is required if an applicant wishes to request permission to make changes (e.g., closure), modifications or, significantly re-locate an existing waterbody. Applicable works are subject to pre-determined fisheries/stream compensation and/or enhancement works, to off-set proposed changes/modifications to the existing waterbody. The terms and conditions may relate to the time of year in which you may do the work, and/or to other measures that protect the aquatic ecosystem, the hydraulic integrity of the stream channel and the rights of water users and landowners downstream. Section 11 WSA Approval applications are submitted to FrontCounter BC, where it will be reviewed by the Water Manager and typically take between 6 months to 1 year to be approved. Approval applications are often conditional to a certain season when proposed work may occur (e.g., instream work windows).</p>

      <p>Under the same legislation, a Notification may be submitted for projects where changes to an existing waterbody are considered low risk changes with minimal impact to the environment. Work must meet requirements as governed by the <i>WSA</i> and comply with conditions and best practices as determined by a habitat officer in response to a Notification for work. Notifications are submitted to FrontCounter BC a minimum of 45 days ahead of proposed work (submitting more than 45 days ahead of time is recommended). A response must be received from an approving officer before work may proceed; however, if no response from the Ministry is received within 45 days of FrontCounter BC receiving the Notification application then proposed work may proceed, providing it meets the terms and conditions outlined in Part 3 of the Water Sustainability Regulation.</p>


    </div>
  </div>
</div>






<div class="row-section row-section_dark bg-color_darky">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Remediation/Reclamation</h2>
      <hr class="short-line">

      <h4>Invasive Species/Weed Management</h4>
      
      <p>According to the provincial <i>Weed Control Act</i> and local municipal requirements, invasive plant species are required to be managed and properly disposed within the disturbed sites. Our biologists/QEPs are prepared to work closely with our clients to determine a management solution for invasive species. PLG’s QEPs have prepared invasive species/weed control plans for many construction projects. Control plans may include manual removal and/or chemical treatment, dependent on the type and extent of each species. Plans will be designed project-specific in order to meet the needs of the client and regulatory criteria. PLG staff also have experience in providing guidance and conducting on-site weed removal and disposal.</p>

      <h4>Post-Construction Assessment/Monitoring</h4>
      
      <p>Following the completion of the project, PLG’s QEPs perform post-construction monitoring to check the functionality of the newly installed features (e.g. riparian vegetation, ponds) and to ensure effectiveness and compliance with the regulatory guidelines. PLG’s QEPs also conduct post-construction assessments to inspect the integrity of the newly established ecological communities (e.g. watercourse or riparian habitat) follow prescribed riparian/landscape planting plans and to certify the release of project bonding.</p>

      <h4>Surface Reclamation and Re-Vegetation</h4>
      <p>PLG Staff provide clients with reclamation services including habitat restoration plan preparation, re-vegetation plan preparation, and follow up monitoring. We are available and qualified to supervise and install riparian planting projects.</p>

    </div>
  </div>
</div>