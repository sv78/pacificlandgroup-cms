<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Qualified Environmental Professionals -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_environment-environmental-assessments">
    <!-- <h1 class="row-section__bg-header">Environmental Assessments</h1> -->
  </div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h2 class="h-simple">Environmental Assessments</h2>
      <hr class="short-line">
      <p>PLG is committed to supporting existing natural systems, by providing opportunities for habitat retention or enhancement. Ecological baseline and habitat assessments of fish and wildlife are fundamental in sustainable development planning. PLG’s environmental assessment work focuses on the identification, evaluation and prevention of negative impacts to aquatic and terrestrial species and their habitats. PLG’s QEPs have the expertise to conduct thorough field analyses [e.g., Environmental Impact Assessments (EIA)], in accordance with governmental regulations and land use planning requirements.</p>

      <p>Please visit the <a href="/environment/environmental-case-studies" class="color_blue">Environmental Case Studies</a> page to read more about our <a href="/environment/environmental-case-studies#fergus-creek" class="color_blue">Fergus Creek Success Story</a> which involved a detailed on-site environmental assessment.</p>

      <?= $staff_card; ?>

    </div>
  </div>

</div>





<div class="row-section row-section_dark bg-color_grayblue">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">


      <h2 class="h-simple">Watercourse Classification Confirmation Assessments</h2>
      <hr class="short-line">

      <p>PLG QEP’s are experienced in conducting Watercourse Classification Confirmation Assessments (WCCA) throughout western Canada, in accordance with local regulatory requirements and guidelines. The WCCA consists of a field assessment to evaluate biophysical conditions, determination of appropriate setbacks (if any), and preparation of a report that summarizes on-site observations and provides recommendations for potential/proposed development purposes. A WCCA provides the client with valuable setback information for development purposes, and can be used as a first step in the Sensitive Ecosystem Development Plan (SEDP) process.</p>



    </div>
  </div>

</div>



<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Riparian Areas Regulation Assessments</h2>
      <hr class="short-line">

      <p>In addition to municipal Streamside Protection and Enhancement Area (SPEA) Regulation bylaws and setbacks, watercourses and wetlands may also be subject to federal and provincial government regulations [e.g., Riparian Areas Regulation (RAR)]. PLG’s QEPs are qualified to conduct RAR assessments for watercourses and wetlands to determine setbacks that are regulated by provincial protection standards. Our QEPs are able to perform field assessments, in accordance with RAR assessment methods and guidelines, to determine appropriate setbacks and provide a written summary (i.e., RAR assessment report) to our clients. A copy of the RAR assessment report is also electronically filed by the QEP into the provincial Riparian Areas Regulation Notification System (RARNS), an online reference database for each professionally assessed watercourse/waterbody in BC.</p>

    </div>
  </div>

</div>





<div class="row-section row-section_dark bg-color_darky">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Species at Risk Assessments/Surveys/Salvage</h2>
      <hr class="short-line">
      <p>PLG’s environmental biologists and technicians are certified and experienced to perform Species at Risk assessments for fish, amphibian and wildlife (e.g., Pacific Water Shrew), including salvages and relocation using various techniques (e.g., electrofishing, minnow traps, fencing), in accordance with applicable federal/provincial legislation (e.g., Federal <i>Fisheries Act</i>).</p>

    </div>
  </div>
</div>





<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Bird Nest Surveys</h2>
      <hr class="short-line">

      <p>The BC <i>Wildlife Act (WA)</i> and the Federal <i>Migratory Birds Convention Act (MBCA)</i> provide protection for native birds, their nests and young from disturbance or harm during the recognized bird breeding season. The nests of some birds (e.g., heron, bald eagle, osprey) are protected year-round, whether active or not.</p>

      <p>The provincially recognized bird nesting period for most birds, from nest-building through to fledging, is <span class="color_blue">March 1 to September 30</span>, as indicated by the BC Develop with Care Best Management Practices Guidelines.</p>

      <img class="img-full-width" src="/assets/uploads/pages/environment/environmental-assessments/calendar.jpg" alt="Bird Nesting Calendar">
      <p class="image-description">*Colour range indicates percentage of total species per habitat actively nesting at any given date. Blue markers show extreme dates predicted for nesting season.</p>

      <p>Our professional biologists and QEPs are able to conduct bird nests surveys and determine whether nests are active/inactive and will provide a detailed report outlining buffers (if required), exclusion zones, etc.</p>

    </div>
  </div>

</div>






<div class="row-section row-section_dark bg-color_noble-blue">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Terrestrial and Wildlife Surveys</h2>
      <hr class="short-line">
      <p>A terrestrial or wildlife habitat survey may be a requirement of your project. For projects in the initial planning stages, our team of biologists are qualified to conduct baseline desktop surveys and provide guidance in advance of any project planning. At the forefront of a project, PLG’s QEPs can conduct ground truthing terrestrial surveys utilizing the results of desktop research to assist our clients in determining next step approaches to confirm wildlife protection measures are met and maintained for a project.</p>


    </div>
  </div>
</div>





<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Sensitive Ecosystem Development Plans</h2>
      <hr class="short-line">

      <p>In response to the City of Surrey adopting new bylaws which affect the way certain development projects are allowed to proceed, a large component of PLGs environmental work includes the preparation of Sensitive Ecosystem Development Plans (SEDP). New bylaws require a Sensitive Ecosystem Development Permit (DP3) Application approval, from the City of Surrey, for any proposed development activity within 50 m of a regulated watercourse (or ditch). These new bylaws will consider not only potential fish habitat and streamside areas, but will consider additional features such as wildlife biodiversity corridors and public access routes with respect to development activities.</p>

      <p>PLG’s environmental team is highly versed in the City of Surrey’s environmental bylaws and updated Ecosystem Protection Measures (i.e., DP3 Areas and Zoning Bylaw Streamside Protection Setback), as well as other local government policies, bylaws and regulations, and is committed to helping you achieve environmental compliance in connection with your development goals. Our QEPs are able to take you through the environmental development process, which may include assistance with determining what environmental requirements are applicable to your property (e.g., slope stability assessment, arborist assessment), management of any necessary permits and approvals by elected officials, and preparation of applications and reports (e.g., SEDP) for submission to governing bodies.</p>


    </div>
  </div>

</div>







<div class="row-section row-section_dark bg-color_darky">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Natural Ecosystem Development Plans</h2>
      <hr class="short-line">
      <p>Similar to the City of Surrey, the City of Abbotsford government has also adopted new bylaws which affect the way projects are allowed to be developed within 50 m of streams or land classified as sensitive ecosystems (as per Metro Vancouver’s Sensitive Ecosystem Inventory). These new bylaws (outlined in the NEDP guidelines) will consider not only potential fish habitat, but will consider additional features such as wildlife corridors, public access routes, and other natural features within the City of Abbotsford.</p>

    </div>
  </div>
</div>