<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Qualified Environmental Professionals -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_environment-permits-and-approvals"></div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h2 class="h-simple">Environmental and Development Permits</h2>
      <hr class="short-line">
      <p>PLG’s QEPs are aware that any delay to our clients can have significant impacts on project budgets and timelines. We help our clients determine which specific development and environmental permits and approvals may be required for a proposed development, in order to allow seamless transition through each phase of the project. Some proposed development activities that may require a development permit and/or environmental permit include, but are not limited to:</p>

      <ul class="color_blue _bold">
        <li>
          Instream works
          <ul class="color_darky _normal">
            <li>Bank stabilization (e.g., rip-rap armouring)</li>
            <li>Fish and/or wildlife salvage/collection</li>
            <li>Watercourse crossing construction (e.g., bridge)</li>
            <li>Watercourse relocation/infilling</li>
          </ul>
        </li>
        <li>Soil excavation/removal</li>
        <li>New construction (e.g., site grading/soil deposit)</li>
        <li>Tree removal/cutting</li>
      </ul>

      <?= $staff_card; ?>

    </div>
  </div>

</div>





<div class="row-section row-section_dark bg-color_noble-blue">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <p>The City of Surrey adopted new Ecosystem Protection Measures (i.e. Sensitive Ecosystem Development Permit (DP3) Areas and Streamside Areas Protection Setback Zoning By-law) on September 12, 2016. For developers, this means the preparation of a Sensitive Ecosystem Development Plan (SEDP), in association with a DP3 application, will be required in advance of proposed development works for a property (within Surrey) which involves any of the following proposed activities within 50 meters of mapped and QEP confirmed sensitive ecosystems (i.e., City of Surrey’s GIN or any regulated watercourse):</p>

      <ul class="color_blue _bold">
        <li>Subdivision of Land</li>
        <li>Construction, addition or alteration of a building or structure</li>
        <li>Construction of roads and trails</li>
        <li>Disturbance of soils, land alteration or land clearing</li>
        <li>Installation of non-structural surfaces with semi-pervious or impervious materials</li>
      </ul>

      <p>Through development permit guidelines, as described in the OCP and Section 7A of the Zoning By-law, a Development Variance Permit (DVP) will also be required if the proposed activity involves varying the By-law’s recommended setbacks for a watercourse or Green Infrastructure Area (GIA). To obtain a DVP approval, additional reports, including an Impact Mitigation Plan (IMP) will be required to provide options and mitigation measures to minimize project effects to the satisfaction of City staff and Council. Both DP3 and IMPs are required to be prepared and approved by a QEP (e.g. R.P.Bio), and PLG is available to provide appropriate government liaising as part of the DP3 application process.</p>

    </div>
  </div>

</div>



<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Environmental Approvals</h2>
      <hr class="short-line">

      <p>The <i>Water Sustainability Act (WSA)</i> was brought into effect on February 29, 2016, as an update to the Water Act, to ensure a sustainable supply of fresh, clean water that meets the needs of B.C. residents is available today and in the future.</p>

      <p>Under Section 11 of the <i>WSA</i>, a Change Approval is required if an applicant wishes to request permission to make changes (e.g., closure), modifications or, significantly re-locate an existing waterbody. Applicable works are subject to pre-determined fisheries/stream compensation and/or enhancement works, to off-set proposed changes/modifications to the existing waterbody. The terms and conditions may relate to the time of year in which you may do the work, and/or to other measures that protect the aquatic ecosystem, the hydrologic integrity of the stream channel and the rights of water users and landowners downstream. Section 11 <i>WSA</i> Change Approval applications are submitted to FrontCounter BC, where it will be reviewed by the Water Manager and typically take between 6 months to 1 year to be approved. <i>WSA</i> Change Approval applications are often conditional to a certain season when proposed work may occur (e.g., instream work windows).</p>

      <p>Under the same legislation, a <i>WSA</i> Notification may be submitted for projects where changes to an existing waterbody are considered low risk changes with minimal impact to the environment. Work must meet requirements as governed by the <i>WSA</i> and comply with conditions and best practices as determined by a habitat officer in response to a <i>WSA</i> Notification for work. <i>WSA</i> Notifications are submitted to FrontCounter BC a minimum of 45 days ahead of proposed work (submitting more than 45 days ahead of time is recommended). A response must be received from an approving officer before work may proceed; however, if no response from the Ministry is received within 45 days of FrontCounter BC receiving the WSA Notification application then proposed work may proceed, providing it meets the terms and conditions outlined in Part 3 of the Water Sustainability Regulation.</p>

    </div>
  </div>

</div>

