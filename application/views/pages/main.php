<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="bg-cover-top-full bg_index-page">
  <div class="row-section row-section_dark bg-color_transparent">

    <div class="row-section__txt row-section__txt_50 row-section__txt_right bg-color_dark-transparent">
      <div class="row-section__content-container">

        <h1 class="h1-compound h1-compound_light"><span>Providing Vision & Leadership </span><span>For Effective Land-Use and Environmental Solutions</span></h1>
        <hr class="short-line-green">
        <p>Pacific Land Group (PLG) is an experienced multidisciplinary land-use consulting firm focused on delivering creative and effective solutions to complex land planning, development and environmental issues. Our expertise also includes, but is not limited to, land development and approvals, feasibility studies, Sensitive Ecosystem Development Plans and Permits (SEDP/DP3), engineering design and construction, topographic surveys, and Erosion and Sediment Control (ESC). We represent both public- and private-sector clients on a variety of planning, zoning and environmental matters.</p>
        <a class="button-like" href="/pacific-land-group-team">Learn more</a>

      </div>
    </div>

    <div class="row-section__bg row-section__bg_50 row-section__bg_left"></div>

  </div>
</div>





<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_67 row-section__txt_left">
    <div class="row-section__content-container">
      <h1 class="h1-compound h1-compound_dark"><span>Pacific Land Group </span><span>What Sets Us Apart</span></h1>
      <hr class="short-line">
      <p>PLG takes pride in making connections and excels at creating lasting relationships with our clients. We utilize our team’s diversity and capitalize on our individual strengths to provide our clients deeper insights and better solutions to complex land use and environmental challenges. Our flexible structure and interconnected departments enables us to have the right individuals directly involved in every project and to provide tailored solutions to each client.</p>
      <a class="button-like" href="/land-planning-solutions">Learn more</a>
    </div>
  </div>

  <div class="row-section__bg row-section__bg_33 row-section__bg_right bg_what-sets-us-apart"></div>

</div>






<div class="row-section row-section_dark bg-color_grayblue">
  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">

      <h1 class="h1-compound h1-compound_light"><span>Our Strengths </span><span>Experience</span></h1>
      <hr class="short-line">
      <p>Since our founding in 1996, PLG has worked with many municipalities and communities across the Lower Mainland and Fraser Valley. We understand that every community is unique and requires its own tailored set of planning and environmental guidelines. What we bring is an unparalleled depth of experience in helping to create the best cohesive solutions for our clients.</p>
      <a class="button-like" href="/success-stories">Learn more</a>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_our-strenghts-experience"></div>
</div>






<div class="row-section row-section_light bg-color_white">
  <div class="row-section__txt row-section__txt_67 row-section__txt_left">
    <div class="row-section__content-container">

      <h1 class="h1-compound h1-compound_dark"><span>Land Use Planning </span><span>Success Stories</span></h1>
      <hr class="short-line">
      <p>PLG strives to incorporate sustainable design and technology into our diverse range of land development services. We have successfully completed a number of commercial and industrial buildings that meet LEED™, Built Green™ specifications.</p>
      <a class="button-like" href="/success-stories">Learn more</a>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_33 row-section__bg_right bg_success-stories"></div>
</div>







<div class="row-section row-section_dark bg-color_noble-blue">
  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">

      <h1 class="h1-compound h1-compound_light"><span>Land Use Planning </span><span>Highest & Best Land Use Analysis</span></h1>
      <hr class="short-line">
      <p>Our highest and best land use analyses are used to determine optimal development potential for unique and/or challenging properties, as well as to assess the development and financial impacts of a partial land taking for public rights-of-way.</p>
      <p>With every project, we aim to clearly identify a property’s highest and best land use while enabling property appraisers to establish a more precise and supportable market value. Our expert planners are available to help simplify the negotiation process and facilitate timely permit issuance.</p>
      <a class="button-like" href="/land-use-planning">Learn more</a>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_land-use-planning"></div>
</div>








<div class="row-section row-section_light bg-color_white">
  <div class="row-section__txt row-section__txt_75 row-section__txt_left">
    <div class="row-section__content-container">

      <h1 class="h1-compound h1-compound_dark"><span>Environment</span><span></span></h1>
      <hr class="short-line">
      <p>PLG’s team of Qualified Environmental Professionals (QEP) are dedicated to helping clients navigate the environmental requirements necessary to complete their projects while achieving environmental compliance with development goals. The environmental division has over 20 years of combined environmental consulting experience across the region and is highly versed in local government policies, bylaws and regulations. Our QEPs are able to take you through the environmental development process, which may include assistance with determining what environmental requirements are applicable to your property, management of any necessary permits and approvals by elected officials, and preparation of applications and reports for submission to governing bodies.</p>
      <a class="button-like" href="/environment">Learn more</a>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_25 row-section__bg_right bg_environment"></div>
</div>






<div class="row-section row-section_dark bg-color_dark">
  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">

      <h1 class="h1-compound h1-compound_light"><span>Civil Engineering</span><span></span></h1>
      <hr class="short-line">
      <p>PLG frequently collaborates with Hub Engineering Inc. (Hub), an in-house division offering a broad range of civil engineering services for municipal land development and building projects. Hub services include detailed engineering design and construction, feasibility studies, and construction tendering and cost estimates. The engineering team also provides construction supervision and inspection services to ensure all works being done comply with regulatory standards.</p>
      <a class="button-like" href="/civil-engineering">Learn more</a>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_civil-engineering"></div>
</div>






<div class="row-section row-section_light bg-color_white">
  <div class="row-section__txt row-section__txt_75 row-section__txt_left">
    <div class="row-section__content-container">

      <h1 class="h1-compound h1-compound_dark"><span>Land Survey</span><span></span></h1>
      <hr class="short-line">
      <p>PLG works with South Fraser Land Surveying (SFLS), an in-house team of experts led by a qualified BC Land Surveyor. Our survey division has over 105 years of combined professional land survey experience and are qualified to tackle a diversity of projects throughout the Lower Mainland and Fraser Valley. SFLS services include both topographic and legal surveys used to aid in PLG’s planning, development and environmental projects. The survey team works collectively with in-house planners, engineers and environmental consultants to achieve the most efficient concept for development plans and ensure maximum benefits to the client.</p>
      <a class="button-like" href="/land-survey">Learn more</a>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_25 row-section__bg_right bg_land-survey"></div>
</div>
