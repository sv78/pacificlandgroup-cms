<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<!-- Sustainability -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_what-sets-us-apart"></div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h1 class="h-simple">Sustainable Development &&nbsp;Sustainable Solutions</h1>
      <hr class="short-line">
      <p>PLG integrates sustainability principles in our wide range of planning services, allowing our clients to design and build projects that meet the highest standards in green building and neighbourhood design. We assist our clients in determining practical sustainable initiatives early in the design process, which allows for significant cost saving benefits. Project coordination and management of supporting consultants ensures early support/backing by local government and timely approvals and support throughout the design and construction stages of the project.</p>

      <?= $staff_card; ?>

    </div>
  </div>

</div>





<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_75 row-section__txt_left">
    <div class="row-section__content-container">

      <h2 class="h-simple">Sustainability Consultant</h2>
      <hr class="short-line">

      <p>Our expertise and experience in sustainable development planning allows us to come up with site unique designs and initiatives specific to the location, taking into account local physical and environmental conditions and political/public expectations. We have an integrated project team of planners, engineers, certified sustainable building advisors and environmental experts to help you implement sustainable building. We pride ourselves on continued education of the latest technologies in such areas as building technology, stormwater management, sediment and erosion design, and renewable energy.</p>

    </div>
  </div>

  <div class="row-section__bg row-section__bg_25 row-section__bg_right bg_public-consultation"></div>

</div>



<div class="row-section row-section_dark bg-color_noble-blue">
  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container text-align-left">

      <h2 class="h-simple">Certified Sustainable Green Building Advisor</h2>
      <hr class="short-line block-left">
      <p class="color_blue">Our staff accreditation brings the latest knowledge and expertise to offer sustainable solutions in our projects:</p>
      <ul class="ul-normal ul_green-dots color_white">
        <li>Development Applications which identify and assess key sustainable building practices and goals</li>
        <li>Analyze the costs and benefits of incorporating specific sustainable building measures</li>
        <li>Application of LEED™, Built Green™, Energy Star™, and other relevant site specific criteria or established guidelines</li>
        <li>Assist designers, architects, builders, operators, and utilities managers to achieve sustainability in their projects</li>
      </ul>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_land-development-services"></div>
</div>








<!-- Our Successful Projects -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_center">
    <div class="row-section__content-container">
      <h2 class="h-simple color_blue">Our Successful Projects</h2>
      <hr class="short-line bg-color_green">
    </div>
  </div>

</div>






<!-- Successful Projects Image Tiles -->

<!-- Line 1 -->

<div class="tile-cards">

  <?php
  $this->load->view('chunks/ss_tiles/delsom-estates-sunstone-community');
  $this->load->view('chunks/ss_tiles/loblaw-food-distribution-facility');
  $this->load->view('chunks/ss_tiles/cloverdale-cold-storage');
  ?>

</div>





<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Delsom Estates (Sunstone Community)</h2>
      <hr class="short-line">
      <p>PLG acted as the lead consultant for a 107-acre master-planned community which included park, commercial, multi-family and single family uses at 84th Avenue and Nordel Way, Delta. <a href="/success-stories/sustainability/delsom-estates-sunstone-community" class="color_blue">Learn more</a></p>

      <h2 class="h-simple">Loblaw Food Distribution Facility</h2>
      <hr class="short-line">
      <p>PLG was the lead consultant for the first Canadian LEED silver accredited Food Distribution Facility (421,000 ft2) in the Campbell Heights industrial area, South Surrey. <a href="/success-stories/sustainability/loblaws-food-distribution-centre-surrey-bc" class="color_blue">Learn more</a></p>

      <h2 class="h-simple">Cloverdale Cold Storage</h2>
      <hr class="short-line">
      <p>PLG was the lead consultant for the food storage industrial development (130,635 ft2) which achieved LEED equivalent rating at 188th Street and 32nd Avenue, Surrey. <a href="/success-stories/commercial-industrial-development/cloverdale-cold-storage-surrey-bc" class="color_blue">Learn more</a></p>

    </div>
  </div>

</div>