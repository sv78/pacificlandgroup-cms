<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<!-- Land Development & Approvals -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_land-development-and-approvals"></div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h1 class="h-simple">Land Development &&nbsp;Development Application Approvals</h1>
      <hr class="short-line">
      <p>PLG's professional staff has extensive public and private sector experience in designing, preparing and managing successful land development projects. Our Principal and planners have held senior government positions and have experience in strategically preparing projects for approval by Government Representatives and elected officials.</p>
      <p>Our expertise includes maximizing achievable unit/floor area yields, testing market potential, estimating residual land value based on development and construction costs, and managing successful development approval strategies.</p>
      <p>Our land development team is typically responsible for concept development, site planning, subdivision, professional reports, leading rezoning and development permit approvals, and community consultation.</p>

      <?= $staff_card; ?>

    </div>
  </div>

</div>





<!-- Land Development Services -->

<div class="row-section row-section_dark bg-color_noble-blue">
  <div class="row-section__txt row-section__txt_50 row-section__txt_left">
    <div class="row-section__content-container text-align-left">

      <h2 class="h-simple">Land Development Services</h2>
      <hr class="short-line block-left">
      <ul class="ul-normal ul_green-dots color_white">
        <li>Site Planning</li>
        <li>Subdivision Design / Comprehensive Land Use Planning</li>
        <li>Development Applications</li>
        <li>Civil Engineering</li>
        <li>Government Agency Approvals</li>
        <li>Development Feasibility Studies</li>
        <li>Urban Design Guidelines / Design Briefs</li>
        <li>Habitat Design & Construction</li>
        <li>Streamside Protection & Compensation</li>
        <li>Environmental Monitoring</li>
        <li>Land Surveying</li>
        <li>Demand Studies and Proformas</li>
        <li>Economic Impact Analysis</li>
      </ul>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_50 row-section__bg_right bg_land-development-services"></div>
</div>







<!-- Our Clients -->

<div class="row-section row-section_dark bg-color_blue">
  <div class="row-section__txt row-section__txt_50 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Our Clients</h2>
      <hr class="short-line">
      <ul class="ul-list color_white">
        <li>Township of Langley</li>
        <li>Trinity Western University</li>
        <li>Surrey City Development Corporation</li>
        <li>Beedie Group</li>
        <li>Wesgroup</li>
        <li>Husky Canada</li>
        <li>Chevron</li>
        <li>Loblaw Companies Ltd. / Loblaw</li>
        <li>Shell Canada</li>
        <li>Pattison Outdoor Advertising</li>
        <li>Parklane Homes</li>
        <li>Delsom Estates</li>
        <li>Canadian Horizons</li>
        <li>Polygon Homes</li>
      </ul>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_50 row-section__bg_left bg_our-clients"></div>
</div>




<!-- Our Strengths Expertise & Experience -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_center">
    <div class="row-section__content-container">
      <h2 class="h-simple color_blue">Our Strengths Expertise &&nbsp;Experience</h2>
      <hr class="short-line bg-color_green">
    </div>
  </div>

</div>






<!-- Successful Projects Image Tiles -->

<!-- Line 1 -->

<div class="tile-cards">

  <?php
  $this->load->view('chunks/ss_tiles/commercial-and-industrial-development');
  $this->load->view('chunks/ss_tiles/residential-development');
  $this->load->view('chunks/ss_tiles/agricultural-land');
  ?>

</div>








<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Commercial and Industrial Development</h2>
      <hr class="short-line">
      <p>PLG staff is experienced in developing commercial and industrial projects which include industrial / business parks, industrial warehouses, food distribution facilities, commercial shopping plazas, and gas stations. Learn about our successful Commercial & Industrial Development projects. <a href="/success-stories/commercial-industrial-development" class="color_blue">Learn more</a></p>

      <h2 class="h-simple">Residential Development</h2>
      <hr class="short-line">
      <p>Our work on residential projects covers a large number of single-family subdivisions, multi-family, and mixed-use developments in <a href="/success-stories/residential-development" class="color_blue">Learn more</a></p>

      <h2 class="h-simple">Agricultural</h2>
      <hr class="short-line">
      <p>Our work on agricultural land includes applications to the ALC for Transportation, Utility and Recreational Trail Uses, ALR Exclusion, Non-farm use, soil deposition and subdivision within the ALR. <a href="/success-stories/agricultural-land" class="color_blue">Learn more</a></p>

    </div>
  </div>

</div>