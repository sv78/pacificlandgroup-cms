<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<!-- Public Consultation -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_public-consultation"></div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h1 class="h-simple">Public Consultation &&nbsp;Community Engagement</h1>
      <hr class="short-line">
      <p>Effective community input and appropriate responses from the planning team can significantly improve a project's reception and approval at the political level. PLG has developed and conducted visioning workshops and opinion surveys, and has coordinated numerous stakeholder meetings and public consultation programs.</p>

      <p>We are adept at acting on stakeholder concerns to achieve both community and political support.</p>

      <?php
      /*
       * 

        <!--
        <div class="row-flex offset-top-20">
        <ul class="row-flex__flex-1 ul-extra color_blue _bold">
        <li></li>
        </ul>

        <ul class="row-flex__col-50 ul-extra color_blue _bold">
        <li></li>
        </ul>
        </div>
        -->

       * 
       */
      ?>


      <?= $staff_card; ?>

    </div>
  </div>

</div>





<div class="row-section row-section_dark bg-color_noble-blue">
  <div class="row-section__txt row-section__txt_75 row-section__txt_left">
    <div class="row-section__content-container text-align-left">

      <h2 class="h-simple">Public Consultation &&nbsp;Facilitation Services:</h2>
      <hr class="short-line block-left">
      <p class="color_blue">Public consultation and facilitation programs are frequently developed as a component of many of our development projects involving sensitive land use. Our consultation processes include:</p>
      <ul class="ul-normal ul_green-dots color_white">
        <li>Public Survey & Methodology Design</li>
        <li>Neighbourhood Support/Canvassing Programs</li>
        <li>Public Information Meetings</li>
        <li>Preparation & Coordination of Display Materials</li>
        <li>Summary Reports to Client and Planning Departments</li>
        <li>Design of Questionnaires</li>
        <li>Focus Groups & Visioning Exercises</li>
        <li>Neighbourhood Consultation Programs</li>
        <li>Project Open House Management</li>
        <li>Consultation with ratepayer groups and local Environmental Societies</li>
      </ul>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_25 row-section__bg_right bg_public-consultation"></div>
</div>




<!-- Our Successful Projects -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_center">
    <div class="row-section__content-container">
      <h2 class="h-simple color_blue">Our Successful Projects</h2>
      <hr class="short-line bg-color_green">
    </div>
  </div>

</div>






<!-- Successful Projects Image Tiles -->

<!-- Line 1 -->

<div class="tile-cards">

  <?php
  $this->load->view("chunks/ss_tiles/delsom-estates-sunstone-community");
  $this->load->view("chunks/ss_tiles/bear-creek-plaza");
  $this->load->view("chunks/ss_tiles/64th-avenue-mufford-crescent-overpass");
  ?>

</div>



<!-- Line 2 -->

<div class="tile-cards">

  <?php
  $this->load->view("chunks/ss_tiles/gas-stations");
  $this->load->view("chunks/ss_tiles/south-surrey-engineering-works-yard-open-house");
  $this->load->view("chunks/ss_tiles/pacificLink-business-park");
  ?>

</div>






<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Delsom Estates (Sunstone Community)</h2>
      <hr class="short-line">
      <p>PLG acted as the lead consultant for a 107 acre master planned community which included park, commercial, multi-family and single family uses at 84th Avenue and Nordel Way, Delta. <a href="/success-stories/sustainability/delsom-estates-sunstone-community" class="color_blue">Learn more</a></p>


      <h2 class="h-simple">Bear Creek Plaza</h2>
      <hr class="short-line">
      <p>PLG was the coordinating consultant for converting 7 low density residential lots (3 acres) into a neighbourhood commercial centre at 88 Avenue and King George Highway, Surrey. A series of open house meetings were held to engage and inform the local community.</p>

      <h2 class="h-simple">64th Avenue / Mufford Crescent Overpass</h2>
      <hr class="short-line">
      <p>PLG was commissioned to manage the Transportation Corridor Use approval from the Agricultural Land Commission (ALC). The proposal involved mitigation of impacts on agriculture lands and complex land acquisition issues. <a href="/success-stories/agricultural-land/64th-avenue-mufford-crescent-overpass" class="color_blue">Learn more</a></p>

      <h2 class="h-simple">Gas Stations (Shell, Chevron and Husky at various locations)</h2>
      <hr class="short-line">
      <p>PLG has extensive experience managing and coordinating the necessary municipal and provincial approvals for both new build and redeveloped gas station sites.  PLG has successfully navigated contentious issues related to conflicts between residential and commercial land uses. <a href="#" class="color_blue">Learn more</a></p>



      <h2 class="h-simple">City of Surrey Engineering Works Yard Open House</h2>
      <hr class="short-line">
      <p>PLG organized the public open house and gathered public opinion on proposed site locations. The south Surrey residential community was engaged to offer meaningful input to the site development.</p>

      <h2 class="h-simple">PacificLink Business Park</h2>
      <hr class="short-line">
      <p>PLG was the coordinating consultant for this 80 acre multi use site at Scott Road and 103A Avenue Surrey. We drafted a Comprehensive Development Zoning Bylaw to permit mixed-use development and minimize road requirements. <a href="/success-stories/commercial-industrial-development/pacificlink-business-park-south-westminster-surrey-bc" class="color_blue">Learn more</a></p>

    </div>
  </div>

</div>