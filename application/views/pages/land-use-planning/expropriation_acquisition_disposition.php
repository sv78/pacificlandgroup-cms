<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<!-- Highest & Best Use Analysis -->

<div class="bg-cover-top-full bg_expropriation-acquisition-disposition-main top-img-bordered-blue">

  <div class="row-section row-section_dark bg-color_transparent">

    <div class="row-section__txt row-section__txt_100 row-section__txt_right">
      <div class="row-section__content-container">

        <h1 class="h1-very-big">Land Expropriation, Land Acquisition &&nbsp;Land Disposition</h1>
        <hr class="short-line">

        <p>Land use assessments are used in land acquisition, land disposition, land valuation and expropriation cases. Land Use Assessment reports provide necessary input for determining a property's market value.  Land Use evaluation strategies are also used to assess development and financial impacts of a partial taking for public rights-of-way, litigation matters, and encroachment of public works on private property.</p>

        <p>We have prepared numerous land acquisition and disposition assessments and expropriation impact assessments - for private land owners, property appraisers, municipal governments, senior government agencies, financial institutions, developers and investor groups. While the study approach is essentially similar in each case - the impetus for the study, the manner in which the information is used, and the physical characteristics of each site are always unique.</p>

      </div>
    </div>

    <!-- <div class="row-section__bg row-section__bg_50 row-section__bg_left"></div> -->

  </div>

</div>


<!-- Strategic Land Analysis & Planning -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_study-process-ead"></div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h2 class="h-simple">Study Process</h2>
      <hr class="short-line">
      <p>In determining the land development potential or land taking impacts on a subject property, we will typically prepare one or more conceptual pre-taking and post-taking site plans to determine a property's optimal development potential - i.e., confirmation of net developable site areas, floor area/unit yields, etc. Our land use analyses are supported by a detailed planning rationale, and consider all pertinent factors, including:</p>

      <div class="row-flex">
        <ul class="ul-extra color_blue _bold">
          <li>Property size and location</li>
          <li>Physical site characteristics</li>
          <li>Official Community Plan designation(s)</li>
          <li>Existing and proposed zoning</li>
          <li>Regulations in effect at the time of taking</li>
          <li>Municipal planning and land use policies</li>
        </ul>

        <ul class="ul-extra color_blue _bold">
          <li>Required environmental setbacks and dedications</li>
          <li>Market supply/demand conditions</li>
          <li>Feasibility and cost of servicing the land</li>
          <li>Site access opportunities</li>
          <li>Surrounding land use context</li>
          <li>Local development activity</li>
        </ul>
      </div>

      <?= $staff_card; ?>

    </div>
  </div>

</div>






<div class="row-section row-section_dark bg-color_noble-blue">
  <div class="row-section__txt row-section__txt_67 row-section__txt_left">
    <div class="row-section__content-container text-align-left">

      <h2 class="h-simple">Key Benefits</h2>
      <hr class="short-line block-left">
      <p class="color_blue">Our expropriation impact assessments:</p>
      <ul class="ul-normal ul_green-dots color_white">
        <li>Clearly identify land taking impacts subject property utilization</li>
        <li>Enable property appraisers to establish a more precise market value</li>
        <li>Help simplify the negotiation process</li>
        <li>Allow settlements to be reached in a more timely manner</li>
      </ul>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_33 row-section__bg_right bg_key-benefits"></div>
</div>





<div class="row-section row-section_light bg-color_light-gray">
  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Our Land Development Assessments</h2>
      <hr class="short-line">
      <ul class="ul-list color_dark">
        <li>Clearly ID maximum property utilization</li>
        <li>Take into account applicable jurisdictional and technical parameters</li>
        <li>Market and Political influences</li>
        <li>Physical site characteristics</li>
        <li>Other influences such as environmental sensitivity</li>
      </ul>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_our-strenghts-experience"></div>
</div>



<div class="row-section row-section_dark bg-color_blue">
  <div class="row-section__txt row-section__txt_50 row-section__txt_left">
    <div class="row-section__content-container">

      <h2 class="h-simple">Our Clients</h2>
      <hr class="short-line">
      <ul class="ul-list color_white">
        <li>British Pacific Properties Ltd.</li>
        <li>District of West Vancouver</li>
        <li>City of Surrey</li>
        <li>Township of Langley</li>
        <li>Borden Ladner Gervais LLP</li>
        <li>Nathanson, Schachter & Thompson LLP</li>
        <li>Robertson, Downe, & Mullally</li>
        <li>Peterson Stark Scott Barristers and Solicitors</li>
      </ul>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_50 row-section__bg_right bg_our-clients"></div>
</div>






<!-- Our Successful Projects -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_center">
    <div class="row-section__content-container">
      <h2 class="h-simple color_blue">Our Successful Projects</h2>
      <hr class="short-line bg-color_green">
    </div>
  </div>

</div>






<!-- Successful Projects Image Tiles -->

<!-- Line 1 -->

<div class="tile-cards">

  <?php
  $this->load->view("chunks/ss_tiles/eagleridge-lands");
  $this->load->view("chunks/ss_tiles/bc-hydro-transmission-row");
  $this->load->view("chunks/ss_tiles/192-street-overpass");
  ?>

</div>

