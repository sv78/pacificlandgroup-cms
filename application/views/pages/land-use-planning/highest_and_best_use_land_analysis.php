<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<div class="bg-cover-top-full bg_highest-and-best-use-analysis-main top-img-bordered-blue">

  <div class="row-section row-section_dark bg-color_transparent">

    <div class="row-section__txt row-section__txt_100 row-section__txt_right">
      <div class="row-section__content-container">

        <h1 class="h1-very-big">Highest &&nbsp;Best Use Land Analysis</h1>
        <hr class="short-line">
        <p>Pacific Land Group specializes in highest and best land use assessments for private land owners, property appraisers, municipal governments, senior government agencies, financial institutions, and lawyers. Our highest and best land use analyses are used to determine the optimal development potential for unique and/or challenging properties. The highest and best land use analyses are often used to establish development and financial impacts of a partial land taking for public right-of-ways. Property purchases/dispositions, right-of-way acquisitions, development feasibility analyses, explorations, and litigation matters can also be supported by our land use evaluation methods.</p>
        <p>Pacific Land Group also specializes in preparing Development Assessments, which identify the land use potential, physical site characteristics, environmental features, and the anticipated development application process. Development Assessments are essential to determine development potential (or loss of potential) of a property in relation to local government and provincial legislation.</p>

      </div>
    </div>

    <!-- <div class="row-section__bg row-section__bg_50 row-section__bg_left"></div> -->

  </div>

</div>



<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_study-process"></div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h2 class="h-simple">Study Process</h2>
      <hr class="short-line">
      <p>In determining the highest and best use of a subject property, we typically prepare one or more conceptual site plans to determine a property's optimal development potential (i.e., confirmation of net developable site areas, floor area/unit yields, etc.). Our land use analyses are supported by a detailed planning rational and research to consider all pertinent factors, including:</p>

      <div class="row-flex">
        <ul class="ul-extra color_blue _bold">
          <li>Property size and location</li>
          <li>Surrounding land use context</li>
          <li>Physical site characteristics (e.g., steep slopes)</li>
          <li>Applicable Provincial and/or Federal legislation</li>
          <li>Official Community Plan designation(s)</li>
          <li>Existing and proposed/potential zoning</li>
        </ul>

        <ul class="ul-extra color_blue _bold">
          <li>Municipal planning and land use policies</li>
          <li>Required environmental setbacks, dedications, and/or protection areas</li>
          <li>Market supply/demand conditions</li>
          <li>Feasibility and cost of servicing the land</li>
          <li>Site access opportunities</li>
          <li>Local development activity</li>
          <li>Conclusions on reasonable probability of land use</li>
        </ul>
      </div>

      <?= $staff_card; ?>

    </div>
  </div>

</div>






<div class="row-section row-section_dark bg-color_noble-blue">
  <div class="row-section__txt row-section__txt_67 row-section__txt_left">
    <div class="row-section__content-container text-align-left">

      <h2 class="h-simple">Key Benefits</h2>
      <hr class="short-line block-left">
      <p class="color_blue">Our highest and best land use analyses:</p>
      <ul class="ul-normal ul_green-dots color_white">
        <li>Clearly identify a property's highest and best land use</li>
        <li>Enable property appraisers to establish a more precise and supportable market value</li>
        <li>Help simplify the negotiation process</li>
        <li>Allow settlements to be reached in a more timely manner</li>
      </ul>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_33 row-section__bg_right bg_key-benefits"></div>
</div>





<div class="row-section row-section_dark bg-color_blue">
  <div class="row-section__txt row-section__txt_50 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Our Clients</h2>
      <hr class="short-line">
      <ul class="ul-list color_white">
        <li>Atlus Group</li>
        <li>British Pacific Properties Ltd.</li>
        <li>District of West Vancouver</li>
        <li>City of Surrey</li>
        <li>Borden Ladner Gervais LLP</li>
        <li>Eyford Macaulay Shaw & Padmanabhan LLP</li>
        <li>Nathanson, Schachter & Thompson LLP</li>
        <li>Robertson, Downe, & Mullally</li>
        <li>Peterson Stark Scott Barristers and Solicitors</li>
        <li>Township of Langley</li>
        <li>Mountain Equipment Co-operative</li>
      </ul>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_50 row-section__bg_left bg_our-clients"></div>
</div>




<!-- Our Successful Projects -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_center">
    <div class="row-section__content-container">
      <h2 class="h-simple color_blue">Our Successful Projects</h2>
      <hr class="short-line bg-color_green">
    </div>
  </div>

</div>






<!-- Successful Projects Image Tiles -->

<!-- Line 1 -->

<div class="tile-cards">

  <?php
  $this->load->view("chunks/ss_tiles/delsom-estates-sunstone-community");
  $this->load->view("chunks/ss_tiles/bear-creek-plaza");
  $this->load->view("chunks/ss_tiles/campbell-heights-north-business-park");
  ?>

</div>



<!-- Line 2 -->

<div class="tile-cards">

  <?php
  $this->load->view("chunks/ss_tiles/64th-avenue-mufford-crescent-overpass");
  $this->load->view("chunks/ss_tiles/168th-street-and-12th-avenue-surrey");
  $this->load->view("chunks/ss_tiles/pacificLink-business-park");
  ?>

</div>


<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Delsom Estates (Sunstone Community)</h2>
      <hr class="short-line">
      <p>PLG acted as the lead consultant for a 107 acre master-planned community which included park, commercial, multi-family and single family uses at 84th Avenue and Nordel Way, Delta. <a href="/success-stories/sustainability/delsom-estates-sunstone-community" class="color_blue">Learn more</a></p>


      <h2 class="h-simple">Bear Creek Plaza</h2>
      <hr class="short-line">
      <p>PLG was the coordinating consultant for converting 7 low density residential lots (3 acres) into a neighborhood commercial centre at 88 Avenue and King George Highway, Surrey.</p>

      <h2 class="h-simple">Campbell Heights North Business Park</h2>
      <hr class="short-line">
      <p>PLG was the planning consultant for the 305 acre multiphase industrial subdivision and rezoning approval at 32nd Avenue and 192nd Street, Surrey. <a href="/success-stories/commercial-industrial-development/campbell-heights-north-business-park-phase-1-surrey-bc" class="color_blue">Learn more</a></p>

      <h2 class="h-simple">64th Avenue / Mufford Crescent Overpass</h2>
      <hr class="short-line">
      <p>PLG was commissioned to manage the Transportation Corridor Use approval from the Agricultural Land Commission (ALC). The Township of Langley Highway Corridor alignment involved impacts to agricultural lands and complex land acquisition issues. <a href="/success-stories/agricultural-land/64th-avenue-mufford-crescent-overpass" class="color_blue">Learn more</a></p>

      <h2 class="h-simple">168th Street and 12th Avenue, Surrey</h2>
      <hr class="short-line">
      <p>PLG was the lead consultant responsible for establishing the developable potential of a 40 acre property consolidation within a very challenging environmentally constrained site area.</p>

      <h2 class="h-simple">PacificLink Business Park</h2>
      <hr class="short-line">
      <p>PLG was the coordinating consultant for this 80 acre multi use site at Scott Road and 103A Avenue Surrey. We drafted a Comprehensive Development Zoning Bylaw to permit mixed-use development and minimize road requirements. <a href="/success-stories/commercial-industrial-development/pacificlink-business-park-south-westminster-surrey-bc" class="color_blue">Learn more</a></p>

    </div>
  </div>

</div>