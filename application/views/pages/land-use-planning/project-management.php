<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>


<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_25 row-section__bg_left bg_project-management"></div>

  <div class="row-section__txt row-section__txt_75 row-section__txt_right">
    <div class="row-section__content-container">
      <h1 class="h-simple">Project management/support for Application Management</h1>
      <hr class="short-line">
      <p>Using specialized teams drawn from both in-house expertise and our associated consultant network, we provide the talent and skills to ensure successful project completion – from vision to reality. Our Project Management teams are capable of taking a land development project from conceptual design, detailed design, and development approvals, through to tendering, construction, and completion.</p>

      <?= $staff_card; ?>

    </div>
  </div>

</div>





<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h1 class="h-simple">Our Project Management Services Include:</h1>
      <hr class="short-line">


      <div class="row-flex offset-top-20">
        <ul class="row-flex__flex-1 ul-extra _bold">
          <li>Preparation of terms of reference</li>
          <li>Managing government agency and public approval processes</li>
          <li>Managing and coordinating all necessary  professional services including planning, survey, architectural, environmental, geotechnical and civil engineering</li>
          <li>Design and implementation of development strategies, including:
            <ul class="_normal">
              <li>Land Acquisition Strategies</li>
              <li>Conceptual Land Planning &&nbsp;Design</li>
              <li>Management or preparation of working drawings</li>
            </ul>
          </li>
        </ul>

        <ul class="row-flex__col-50 ul-extra _bold">
          <li>Municipal &&nbsp;Outside Agency Approvals</li>
          <li>Building Permit Processing</li>
          <li>Tendering &&nbsp;Contracts</li>
          <li>Scheduling &&nbsp;Contract Administration</li>
          <li>Certification &&nbsp;Construction Management through our affiliates</li>
          <li>Completion</li>
        </ul>
      </div>

    </div>
  </div>

</div>






<div class="row-section row-section_dark bg-color_noble-blue">
  <div class="row-section__txt row-section__txt_67 row-section__txt_left">
    <div class="row-section__content-container text-align-left">

      <h2 class="h-simple">Key Benefits</h2>
      <hr class="short-line block-left">
      <p class="color_blue">Pacific Land Group's Project Management Team:</p>
      <ul class="ul-normal ul_green-dots color_white">
        <li>Anticipates and deals with potential problems before they arise</li>
        <li>Has expert knowledge of the development and approval process</li>
        <li>Provides direct and effective communication with the client/owner</li>
        <li>Provides expert control of timelines and budgets</li>
        <li>Takes a flexible approach, allowing for adjustment to the project as it evolves</li>
        <li>Includes an in-house affiliate multi-disciplinary talent bank for each project</li>
      </ul>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_33 row-section__bg_right bg_key-benefits"></div>
</div>





<div class="row-section row-section_dark bg-color_blue">
  <div class="row-section__txt row-section__txt_50 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Our Clients</h2>
      <hr class="short-line">
      <ul class="ul-list color_white">
        <li>Colliers International</li>
        <li>Revolution Resource Recovery</li>
        <li>North West Development Corporation</li>
        <li>New World Technology</li>
        <li>Borden Ladner Gervais LLP</li>
        <li>Bentall Kennedy</li>
        <li>City of Abbotsford</li>
        <li>City of Surrey</li>
        <li>Amix Group</li>
      </ul>

    </div>
  </div>
  <div class="row-section__bg row-section__bg_50 row-section__bg_left bg_our-clients"></div>
</div>




<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_center">
    <div class="row-section__content-container">
      <h2 class="h-simple color_blue">Our Successful Projects</h2>
      <hr class="short-line bg-color_green">
    </div>
  </div>

</div>






<!-- Successful Projects Image Tiles -->

<!-- Line 1 -->

<div class="tile-cards">

  <?php
  $this->load->view("chunks/ss_tiles/delsom-estates-sunstone-community");
  $this->load->view("chunks/ss_tiles/burnaby-business-park");
  $this->load->view("chunks/ss_tiles/campbell-heights-north-business-park");
  ?>

</div>



<!-- Line 2 -->

<div class="tile-cards">

  <?php
  $this->load->view("chunks/ss_tiles/loblaw-food-distribution-facility");
  $this->load->view("chunks/ss_tiles/cloverdale-cold-storage");
  $this->load->view("chunks/ss_tiles/gas-stations");
  ?>

</div>






<div class="row-section row-section_light bg-color_light-gray">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Delsom Estates (Sunstone Community)</h2>
      <hr class="short-line">
      <p>PLG acted as the lead consultant for a 107 acre master planned community which included park, commercial, multi-family and single family uses at 84th Avenue and Nordel Way, Delta. Today PLG is working with the developer to manage the approvals of the final parcel.  This includes a seniors’ rental building, Market Condos and over 39,909 square feet of commercial /office space.  PLG is coordinating the Building Permit for the phased project ensuring the commitments made in 2006 are implemented in 2018 and also ensuring the design is appropriate for the retail needs of today. <a href="/success-stories/sustainability/delsom-estates-sunstone-community" class="color_blue">Learn more</a></p>


      <h2 class="h-simple">Burnaby Business Park</h2>
      <hr class="short-line">
      <p>PLG was the lead consultant of an 88 acre industrial subdivision and rezoning approval at North Fraser Way, Burnaby. Detailed design guidelines and environment management plans were developed as part of the development permit and building permit requirements. <a href="/success-stories/commercial-industrial-development/burnaby-business-park-surrey-bc" class="color_blue">Learn more</a></p>


      <h2 class="h-simple">Campbell Heights North Business Park</h2>
      <hr class="short-line">
      <p>PLG was the planning consultant for the 305 acre multiphase industrial park which involved subdivision and rezoning approval for Surrey’s signature Business Park at 32nd Avenue and 192nd Street, Surrey. This work included creating the original Industrial Design Guidelines for the area. These guidelines are used as the basis for Development Permits required by the City of Surrey. <a href="/success-stories/commercial-industrial-development/campbell-heights-north-business-park-phase-1-surrey-bc" class="color_blue">Learn more</a></p>


      <h2 class="h-simple">Loblaw Food Distribution Facility</h2>
      <hr class="short-line">
      <p>PLG was the lead consultant for the first Canadian LEED silver accredited Food Distribution Facility (421,000 ft2) in the Campbell Heights industrial area, South Surrey. A number of sustainability factors were incorporated at the zoning and development permit stage. The building design received awards for use of innovative materials and sustainable storm water management. <a href="/success-stories/sustainability/loblaws-food-distribution-centre-surrey-bc" class="color_blue">Learn more</a></p>


      <h2 class="h-simple">Cloverdale Cold Storage</h2>
      <hr class="short-line">
      <p>PLG was the lead consultant for the food storage industrial development (130,635 ft2) which achieved LEED equivalent rating at 188th Street and 32nd Avenue, Surrey. As one of the cold storage warehouses in the Campbell Heights area- this building adheres to the design requirements for the designated Development Permit area and integrates the latest Green Building design technology. The application management portion of the project required exceptional communication and organizational skills to ensure the intent of the original building design was carried through to the construction stage. Ensuring the building design was functional, while addressing the architectural vision of the City of Surrey proved the most challenging, however in the end was successfully achieved. <a href="/success-stories/commercial-industrial-development/cloverdale-cold-storage-surrey-bc" class="color_blue">Learn more</a></p>


    </div>
  </div>

</div>