<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Urban and Agricultural Land Use Planning & Development -->

<div class="row-section row-section_light bg-color_white">

  <div class="row-section__bg row-section__bg_33 row-section__bg_left bg_land-use-planning-main">
    <h1 class="row-section__bg-header">Land Use Planning</h1>
  </div>

  <div class="row-section__txt row-section__txt_67 row-section__txt_right">
    <div class="row-section__content-container">
      <h2 class="h-simple">Urban and Agricultural Land Use Planning &&nbsp;Development</h2>
      <hr class="short-line">
      <p>Pacific Land Group (PLG) is an experienced multidisciplinary development planning firm focused on delivering solutions to complex land use and development issues. Our expertise includes Development Planning and Approvals, Highest and Best Land Use Assessments, Expropriation Analyses, Project Management, Sustainability Reports and Public Consultation.</p>
      <p class="color_blue">Land Use Planning is a large umbrella – the following are PLG’s areas of expertise:</p>
      <p>Development Planning Approvals- From Urban to Rural, Greenfield to Brownfield, Town Centres to Agricultural Land – our staff can create a development approval strategy to meet your project needs.</p>


      <?= $staff_card; ?>

    </div>
  </div>

</div>



<div class="row-section row-section_dark bg-color_noble-blue">
  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <p>As development strategists, PLG’s team can create a unique roadmap tailored to your specific project in order to position it for success.  We have a solid understanding of local community interests, policies and political atmosphere. A successful project begins with putting together an appropriate strategy including a specific consulting team to achieve approvals from Local Government and related agencies.</p>

      <p>PLG's professional planning and urban design staff have extensive public and private sector experience designing and managing successful land development projects. Our Principal has held senior government position and has experience in preparing and coordinating projects for approval. PLG ‘s planning staff  are registered  professional planners and hold memberships in a number of related organizations.</p>

    </div>
  </div>
</div>




<div class="row-section row-section_light">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Highest and Best Land Use Assessments:</h2>
      <hr class="short-line">

      <p>PLG can address straight forward and complex land use questions such as:  “What can I do with this piece of property?”  It doesn’t matter who you are, experienced developer, private property owner, local government or corporate firm. A Highest and Best Land Use Assessment will provide you with the key land use information you need to determine what to do next. Our team can identify actions required to achieve the approvals necessary to unlock the development potential of your property.</p>

      <h2 class="h-simple">Expropriation Impact Analyses:</h2>
      <hr class="short-line">
      <p>Understanding the future development and Highest and Best Land Use potential of a property is the first step in assessing the impact and subsequent land value associated with an expropriation. PLG examines and integrates a comprehensive land development analysis. We analyze development potential from many angles, including Municipal/Provincial regulations, local policies, environmental and civil engineering perspectives and other applicable parameters.</p>
      <p>The impacts of expropriation on physical elements such as site access, potential building foot print, parking and required setbacks are examined in order to determine impact of expropriation.  PLG collaborates with other professionals to strengthen the expropriation analyses.  Defining the impact of the expropriation in relation to the future development potential is key to determining the value.</p>

      <h2 class="h-simple">Project Management:</h2>
      <hr class="short-line">
      <p>PLG can put together a specialized design team combining our in-house expertise and our associated consultant network. Depending on the project we can provide the talent and skills to create the right team for your project.  PLG acts as the project lead to successfully manage projects through municipal and other governmental approval agencies.</p>

    </div>
  </div>

</div>


<?php
$this->load->view('chunks/land-use-planning/tile-menu');
?>


<div class="row-section row-section_light bg-color_white">

  <div class="row-section__txt row-section__txt_100 row-section__txt_right">
    <div class="row-section__content-container">

      <h2 class="h-simple">Sustainability:</h2>
      <hr class="short-line">
      <p>We have an integrated project team of planners, engineers, biologists, certified sustainable building advisors and qualified environmental professionals to help you implement a sustainable project. We pride ourselves on continued education of the latest technologies in such areas as building technology, stormwater management, and sediment and erosion control.</p>

      <h2 class="h-simple">Public Consultation:</h2>
      <hr class="short-line">
      <p>Meaningful public consultation and community engagement are essential elements of a successful project.  Better projects, better communities and better cities are created when there is meaningful engagement with the local community. PLG’s staff work well with community resident associations, environmental advocacy groups and local residents. PLG can create a public consultation program that suits your project and surrounding community.</p>

    </div>
  </div>

</div>