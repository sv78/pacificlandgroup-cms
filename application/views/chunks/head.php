<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">

    <?php
    if (!empty($title)):
      ?>
      <!-- TITLE -->
      <title><?= $title; ?></title>
      <?php
    endif;

    if (!empty($this->canonical_url)) :
      ?>
      <!-- CANONICAL URL -->
      <link rel="canonical" href="<?= $this->canonical_url; ?>">
      <?php
    endif;
    ?>

    <!-- ESSENTIAL META -->
    <?php
    if (!empty($meta_description)):
      ?>
      <meta name="description" content="<?= $meta_description; ?>">
      <?php
    endif;

    if (!empty($meta_keywords)):
      ?>
      <meta name="keywords" content="<?= $meta_keywords; ?>">
      <?php
    endif;
    ?>
    <meta name="robots" content="<?php echo empty($meta_robots) ? 'index, follow' : $meta_robots; ?>">
    <?php
    if (!empty($meta_subject)):
      ?>
      <meta name="subject" content="<?= $meta_subject; ?>">
      <?php
    endif;

    if (!empty($meta_author)):
      ?>
      <meta name="author" content="<?= $meta_author; ?>">
      <?php
    endif;

    if (!empty($meta_owner)):
      ?>
      <meta name="owner" content="<?= $meta_owner; ?>">
      <?php
    endif;

    if (!empty($meta_copyright)):
      ?>
      <meta name="copyright" content="<?= $meta_copyright; ?>">
      <?php
    endif;
    ?>

    <!-- MISC META -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <meta name="apple-mobile-web-app-capable" content="yes"/>

    <!-- META FOR SOCIALS -->
    <?php
    if (!empty($title)):
      ?><meta property="og:title" content="<?= $title; ?>">
      <?php
    endif;
    if (!empty($meta_description)):
      ?><meta property="og:description" content="<?= $meta_description; ?>">
      <?php
    endif;

    /*
      <!-- <meta property="og:image" content=""> -->
      <!-- <meta property="og:url" content=""> -->
      <!-- <meta name="twitter:card" content=""> -->
     */
    ?>


    <!--[if IE 5]>
    <script src="/assets/js/ie/stop_ie.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if IE 6]>
    <script src="/assets/js/ie/stop_ie.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if IE 7]>
    <script src="/assets/js/ie/stop_ie.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if IE 8]>
    <script src="/assets/js/ie/stop_ie.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if IE 9]>
    <script src="/assets/js/ie/stop_ie.js" type="text/javascript"></script>
    <![endif]-->

    <!-- CSS -->
    <link href="/assets/themes/default/css.css" rel="stylesheet" media="screen">

    <!-- SCRIPTS -->
    <script src="/assets/themes/default/js/jquery-3.4.0.min.js"></script>
    <script src="/assets/themes/default/js/js.js"></script>

    <!-- FAVICON -->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico">
    <?php
// <!-- VERIFICATIONS -->
// <meta name="google-site-verification" content="********-********" />
    ?>

    <?php
    if ($this->uri->segment(1) !== 'admin') :
      ?>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122949295-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
          dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-122949295-1');
      </script>
      <?php
    endif;
    ?>

    <?php
    if (!empty($structured_data)):
      ?><!-- Structured Data -->
      <script type="application/ld+json">
  <?php echo $structured_data; ?>
      </script>
      <?php
    endif;
    ?>
  </head>
  <body class="theme-default" id="body">

    <div class="site-wrapper">

      <!--noindex-->
      <noscript>Your browser does not support JavaScript. Probably it is disabled. You should enable JavaScript to continue browsing this website.</noscript>
      <!--/noindex-->
