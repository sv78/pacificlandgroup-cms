<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<!-- Tile Menu -->

<!-- Line 1 -->

<div class="tile-menu">




  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_hblua">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/land-use-planning/highest-and-best-use-land-analysis"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/land-use-planning/highest-and-best-use-land-analysis" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Highest &&nbsp;Best Land Use Analysis</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_pm">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="#"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="#" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Project Management</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_pc">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="#"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="#" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Public Consultation</div>
      </a>
    </div>

  </div>



</div>




<!-- Line 2 -->


<div class="tile-menu">




  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_ead">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="#"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="#" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Exploration, Aquisition &&nbsp;Disposition</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_lda">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="#"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="#" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Land Development &&nbsp;Approvals</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_s">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="#"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="#" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Sustainability</div>
      </a>
    </div>

  </div>



</div>
