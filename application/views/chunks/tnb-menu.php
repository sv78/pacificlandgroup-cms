<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="menu">

  <div class="menu__item">
    <div class="menu__item-title">
      <a href="/land-use-planning">Land Use Planning</a>
    </div>
    <ul class="menu__ul-1">
      <li>
        <a href="/land-use-planning">Urban and Agricultural Land Use Planning</a>
      </li>
      <li>
        <a href="/land-use-planning/project-management">Project Management</a>
      </li>
      <li>
        <a href="/land-use-planning/highest-best-use-land-analysis">Highest &&nbsp;Best Use Land Analysis</a>
      </li>
      <li>
        <a href="/land-use-planning/public-consultation">Public Consultation</a>
      </li>
      <li>
        <a href="/land-use-planning/expropriation-acquisition-disposition">Expropriation, Acquisition &&nbsp;Disposition</a>
      </li>
      <li>
        <a href="/land-use-planning/land-development-and-approvals">Land Development &&nbsp;Approvals</a>
      </li>
      <li>
        <a href="/land-use-planning/sustainable-development">Sustainable Development</a>
      </li>
    </ul>
  </div>

  <div class="menu__item">
    <div class="menu__item-title">
      <a href="/environment">Environment</a>
    </div>
    <ul class="menu__ul-1">
      <li>
        <a href="/environment">Qualified Environment Professionals</a>
      </li>
      <li>
        <a href="/environment/environmental-assessments">Environmental Assessments</a>
      </li>
      <li>
        <a href="/environment/erosion-sediment-control">Erosion And Sediment Control</a>
      </li>
      <li>
        <a href="/environment/environmental-monitoring">Environmental Monitoring</a>
      </li>
      <li>
        <a href="/environment/permits-and-approvals">Permits and Approvals</a>
      </li>
      <li>
        <a href="/environment/environmental-case-studies">Environmental Case Studies</a>
      </li>
      <li>
        <a href="/environment/environmental-glossary">Environmental and Land Planning Glossary</a>
      </li>
    </ul>
  </div>

  <div class="menu__item">
    <div class="menu__item-title">
      <a href="/civil-engineering">Civil Engineering</a>
    </div>
    <ul class="menu__ul-1">
      <li>
        <a href="/civil-engineering">Civil Engineering Services</a>
      </li>
      <li>
        <a href="/civil-engineering/engineering-design-construction">Engineering Design &&nbsp;Construction</a>
      </li>
      <li>
        <a href="/civil-engineering/construction-tendering">Construction Tendering</a>
      </li>
      <li>
        <a href="/civil-engineering/construction-cost-estimate">Feasibility Studies &&nbsp;Construction Cost Estimates</a>
      </li>
    </ul>
  </div>

  <div class="menu__item">
    <div class="menu__item-title">
      <a href="/land-survey">Land Survey</a>
    </div>
  </div>

  <div class="menu__item">
    <div class="menu__item-title">
      <a href="/success-stories">Success Stories</a>
    </div>
    <ul class="menu__ul-1">
      <li>
        <a href="/success-stories">Overview</a>
      </li>
      <li>
        <a href="/success-stories/sustainability/">Sustainability</a>
      </li>
      <li>
        <a href="/success-stories/agricultural-land">Agricultural Land</a>
      </li>
      <li>
        <a href="/success-stories/expropriation-impact-assessment">Expropriation Impact Assessment / Highest &&nbsp;Best Land Use Analysis</a>
      </li>
      <li>
        <a href="/success-stories/commercial-industrial-development">Commercial &&nbsp;Industrial Development</a>
      </li>
      <li>
        <a href="/success-stories/residential-development">Residential Development</a>
      </li>
    </ul>
  </div>


</div>

<script>
  /*
   var sgm = ("/" + location.pathname.split('/')[1]).toLowerCase();
   var tnb_links = document.querySelector('.tnb-menu__container').querySelectorAll('a');
   if (tnb_links !== null) {
   for (var i = 0; i < tnb_links.length; i++) {
   if (sgm === tnb_links[i].getAttribute('href').toLowerCase()) {
   $(tnb_links[i]).addClass('tnb-menu__active-link');
   }
   }
   }
   * 
   */
</script>