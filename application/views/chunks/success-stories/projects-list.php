<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!empty($project_list)) {
  ?>
  <ul>
    <?php
    foreach ($project_list as $project) :
      ?>
      <li>
        <a href="/success-stories/<?= $project['cat_slug'] . "/" . $project['project_slug']; ?>" class="color_blue _bold"><?= Str::decode_plain_string($project['project_name']); ?></a>
        <br>
        <small><?= Str::decode_plain_string($project['lead']); ?></small>
      </li>
      <?php
    endforeach;
    ?>
  </ul>
  <?php
}