<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="tnb-menu">
  <div class="container-fluid" style="overflow: visible;">
    <div class="tnb-menu__container">

      <a href="/land-use-planning">
        <div class="tnb-menu__link-container">
          <div>Land Use Planning</div>
        </div>
      </a>

      <a href="/environment">
        <div class="tnb-menu__link-container">Environment</div>
      </a>

      <a href="/civil-engineering">
        <div class="tnb-menu__link-container">Civil Engineering</div>
      </a>

      <a href="/land-survey">
        <div class="tnb-menu__link-container" >Land Survey</div>
      </a>

      <a href="/success-stories">
        <div class="tnb-menu__link-container">Success Stories</div>
      </a>

    </div>
  </div>
</div>

<script>
  var sgm = ("/" + location.pathname.split('/')[1]).toLowerCase();
  var tnb_links = document.querySelector('.tnb-menu__container').querySelectorAll('a');
  if (tnb_links !== null) {
    for (var i = 0; i < tnb_links.length; i++) {
      if (sgm === tnb_links[i].getAttribute('href').toLowerCase()) {
        $(tnb_links[i]).addClass('tnb-menu__active-link');
      }
    }
  }
</script>