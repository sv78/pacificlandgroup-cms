<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/pages/project-management/loblaw-food-distribution-facility.jpg" alt="Loblaw Food Distribution Facility" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/sustainability/loblaws-food-distribution-centre-surrey-bc">Loblaw Food Distribution Facility</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>