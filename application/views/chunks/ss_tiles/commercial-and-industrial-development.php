<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/pages/land-dev-and-approvals/commercial-and-industrial-development.jpg" alt="Commercial & Industrial Development" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/commercial-industrial-development">Commercial &&nbsp;Industrial Development</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>