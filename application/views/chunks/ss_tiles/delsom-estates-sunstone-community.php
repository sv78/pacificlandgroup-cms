<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/pages/project-management/delsom-estates.jpg" alt="Delsom Estates (Sunstone Community)" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/sustainability/delsom-estates-sunstone-community">Delsom Estates (Sunstone Community)</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>