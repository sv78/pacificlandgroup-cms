<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/pages/project-management/gas-stations.jpg" alt="Gas Stations (Shell, Chevron and Husky at various locations)" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/commercial-industrial-development/gas-stations">Gas Stations</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>