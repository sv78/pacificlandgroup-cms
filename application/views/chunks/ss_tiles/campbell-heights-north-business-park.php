<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/pages/project-management/campbell-heights-north-business-park.jpg" alt="Campbell Heights North Business Park" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/commercial-industrial-development/campbell-heights-north-business-park-phase-1-surrey-bc">Campbell Heights North Business Park</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>