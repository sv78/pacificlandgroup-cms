<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/expropriation-acquisition-disposition/BC_Hydro_Transmission_ROW.jpg" alt="BC Hydro Transmission ROW" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/expropriation-impact-assessment/bc-hydro-transmission-row">BC Hydro Transmission ROW</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>