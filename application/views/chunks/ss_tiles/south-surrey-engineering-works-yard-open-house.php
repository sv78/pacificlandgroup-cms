<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/pages/land-use-planning/public-consultation/south-surrey-public-works-yard.jpg" alt="South Surrey Engineering Works Yard Open House" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/commercial-industrial-development/south-surrey-engineering-work-yard-open-house">South Surrey Engineering Works Yard Open House</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>