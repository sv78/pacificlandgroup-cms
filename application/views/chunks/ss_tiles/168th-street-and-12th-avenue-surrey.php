<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/pages/land-use-planning/highest-best-use-land-analysis/168-street-12-avenue-surrey.jpg" alt="168th Street and 12th Avenue, Surrey" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/residential-development/168-street-12-avenue-surrey">168th Street and 12th Avenue, Surrey</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>