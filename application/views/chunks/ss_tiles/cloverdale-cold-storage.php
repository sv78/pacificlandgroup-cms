<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/pages/project-management/cloverdale-cold-storage.jpg" alt="Cloverdale Cold Storage" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/commercial-industrial-development/cloverdale-cold-storage-surrey-bc">Cloverdale Cold Storage</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>