<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/pages/public-consultation/64th-avenue.jpg" alt="64th Avenue / Mufford Crescent Overpass" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/agricultural-land/64th-avenue-mufford-crescent-overpass">64th Avenue / Mufford Crescent Overpass</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>