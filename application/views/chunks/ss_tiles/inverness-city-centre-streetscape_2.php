<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/highest-and-best-use-analysis/Inverness_City_Centre_Streetscape_View.jpg" alt="Inverness City Centre Streetscape" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="#">Inverness City Centre Streetscape</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>