<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/highest-and-best-use-analysis/Cornish_Mining_World_Heritage.jpg" alt="Cornish Mining World Heritage Site SPD" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="#">Cornish Mining World Heritage Site SPD</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>
