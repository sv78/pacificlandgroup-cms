<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/expropriation-acquisition-disposition/192_Street_Overpass.jpg" alt="192 Street Overpass" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/expropriation-impact-assessment/192-street-overpass">192 Street Overpass</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>