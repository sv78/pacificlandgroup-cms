<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/pages/image-required.jpg" alt="196 Street Overpass" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="#">196 Street Overpass</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>