<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/pages/public-consultation/pacificlink-business-park.jpg" alt="PacificLink Business Park" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/commercial-industrial-development/pacificlink-business-park-south-westminster-surrey-bc">PacificLink Business Park</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>