<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/highest-and-best-use-analysis/Plan_for_Battle_of_Northampton.jpg" alt="Conversation Management Plan for the Site of the Battle of Northampton" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="#">Conversation Management Plan for the Site of the Battle of Northampton</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>