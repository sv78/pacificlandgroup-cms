<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/expropriation-acquisition-disposition/Eagleridge_Lands.jpg" alt="Eagleridge Lands" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/expropriation-impact-assessment/eagleridge-lands-west-vancouver">Eagleridge Lands</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>