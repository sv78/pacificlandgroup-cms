<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/pages/land-dev-and-approvals/agricultural-land.jpg" alt="Agricultural Land" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/agricultural-land">Agricultural Land</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>