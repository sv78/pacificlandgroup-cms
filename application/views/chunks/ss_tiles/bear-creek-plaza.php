<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="tile-cards__item">
  <div class="tile-cards__img-holder">
    <img src="/assets/uploads/pages/land-use-planning/highest-best-use-land-analysis/osp_bear-creek.jpg" alt="Bear Creek Plaza" class="tile-cards__img">
  </div>
  <div class="tile-cards__txt-holder">
    <a class="tile-cards__link" href="/success-stories/commercial-industrial-development/bear-creek-plaza">Bear Creek Plaza</a>
    <div class="tile-cards__underliner"></div>
  </div>
</div>