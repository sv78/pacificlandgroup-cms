<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<!-- Tile Menu -->

<!-- Line 1 -->

<div class="tile-menu">




  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_civil-engineering_engineeing-design">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/civil-engineering/engineering-design-construction"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/civil-engineering/engineering-design-construction" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Engineering Design &&nbsp;Construction</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_civil-engineering_construction-tendering">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/civil-engineering/construction-tendering"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/civil-engineering/construction-tendering" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Construction Tendering</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_civil-engineering_feasibilities-studies">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/civil-engineering/construction-cost-estimate"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/civil-engineering/construction-cost-estimate" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Feasibility Studies &&nbsp;Construction Cost Estimate</div>
      </a>
    </div>

  </div>



</div>
