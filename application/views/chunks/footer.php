<?php	defined('APPPATH')	OR	exit('No direct file access allowed…');	?>
<footer>
				<div class="footer__container">

								<div class="footer__box">
												<div class="footer__box-1-offset">
																<p class="footer__plg">Pacific Land Group</p>
																<address class="footer__address">212 - 12992 76 Avenue, Surrey, British Columbia V3W 2V6</address>
																<p class="footer__phone-p">Phone: <a class="footer__phone-a" href="tel:<?=	$this->config->item('phone_number');	?>"><?=	$this->config->item('phone_number_view');	?></a></p>
																<p class="footer__email-p"><a class="footer__email-a" href="mailto:<?=	$this->config->item('customer_requests_from_email');	?>"><?=	$this->config->item('customer_requests_from_email');	?></a></p>

																<div class="footer__social-buttons">
																				<div class="footer__social-buttons-container">
																								<a href="https://twitter.com/pacificlandgrp?lang=en" class="footer__social-btn footer__social-btn_twitter" title="Pacific Land Group - Twitter"></a>
																								<a href="https://ca.linkedin.com/company/pacific-land-group" class="footer__social-btn footer__social-btn_linkedin" title="Pacific Land Group - Linked In"></a>
																				</div>

																</div>
												</div>

												<img class="footer__logo" src="/assets/themes/default/img/plg_logo.svg" alt="Pacific Land Group Logo">

								</div>

								<div class="footer__box">
												<div class="footer__big-links">
																<div>
																				<a href="/pacific-land-group-team">Who we are</a>
																</div>
																<div>
																				<a href="/land-planning-solutions">What we do</a>
																</div>
																<div>
																				<a href="/success-stories">Our work</a>
																</div>
																<div>
																				<a href="/land-planner-corner">News &&nbsp;Events</a>
																</div>
																<div>
																				<a href="/contact-us">Contact</a>
																</div>
																<div>
																				<a href="/login">Login</a>
																</div>
												</div>
								</div>

								<div class="footer__box">
												<ul class="footer__links">
																

																<li class="footer__links_header">
																				<a href="/land-use-planning">Land use planning</a>
																</li>
																<li>
																				<a href="/land-use-planning/project-management">Project management</a>
																</li>
																<li>
																				<a href="/land-use-planning/highest-best-use-land-analysis">Highest &&nbsp;best use land analysis</a>
																</li>
																<li>
																				<a href="/land-use-planning/public-consultation">Public consultation</a>
																</li>
																<li>
																				<a href="/land-use-planning/expropriation-acquisition-disposition">Expropriation, acquisition &&nbsp;disposition</a>
																</li>
																<li>
																				<a href="/land-use-planning/land-development-and-approvals">Land development &&nbsp;approvals</a>
																</li>
																<li>
																				<a href="/land-use-planning/sustainable-development">Sustainable Development</a>
																</li>


																<li class="footer__links_header">
																				<a href="/success-stories">Success stories</a>
																</li>
																<li>
																				<a href="/success-stories/sustainability">Sustainability</a>
																</li>
																<li>
																				<a href="/success-stories/agricultural-land">Agricultural land</a>
																</li>
																<li>
																				<a href="/success-stories/expropriation-impact-assessment">Expropriation impact assessment / Highest &&nbsp;best land use analysis</a>
																</li>
																<li>
																				<a href="/success-stories/commercial-industrial-development">Commercial &&nbsp;industrial development</a>
																</li>
																<li>
																				<a href="/success-stories/residential-development">Residential development</a>
																</li>

																
												</ul>
								</div>

								<div class="footer__box">
												<ul class="footer__links">
																

																<li class="footer__links_header">
																				<a href="/environment">Environment</a>
																</li>
																<li>
																				<a href="/environment/erosion-sediment-control">Erosion and sediment control</a>
																</li>
																<li>
																				<a href="/environment/environmental-assessments">Environmental Assessment</a>
																</li>
																<li>
																				<a href="/environment/permits-and-approvals">Permits &&nbsp;approvals</a>
																</li>
																<li>
																				<a href="/environment/environmental-case-studies">Case studies</a>
																</li>
																<li>
																				<a href="/environment/environmental-monitoring">Environmental Monitoring</a>
																</li>
																<li>
																				<a href="/environment/environmental-glossary">Environmental Glossary</a>
																</li>



																<li class="footer__links_header">
																				<a href="/civil-engineering">Civil Engineering</a>
																</li>
																<li>
																				<a href="/civil-engineering/engineering-design-construction">Engineering design &&nbsp;construction</a>
																</li>
																<li>
																				<a href="/civil-engineering/construction-tendering">Construction tendering</a>
																</li>
																<li>
																				<a href="/civil-engineering/construction-cost-estimate">Feasibility studies &&nbsp;construction cost estimates</a>
																</li>

																
												</ul>
								</div>

				</div>
</footer>