<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="staff-card__icons-panel">
  <a href="/contact-us" class="staff-card__icon staff-card__icon_email"></a>
  <a href="tel:<?= $this->config->item('phone_number'); ?>" class="staff-card__icon staff-card__icon_phone"></a>
  <a href="tel:<?= $this->config->item('phone_number'); ?>" class="staff-card__icon-prone-number staff-card__icon-prone-number_hidden"><?= $this->config->item('phone_number_view'); ?></a>
</div>