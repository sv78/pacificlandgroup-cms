<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="staff-card">
  <div class="staff-card__img-container">
    <img src="/assets/uploads/staff/eric-wang.jpg" alt="Eric Wang">
  </div>
  <div class="staff-card__txt-container">
    <h5 class="staff-card__header">Talk to us</h5>
    <p>Talk to us about Strategic Planning & Assessment, please contact <a href="/pacific-land-group-team#plg_staff_eric_wang" class="color_a _bold _italic">Eric</a>, <mark>MSc., MRTPI, MCIP, RPP Senoir Urban Design Planner</mark></p>
        <?php
        $this->load->view('chunks/staff-cards/icons-block');
        ?>
  </div>
</div>