<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="staff-card">
  <div class="staff-card__img-container">
    <img src="/assets/uploads/staff/oleg-verbenkov.jpg" alt="Oleg Verbenkov">
  </div>
  <div class="staff-card__txt-container">
    <h5 class="staff-card__header">Talk to us</h5>
    <p>Talk to us about Land Surveying. Please contact <a href="/pacific-land-group-team#plg_staff_oleg_verbenkov" class="color_a _bold _italic">Oleg</a> a senior planner and senior project manager with over 30 years of combined public and private sector experience in managing land use and development matters. Understanding exactly what needs to be surveyed (topographic, trees, top of bank, extent of road way and utilities) is critical in order to build a base for a successful project.</p>
    <?php
    $this->load->view('chunks/staff-cards/icons-block');
    ?>
  </div>
</div>