<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="staff-card">
  <div class="staff-card__img-container">
    <img src="/assets/uploads/staff/oleg-verbenkov.jpg" alt="Oleg Verbenkov">
  </div>
  <div class="staff-card__txt-container">
    <h5 class="staff-card__header">Talk to us</h5>
    <p>Talk to us about construction tendering. Please contact <a href="/pacific-land-group-team#plg_staff_oleg_verbenkov" class="color_a _bold _italic">Oleg</a> a senior planner and senior project manager with over 30 years of combined public and private sector experience in managing land use and development matters that include supportive disciplines such as: <mark>engineering, environmental, urban design, landscape architecture, survey, appraisal and land economics</mark>.</p>
        <?php
        $this->load->view('chunks/staff-cards/icons-block');
        ?>
  </div>
</div>