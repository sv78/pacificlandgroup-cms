<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="staff-card">
  <div class="staff-card__img-container">
    <img src="/assets/uploads/staff/laura-jones.jpg" alt="Laura">
  </div>
  <div class="staff-card__txt-container">
    <h5 class="staff-card__header">Talk to us</h5>
    <p>Talk to <a href="/pacific-land-group-team#plg_staff_laura_jones" class="color_a _bold _italic">Laura</a> an experienced development Planner with a local government experience background.  Laura excels at preparing and managing development applications. She is skilled in <mark>project coordination with local government agencies, engineers, surveyors, architects, landscape architects and project managers</mark>.</p>
        <?php
        $this->load->view('chunks/staff-cards/icons-block');
        ?>
  </div>
</div>