<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="staff-card">
  <div class="staff-card__img-container">
    <img src="/assets/uploads/staff/kyla.jpg" alt="Kyla">
  </div>
  <div class="staff-card__txt-container">
    <h5 class="staff-card__header">Talk to us</h5>
    <p>Talk to our environmental professionals. Please contact <a href="/pacific-land-group-team#plg_staff_kyla" class="color_a _bold _italic">Kyla</a>, a QEP with experience in <mark>watercourse assessments, Riparian Area Regulation (RAR) assessments, bird nest surveys, fish and wildlife salvages, environmental monitoring, and the preparation of Sensitive Ecosystem Development Plans (SEDP), Construction Environmental Management Plans (CEMP), and Invasive Species Management Plans (ISMP)</mark>.</p>
        <?php
        $this->load->view('chunks/staff-cards/icons-block');
        ?>
  </div>
</div>