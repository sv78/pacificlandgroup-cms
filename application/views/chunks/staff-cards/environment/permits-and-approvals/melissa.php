<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="staff-card">
  <div class="staff-card__img-container">
    <img src="/assets/uploads/staff/melissa.jpg" alt="Melissa">
  </div>
  <div class="staff-card__txt-container">
    <h5 class="staff-card__header">Talk to us</h5>
    <p>Talk to our environmental professionals. Please contact <a href="/pacific-land-group-team#plg_staff_melissa" class="color_a _bold _italic">Melissa</a>, a Biologist in Training (BIT) with experience in <mark>invasive vegetation management, riparian habitat restoration, watercourse assessments, environmental sampling, and water quality monitoring</mark>.</p>
        <?php
        $this->load->view('chunks/staff-cards/icons-block');
        ?>
  </div>
</div>