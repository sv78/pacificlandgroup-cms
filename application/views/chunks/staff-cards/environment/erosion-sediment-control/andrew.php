<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="staff-card">
  <div class="staff-card__img-container">
    <img src="/assets/uploads/staff/andrew.jpg" alt="Andrew">
  </div>
  <div class="staff-card__txt-container">
    <h5 class="staff-card__header">Talk to us</h5>
    <p>Talk to our environmental professionals. Please contact <a href="/pacific-land-group-team#plg_staff_andrew" class="color_a _bold _italic">Andrew</a>, a Biologist in Training (BIT) with experience in <mark>Erosion and Sediment Control (ESC) monitoring, fish salvages, bird nest surveys, and Floc Systems Inc. Tigerfloc product installation and maintenance</mark>.</p>
        <?php
        $this->load->view('chunks/staff-cards/icons-block');
        ?>
  </div>
</div>