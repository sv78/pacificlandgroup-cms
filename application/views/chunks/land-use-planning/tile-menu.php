<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<!-- Tile Menu -->

<!-- Line 1 -->

<div class="tile-menu">




  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_hblua">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/land-use-planning/highest-best-use-land-analysis"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/land-use-planning/highest-best-use-land-analysis" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Highest &&nbsp;Best Land Use Analysis</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_pm">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/land-use-planning/project-management"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/land-use-planning/project-management" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Project Management</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_pc2">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/land-use-planning/public-consultation"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/land-use-planning/public-consultation" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Public Consultation</div>
      </a>
    </div>

  </div>



</div>




<!-- Line 2 -->


<div class="tile-menu">




  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_ead">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/land-use-planning/expropriation-acquisition-disposition"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/land-use-planning/expropriation-acquisition-disposition" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Expropriation, Aquisition &&nbsp;Disposition</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_lda">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/land-use-planning/land-development-and-approvals"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/land-use-planning/land-development-and-approvals" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Land Development &&nbsp;Approvals</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_s">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/land-use-planning/sustainable-development"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/land-use-planning/sustainable-development" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Sustainable Development</div>
      </a>
    </div>

  </div>



</div>
