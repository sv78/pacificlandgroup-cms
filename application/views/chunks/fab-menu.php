<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<div class="fab-menu-open menustarter-btn menustarter-btn_hidden" title="Click here to enter menu"></div>
<div class="backtotop-btn backtotop-btn_hidden" title="Click here to return back to the top of the page"></div>

<div class="fab-menu">
  <div class="fab-menu__state-container">

    <div class="fab-menu__screen-fader"></div>
    <div class="fab-menu__container">


      <div class="fab-menu__content-scroller">

        <div class="fab-menu__content-holder-outer">

          <div class="fab-menu__content-holder-inner">


            <!-- 1-st block -->


            <div class="fab-menu__block">

              <div class="fab-menu__category">
                <a href="/">Pacific Land Group</a>
              </div>

              <div class="fab-menu__category">
                <a href="/pacific-land-group-team">Who We Are</a>
              </div>

              <div class="fab-menu__category">
                <a href="/land-planning-solutions">What We Do</a>
              </div>

              <div class="fab-menu__category">
                <a href="/land-planner-corner">Industry News Corner</a>
              </div>

              <div class="fab-menu__category">
                <a href="/contact-us">Contact Us</a>
              </div>

            </div>

            <!-- 2-nd block -->

            <div class="fab-menu__block">

              <div class="fab-menu__category">
                <a href="/land-use-planning">Land Use Planning</a>

                <div class="fab-menu__category-items-container">

                  <div class="fab-menu__category-item">
                    <a href="/land-use-planning">Urban and Agricultural Land Use Planning</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/land-use-planning/project-management">Project Management</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/land-use-planning/highest-best-use-land-analysis">Highest & Best Use Land Analysis</a>
                  </div>
                  
                  <!-- add this class to a element "fab-menu__a-active" -->

                  <div class="fab-menu__category-item">
                    <a href="/land-use-planning/public-consultation">Public Consultation</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/land-use-planning/expropriation-acquisition-disposition">Expropriation, Acquisition & Disposition</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/land-use-planning/land-development-and-approvals">Land Development & Approvals</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/land-use-planning/sustainable-development">Sustainable Development</a>
                  </div>

                </div>

              </div>





              <div class="fab-menu__category">
                <a href="/environment">Environment</a>

                <div class="fab-menu__category-items-container">

                  <div class="fab-menu__category-item">
                    <a href="/environment">Qualified Environment Professionals</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/environment/environmental-assessments">Environmental Assessments</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/environment/erosion-sediment-control">Erosion And Sediment Control</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/environment/environmental-monitoring">Environmental Monitoring</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/environment/permits-and-approvals">Permits and Approvals</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/environment/environmental-case-studies">Environmental Case Studies</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/environment/environmental-glossary">Environmental and Land Planning Glossary</a>
                  </div>

                </div>

              </div>







              <div class="fab-menu__category">
                <a href="/civil-engineering">Civil Engineering</a>

                <div class="fab-menu__category-items-container">

                  <div class="fab-menu__category-item">
                    <a href="/civil-engineering">Civil Engineering Services</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/civil-engineering/engineering-design-construction">Engineering Design & Construction</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/civil-engineering/construction-tendering">Construction Tendering</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/civil-engineering/construction-cost-estimate">Feasibility Studies & Construction Cost Estimates</a>
                  </div>

                </div>

              </div>







              <div class="fab-menu__category">
                <a href="/land-survey">Land Survey</a>
              </div>






              <div class="fab-menu__category">
                <a href="/success-stories/">Success Stories</a>

                <div class="fab-menu__category-items-container">

                  <div class="fab-menu__category-item">
                    <a href="/success-stories">Overview</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/success-stories/sustainability/">Sustainability</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/success-stories/agricultural-land">Agricultural Land</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/success-stories/expropriation-impact-assessment">Expropriation Impact Assessment / Highest & Best Land Use Analysis</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/success-stories/commercial-industrial-development">Commercial & Industrial Development</a>
                  </div>

                  <div class="fab-menu__category-item">
                    <a href="/success-stories/residential-development">Residential Development</a>
                  </div>

                </div>

              </div>


            </div>

            <!--  image block -->

            <div class="fab-menu__img"></div>


          </div>

        </div>

      </div>


      <!--  close button -->

      <div class="fab-menu__close-button fab-menu-close" title="Click here or press `Esc` to close this menu"></div>

    </div>
  </div>
</div>
