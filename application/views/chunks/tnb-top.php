<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="tnb-top">

  <div class="tnb-top__left-part">

    <div class="tnb-top__menu-caller-container">
      <div class="tnb-top__menu-caller">

        <div class="fab-menu-open" title="Click here to enter menu">
          <svg viewBox="0 0 24 24">
          <path d="M0 0h24v24H0z" fill="none"/>
          <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/>
          </svg>
        </div>

      </div>
      <div class="tnb-top__menu-caller-description">MENU</div>
    </div>

    <a class="tnb-top__logo" href="/" title="Pacific Land Group - Home"></a>

  </div>

  <div class="tnb-top__right-part">

    <div class="tnb-top__phone-container">
      <div class="tnb-top__phone"><?= $this->config->item('phone_number_view'); ?></div>
      <a class="tnb-top__phone-icon" href="tel:<?= $this->config->item('phone_number'); ?>"></a>
    </div>

    <ul class="tnb-top__links">
      <li>
        <a href="/pacific-land-group-team">Who we are</a>
        <div></div>
      </li>
      <li>
        <a href="/contact-us">Contact us</a>
        <div></div>
      </li>
      <li>
        <a href="/land-planner-corner">Industry news corner</a>
        <div></div>
      </li>
    </ul>

  </div>


</div>

<script>
  var ttl_links = document.querySelector('.tnb-top__links').querySelectorAll('a');
  if (ttl_links != null) {
    for (var i = 0; i < ttl_links.length; i++) {
      if (window.location.pathname.toLowerCase() == ttl_links[i].getAttribute('href')) {
        // $(ttl_links[i].parentNode).addClass('tnb-top__link-active');
        highlight(ttl_links[i]);
      }
    }
  }
  function highlight(elm) {
    setTimeout(function () {
      $(elm.parentNode).addClass('tnb-top__link-active');
    }, 300);
  }
</script>