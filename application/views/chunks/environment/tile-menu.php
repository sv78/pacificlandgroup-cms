<?php defined('APPPATH') OR exit('No direct file access allowed…'); ?>

<!-- Tile Menu -->

<!-- Line 1 -->

<div class="tile-menu">


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_environment_fish-wildlife-habitat-assessments">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/environment/environmental-assessments"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/environment/environmental-assessments" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Environmental Assessments</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_environment_sediment-erosion-control">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/environment/erosion-sediment-control"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="environment/erosion-sediment-control" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Erosion And Sediment Control</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_environment_environmental-monitoring">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/environment/environmental-monitoring"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/environment/environmental-monitoring" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Environmental Monitoring</div>
      </a>
    </div>

  </div>


</div>




<!-- Line 2 -->


<div class="tile-menu">



  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_environment_permits-and-approvals">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/environment/permits-and-approvals"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/environment/permits-and-approvals" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Permits &&nbsp;Approvals</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_environment_environmental-case-studies">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/environment/environmental-case-studies"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/environment/environmental-case-studies" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Environmental Case Studies</div>
      </a>
    </div>

  </div>


  <div class="tile-menu__item">

    <div class="tile-menu__img tile-menu__img_environment_environmental-glossary">
      <div class="tile-menu__img-overlay"></div>
      <a class="tile-menu__img-link" href="/environment/environmental-glossary"></a>
    </div>

    <div class="tile-menu__txt-container">
      <a href="/environment/environmental-glossary" class="tile-menu__txt-box-link">
        <div class="tile-menu__txt-content">Environmental Glossary</div>
      </a>
    </div>

  </div>



</div>
