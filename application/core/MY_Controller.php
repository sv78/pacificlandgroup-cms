<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// class
class MY_controller extends CI_Controller {

  public $canonical_url;

  // CONSTRUCTOR
  public function __construct() {

    parent::__construct();
    
    /*
     * Prevents ability to call controller's name in uri
     * as Codeigniter it makes possible by default
     */
    if (strtolower($this->uri->segment(1)) == strtolower(get_class($this))) {
      show_404();
    }

    /*
     * Denying index.php
     * now index.php request shows 404
     */
    $this->denyIndexPhp("/index.php");

    $this->canonical_url = "https://" . filter_input(INPUT_SERVER, 'HTTP_HOST') . "/" . uri_string();

    /*
     * Enables Codeigniter internal profiler here,
     * which shows useful info at the bottom of page
     * Crashes AJAX requests because of text output!!!
     */
    // $this->output->enable_profiler(true);
  }

  //METHODS

  /**
   * Ignoring index.php or any defined URI
   * 
   * @param string $str строка uri
   */
  protected function denyIndexPhp($str) {
    if (strtolower(explode("?", $_SERVER['REQUEST_URI'])[0]) == $str) {
      show_404();
    }
  }

}

/*
 * ****************************************************
 * *** EXTENDED CONTROLLER CLASSES ********************
 * ****************************************************
 */

// class
class Admin_controller extends MY_controller {

  public function __construct() {
    parent::__construct();

    if (!$this->auth->is_admin()) {
      //show_error("Server Error", 500, '500 Server Error');
      show_404();
    }

    /*
      if (empty($_SESSION['user']['is_admin']) OR $_SESSION['user']['is_admin'] != 1) {
      log_message('error', __FILE__ . " - line " . __LINE__ . " - attempt to access into admin zone without admin status in session");
      show_error("Server Error", 500, '500 Server Error');
      }
     * 
     */
  }

  /* public function my_map($uri) {
    echo __METHOD__ . ' : $uri = ' . $uri;
    } */
}

/*
 * ****************************************************
 * *** AJAX *******************************************
 * ****************************************************
 */

// class
class Ajax_controller extends MY_controller {

  public function __construct() {
    parent::__construct();
    // !!! раскоментируй условие ниже, а также придумай для ajax более крутую проверку
    /*
      if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || ( strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest' ) || $_SERVER['HTTP_REFERER'] == '') {
      log_message('error', __FILE__ . " - line " . __LINE__ . " - ajax direct attempt");
      show_error("The page you requested was not found", 404, '404 Page Not Found');
      }
     * 
     */
  }

  public function my_map($uri) {
    if (method_exists($this, $uri)) {
      $this->$uri();
    } else {
      log_message('error', __FILE__ . " - line " . __LINE__ . " - ajax missing method called");
      show_error("Server Error", 500, '500 Server Error');
    }
  }

}

// class
class Admin_ajax_controller extends Admin_controller {

  public function __construct() {
    parent::__construct();
    // !!! раскоментируй условие ниже
    /*
      if (empty($_SESSION['user']['id'])) {
      log_message('error', __FILE__ . " - line " . __LINE__ . " - ajax access attempt without session");
      show_error("Server Error", 500, '500 Server Error');
      }
     * 
     */
  }

  public function my_map($uri) {
    if (method_exists($this, $uri)) {
      $this->$uri();
    } else {
      //log_message('error', __FILE__ . " - line " . __LINE__ . " - ajax missing method called");
      show_error("Server Error", 500, '500 Server Error');
    }
  }

}
