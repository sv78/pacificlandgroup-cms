<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_model extends CI_Model {

  public function get_page_by_id($id, $fields = NULL) {
    $this->db->distinct();
    $this->db->where('id', $id);
    if ($fields != NULL) {
      $this->db->select($fields);
    }
    $query = $this->db->get('pages');
    return $query->row_array();
  }

  public function get_page_by_slug($slug) {
    $this->db->distinct();
    $this->db->where('slug', $slug);
    $query = $this->db->get('pages');
    return $query->row_array();
  }

  public function get_nested_pages_by_id($id) {
    $this->db->order_by('id', "ASC");
    $this->db->where('parent_id', $id);
    $query = $this->db->get('pages');
    return $query->result_array();
  }

  public function get_all_pages($fields = NULL) {
    $this->db->order_by('parent_id', "ASC");
    $this->db->order_by('id', "ASC");
    $this->db->order_by('name', "ASC");
    if ($fields) {
      $this->db->select($fields);
    }
    $query = $this->db->get('pages');
    return $query->result_array();
  }

  public function insert_new_page($db_data = NULL) {
    $db_data['time_modified'] = time();
    $this->db->insert('pages', $db_data);
    return $this->db->insert_id();
  }

  public function count_all_pages() {
    $this->db->select('count(id) AS count_all_pages');
    $query = $this->db->get('pages');
    return $query->row_array()['count_all_pages'];
  }

  public function count_published_pages() {
    $this->db->select('count(id) AS count_published_pages');
    $this->db->where('is_published', 1);
    $query = $this->db->get('pages');
    return $query->row_array()['count_published_pages'];
  }

  public function update_page($id, $db_data) {
    $db_data['time_modified'] = time();
    $this->db->where('id', $id);
    $this->db->update('pages', $db_data);
  }

  public function get_meta_robots_values() {
    // SHOW COLUMNS FROM pages WHERE field = 'meta_robots'
    $sql = "show columns from pages where field = 'meta_robots'";
    $query = $this->db->query($sql);
    $str = $query->row_array()['Type'];
    $str = preg_replace("/(enum\()(.+)(\))/", "$2", $str);
    $str = trim($str, "'");
    $arr = explode("','", $str);
    return $arr;
  }

  public function delete_page($id) {
    $this->db->where('id', $id);
    $this->db->delete('pages');
  }

  public function get_page_id_from_last_slug($uri_segments) {
    $last_slug = $uri_segments[count($uri_segments) - 1];

    // Loading form validation library
    $this->load->library('form_validation');

    // Setting validation rules for page URI slugs
    $this->form_validation->set_data(array('slug' => $last_slug));
    $this->form_validation->set_rules('slug', 'Slug', 'trim|max_length[64]|alpha_dash');

    // Validating slug
    if ($this->form_validation->run() === false) {
      show_404();
    }

    $page = $this->get_page_by_slug($last_slug);
    return $page;
  }

  // end of class
}
