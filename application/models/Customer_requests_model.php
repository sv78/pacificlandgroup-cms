<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_requests_model extends CI_Model {

  public function save_request($db_data = NULL) {
    $this->db->insert('customer_requests', $db_data);
  }

  public function get_requests($order_by = NULL, $order_direction = "ASC") {
    if ($order_by === NULL) {
      $this->db->order_by('time', "DESC");
    } else {
      $this->db->order_by($order_by, $order_direction);
    }
    $this->db->select('id, name, time');
    $this->db->from('customer_requests');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function get_request_by_id($id = NULL) {
    $this->db->distinct();
    $this->db->where('id', $id);
    $this->db->select('id, name, company, email, phone, message, type, foc, time');
    $this->db->from('customer_requests');
    $query = $this->db->get();
    return $query->row_array();
  }

  // end of class
}
