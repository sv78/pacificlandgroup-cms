<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Success_stories_model extends CI_Model {

  public function get_cats() {
    $this->db->order_by('id', "ASC");
    $query = $this->db->get('success_stories_cats');
    return $query->result_array();
  }

  public function get_cat_by_id($id = NULL) {
    $this->db->distinct();
    $this->db->where('id', $id);
    $this->db->select('id, name, slug');
    $this->db->from('success_stories_cats');
    $query = $this->db->get();
    return $query->row_array();
  }
  
  public function get_cat_by_slug($slug = NULL) {
    $this->db->distinct();
    $this->db->where('slug', $slug);
    $this->db->select('id, name, slug');
    $this->db->from('success_stories_cats');
    $query = $this->db->get();
    return $query->row_array();
  }

  public function get_projects_by_cat_id($id = NULL, $only_published = false) {
    $this->db->select(''
            . 'success_stories_projects.id, '
            . 'success_stories_projects.name AS project_name, '
            . 'success_stories_projects.lead, '
            . 'success_stories_projects.slug AS project_slug, '
            . 'success_stories_cats.id AS cat_id, '
            . 'success_stories_cats.name AS cat_name, '
            . 'success_stories_cats.slug AS cat_slug');
    $this->db->from('success_stories_projects');
    $this->db->join('success_stories_cats', 'success_stories_projects.success_stories_cat_id = success_stories_cats.id', 'left');
    $this->db->where('success_stories_cats.id', $id);
    if ($only_published) {
      $this->db->where('success_stories_projects.published', 1);
    }
    $query = $this->db->get();
    return $query->result_array();
  }

  public function get_project_by_id($id = NULL) {
    $this->db->limit(1);
    $this->db->select('id, success_stories_cat_id, name, slug, title, meta_description, lead, html, img_extension, published');
    $this->db->from('success_stories_projects');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->row_array();
  }
  
  public function get_project_by_slug($slug = NULL) {
    $this->db->limit(1);
    $this->db->select('id, success_stories_cat_id, name, slug, title, meta_description, lead, html, img_extension, published');
    $this->db->from('success_stories_projects');
    $this->db->where('slug', $slug);
    $query = $this->db->get();
    return $query->row_array();
  }

  public function insert_new_project($db_data = NULL) {
    $data['name'] = $db_data['name'];
    $data['success_stories_cat_id'] = $db_data['category_id'];
    $data['slug'] = $db_data['slug'];
    $this->db->insert('success_stories_projects', $data);
    return $this->db->insert_id();
  }

  public function slug_exists($slug) {
    $this->db->limit(1);
    $this->db->select('id');
    $this->db->where('slug', $slug);
    $this->db->from('success_stories_projects');
    $query = $this->db->get();
    if (empty($query->row_array())) {
      return false;
    }
    return true;
  }

  public function update_img_extension($project_id, $extension) {
    $this->db->set('img_extension', $extension);
    $this->db->where('id', $project_id);
    $this->db->update('success_stories_projects');
  }

  public function update_project($project_id, $db_data) {
    $this->db->set('success_stories_cat_id', $db_data['category_id']);
    $this->db->set('name', $db_data['name']);
    $this->db->set('title', $db_data['title']);
    $this->db->set('meta_description', $db_data['meta_description']);
    $this->db->set('lead', $db_data['lead']);
    $this->db->set('html', $db_data['html']);
    $this->db->set('published', $db_data['published']);
    $this->db->where('id', $project_id);
    $this->db->update('success_stories_projects');
  }

  public function delete_project($num_id) {
    $this->db->where('id', $num_id);
    $this->db->delete('success_stories_projects');
  }

  // end of class
}
