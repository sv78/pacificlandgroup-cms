<?php

class MyRedirect {

  /**
   *
   * ! IMPORTANT !!! Source URIs in mapping tables must be WiTHOUT trailing slash!
   */
  private $request_uri = null;

  /**
   * URIs to return 410 code
   */
  private $mapping_table_410 = array(
      "/media-center/videos.htm",
      "/about-us/careers.htm",
      "/whats-new.htm",
  );

  /**
   * Example:
   * old uri => new uri
   */
  private $mapping_table_301 = array(
      /**
       * LAND USE PLANNING
       */
      '/land-use-planning/default.htm' => '/land-use-planning',
      '/land-use-planning/projectmanagement.htm' => '/land-use-planning/project-management',
      '/land-use-planning/landuseanalysis.htm' => '/land-use-planning/highest-best-use-land-analysis',
      '/land-use-planning/publicconsultation.htm' => '/land-use-planning/public-consultation',
      '/land-use-planning/expropriation.htm' => '/land-use-planning/expropriation-acquisition-disposition',
      '/land-use-planning/Landdevelopment.htm' => '/land-use-planning/land-development-and-approvals',
      '/land-use-planning/sustainability.htm' => '/land-use-planning/sustainable-development',
      /**
       * ENVIRONMENT
       */
      '/environment/default.htm' => '/environment',
      '/environment/sediment-erosion-control.htm' => '/environment/erosion-sediment-control',
      '/environment/fish-wildlife-assessments.htm' => '/environment/environmental-assessments',
      '/environment/environmental-monitoring.htm' => '/environment/environmental-monitoring',
      /**
       * CIVIL ENGINEERING
       */
      '/civil-engineering/default.htm' => '/civil-engineering',
      '/civil-engineering/engineering-design.htm' => '/civil-engineering/engineering-design-construction',
      '/civil-engineering/construction-tendering.htm' => '/civil-engineering/construction-tendering',
      '/civil-engineering/development-feasibility-studies.htm' => '/civil-engineering/construction-cost-estimate',
      /**
       * LAND SURVEY
       */
      '/land-survey/default.htm' => '/land-survey',
      /**
       * SUCCESS STORIES
       */
      '/success-stories/default.htm' => '/success-stories',
      '/success-stories/sustainability.htm' => '/success-stories/sustainability',
      '/success-stories/residential/delsom.htm' => '/success-stories/sustainability/delsom-estates-sunstone-community',
      '/success-stories/commercial-development/loblaws.htm' => '/success-stories/sustainability/loblaws-food-distribution-centre-surrey-bc',
      '/success-stories/commercial-development/cloverdale.htm' => '/success-stories/sustainability/cloverdale-cold-storage',
      '/success-stories/agricultural-land.htm' => '/success-stories/agricultural-land',
      '/success-stories/agricultural-land/mufford-crescent.htm' => '/success-stories/agricultural-land/64th-avenue-mufford-crescent-overpass',
      '/success-stories/agricultural-land/west-maple-ridge-lands.htm' => '/success-stories/agricultural-land/west-maple-ridge-lands',
      '/success-stories/agricultural-land/mount-lehman-industrial-land%20.htm' => '/success-stories/agricultural-land/mount-lehman-industrial-lands',
      '/success-stories/expropriation-impact.htm' => '/success-stories/expropriation-impact-assessment',
      '/success-stories/expropriation-impact-assesment/eagleridge.htm' => '/success-stories/expropriation-impact-assessment/eagleridge-lands-west-vancouver',
      '/success-stories/expropriation-impact-assesment/bc-hydro.htm' => '/success-stories/expropriation-impact-assessment/bc-hydro-transmission-row',
      '/success-stories/expropriation-impact-assesment/port-mann.htm' => '/success-stories/expropriation-impact-assessment/port-mann-hwy-1-improvement',
      '/success-stories/expropriation-impact-assesment/knight-st.htm' => '/success-stories/expropriation-impact-assessment/knight-street-widening-vancouver-bc',
      '/success-stories/expropriation-impact-assesment/192-st-overpass.htm' => '/success-stories/expropriation-impact-assessment/192-street-overpass',
      '/success-stories/expropriation-impact-assesment/196-st-overpass.htm' => '/success-stories/expropriation-impact-assessment/196-street-overpass',
      '/success-stories/residential/delsom.htm' => '/success-stories/expropriation-impact-assessment/delsom-estates-sunstone-community-project',
      '/success-stories/expropriation-impact-assesment/202-st-parknride.htm' => '/success-stories/expropriation-impact-assessment/202-street-park-ride-township-of-langley-bc',
      '/success-stories/commercial-industrial-development.htm' => '/success-stories/commercial-industrial-development',
      '/success-stories/commercial-development/burnaby-business-park.htm' => '/success-stories/commercial-industrial-development/burnaby-business-park-surrey-bc',
      '/success-stories/commercial-development/pacificlink.htm' => '/success-stories/commercial-industrial-development/pacificlink-business-park-south-westminster-surrey-bc',
      '/success-stories/expropriation-impact-assesment/port-mann.htm' => '/success-stories/commercial-industrial-development/port-mann-highway-1-improvement',
      '/success-stories/commercial-development/88-avenue.htm' => '/success-stories/commercial-industrial-development/88th-avenue-king-george-hwy-surrey-bc',
      '/success-stories/commercial-development/campbell-heights.htm' => '/success-stories/commercial-industrial-development/campbell-heights-north-business-park-phase-1-surrey-bc',
      '/success-stories/expropriation-impact-assesment/196-st-overpass.htm' => '/success-stories/commercial-industrial-development/196-street-overpass-surrey-bc',
      '/success-stories/commercial-development/cloverdale.htm' => '/success-stories/commercial-industrial-development/cloverdale-cold-storage-surrey-bc',
      '/success-stories/commercial-development/loblaws.htm' => '/success-stories/commercial-industrial-development/loblaws-food-distribution-centre-project-surrey-bc',
      '/success-stories/residential-development.htm' => '/success-stories/residential-development',
      '/success-stories/residential/delsom.htm' => '/success-stories/residential-development/delsom-estates-sunstone-community-delta-bc',
      '/success-stories/residential/synergy.htm' => '/success-stories/residential-development/synergy-homes-sunshine-coast-bc',
      /**
       * SEPARATED PAGES
       */
      '/media-center/galleries.htm' => '/land-planner-corner',
      '/contact.aspx' => '/contact-us',
      '/about-us' => '/pacific-land-group-team',
  );

  public function __construct() {
    $this->request_uri = $this->get_request_uri();
  }

  public function redirect() {
    $this->redirect_410();
    $this->redirect_301();
  }

  private function redirect_301() {
    $old_uris = array_keys($this->mapping_table_301);
    if (in_array($this->request_uri, $old_uris)) {
      $new_uri = $this->mapping_table_301[$this->request_uri];
      header("Location: " . $new_uri, true, 301);
      die();
    }
  }

  private function redirect_410() {
    if (in_array($this->request_uri, $this->mapping_table_410)) {
      header("HTTP/1.0 410 Gone");
      die();
    }
  }

  private function get_request_uri() {
    return ("/" . trim(filter_input(INPUT_SERVER, 'REQUEST_URI'), '/'));
  }

}
