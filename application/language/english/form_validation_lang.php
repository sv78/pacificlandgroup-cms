<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// MY RULES

$lang['form_validation_my_unique_page_slug'] = "The &apos;{field}&apos; value duplication! Please enter another one.";

$lang['form_validation_my_differs'] = "The &apos;{field}&apos; can not have the same value.";

$lang['form_validation_my_single_word'] = 'The &apos;{field}&apos; must be a single word and must not contain any other characters than letters of the Latin alphabet.';

$lang['form_validation_my_password_chars'] = 'The &apos;{field}&apos; field contains invalid symbols. Please use the letters of the Latin alphabet, digits and symbols - [ _!@#$%^&*()~+?.[]{} ].';

$lang['form_validation_my_position'] = 'The &apos;{field}&apos; contains invalid character combinations.';

$lang['form_validation_my_forbidden_character_combinations'] = 'The &apos;{field}&apos; contains invalid character combinations.';

$lang['form_validation_my_username'] = 'The &apos;{field}&apos; contains invalid characters or symbols. Please use the letters of the Latin alphabet.';

$lang['form_validation_my_checkbox'] = 'Invalid value received in &apos;{field}&apos; .';

$lang['form_validation_my_submit'] = 'Invalid value received in &apos;{field}&apos; .';

$lang['form_validation_my_is_registered_user'] = 'Our System does not have an account with the email address specified.';

$lang['form_validation_my_password_repeat'] = '&apos;{field}&apos; value does not match to password value.';

///[^0-9A-Za-z\-\_\!\@\#\$\%\^\&\*\(\)\~\+\?\.\[\]\{\}

// OVERRIDE SYSTEM

// $lang['form_validation_required'] = 'Поле &lt;{field}&gt; обязательно должно быть заполнено.';
// $lang['form_validation_valid_email'] = 'Поле &lt;{field}&gt; должно содержать верный адрес email.';
// $lang['form_validation_is_natural'] = 'Поле &lt;{field}&gt; должно содержать только цифры.';
// $lang['form_validation_alpha_numeric']		= 'Поле &lt;{field}&gt; может содержать только латинские буквы или цифры.';

//$lang['form_validation_alpha_numeric_dash']		= 'The {field} field accepts only latin characters, digits and dashes.';
//$lang['form_validation_robots_metatag']		= 'The {field} field error. Use dropdown list values only.';
//$lang['form_validation_checkbox']		= 'The {field} checkbox error.';
//$lang['form_validation_captcha']		= 'The {field} word you typed seems to be incorrect.';
//$lang['form_validation_captcha']		= 'Число на картинке не совпадает с указанным Вами числом.';
//$lang['form_validation_pwd_regexp']		= 'The {field} field uses restricted symbols. No spaces allowed. Non-latin letters are prohibited.';
//$lang['form_validation_reserved_slugs']		= 'The {field} can not be this value. Please use another one.';
//$lang['form_validation_cart_phone']		= 'The {field} has wrong value. Please enter correct data.';


/* SYSTEM */

/*
$lang['form_validation_required']		= 'The {field} field is required.';
$lang['form_validation_isset']			= 'The {field} field must have a value.';
$lang['form_validation_valid_email']		= 'The {field} field must contain a valid email address.';
$lang['form_validation_valid_emails']		= 'The {field} field must contain all valid email addresses.';
$lang['form_validation_valid_url']		= 'The {field} field must contain a valid URL.';
$lang['form_validation_valid_ip']		= 'The {field} field must contain a valid IP.';
$lang['form_validation_min_length']		= 'The {field} field must be at least {param} characters in length.';
$lang['form_validation_max_length']		= 'The {field} field cannot exceed {param} characters in length.';
$lang['form_validation_exact_length']		= 'The {field} field must be exactly {param} characters in length.';
$lang['form_validation_alpha']			= 'The {field} field may only contain alphabetical characters.';
$lang['form_validation_alpha_numeric']		= 'The {field} field may only contain alpha-numeric characters.';
$lang['form_validation_alpha_numeric_spaces']	= 'The {field} field may only contain alpha-numeric characters and spaces.';
$lang['form_validation_alpha_dash']		= 'The {field} field may only contain alpha-numeric characters, underscores, and dashes.';
$lang['form_validation_numeric']		= 'The {field} field must contain only numbers.';
$lang['form_validation_is_numeric']		= 'The {field} field must contain only numeric characters.';
$lang['form_validation_integer']		= 'The {field} field must contain an integer.';
$lang['form_validation_regex_match']		= 'The {field} field is not in the correct format.';
$lang['form_validation_matches']		= 'The {field} field does not match the {param} field.';
$lang['form_validation_differs']		= 'The {field} field must differ from the {param} field.';
$lang['form_validation_is_unique'] 		= 'The {field} field must contain a unique value.';
$lang['form_validation_is_natural']		= 'The {field} field must only contain digits.';
$lang['form_validation_is_natural_no_zero']	= 'The {field} field must only contain digits and must be greater than zero.';
$lang['form_validation_decimal']		= 'The {field} field must contain a decimal number.';
$lang['form_validation_less_than']		= 'The {field} field must contain a number less than {param}.';
$lang['form_validation_less_than_equal_to']	= 'The {field} field must contain a number less than or equal to {param}.';
$lang['form_validation_greater_than']		= 'The {field} field must contain a number greater than {param}.';
$lang['form_validation_greater_than_equal_to']	= 'The {field} field must contain a number greater than or equal to {param}.';
$lang['form_validation_error_message_not_set']	= 'Unable to access an error message corresponding to your field name {field}.';
$lang['form_validation_in_list']		= 'The {field} field must be one of: {param}.';
*/

/* // SYSTEM */