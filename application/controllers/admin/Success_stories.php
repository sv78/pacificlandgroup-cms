<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Success_stories extends Admin_controller {

  public function header_edit() {
    $data = array();
    $data['title'] = 'Administrator: Success Stories - Header Edit';

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/success-stories', 'Success Stories'],
        ['admin/success-stories/header_edit', 'Edit header'],
    ];
    
    $data['html'] = file_get_contents($this->config->item('success_stories_header_file'));

    // Loading form validation library
    $this->load->library('form_validation');

    /*
     *  Setting validation rules for page fields
     */

    // content (html, js, php, etc.)
    $this->form_validation->set_rules('html', 'Content', 'trim');

    /*
     * Validating form
     */
    if ($this->form_validation->run() === false) {
      /*
       * If form data is not valid I generate this page again with errors
       * and return to start over
       */

      #$data['bc'][] = ["admin/pages/{$page['id']}", "Editing: " . Str::decode_plain_string($page['name'])];

      $data['content'] = $this->load->view('pages/admin/success_stories/header_edit', $data, true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }
    
    // save data to file
    file_put_contents($this->config->item('success_stories_header_file'), $this->input->post('html'));

    $data['bc'][] = ['admin/success-stories/header_edit/saved', 'Saved'];
    
    $data['content'] = $this->load->view('pages/admin/success_stories/save_success', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }
  
  public function content_edit() {
    $data = array();
    $data['title'] = 'Administrator: Success Stories - Content Edit';

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/success-stories', 'Success Stories'],
        ['admin/success-stories/content_edit', 'Edit content'],
    ];
    
    $data['html'] = file_get_contents($this->config->item('success_stories_page_content_file'));

    // Loading form validation library
    $this->load->library('form_validation');

    /*
     *  Setting validation rules for page fields
     */

    // content (html, js, php, etc.)
    $this->form_validation->set_rules('html', 'Content', 'trim');

    /*
     * Validating form
     */
    if ($this->form_validation->run() === false) {
      /*
       * If form data is not valid I generate this page again with errors
       * and return to start over
       */

      #$data['bc'][] = ["admin/pages/{$page['id']}", "Editing: " . Str::decode_plain_string($page['name'])];

      $data['content'] = $this->load->view('pages/admin/success_stories/content_edit', $data, true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }
    
    // save data to file
    file_put_contents($this->config->item('success_stories_page_content_file'), $this->input->post('html'));

    $data['bc'][] = ['admin/success-stories/content_edit/saved', 'Saved'];
    
    $data['content'] = $this->load->view('pages/admin/success_stories/save_success', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

}
