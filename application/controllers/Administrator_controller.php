<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator_controller extends Admin_Controller {

  public function index() {

    $data = array();
    $data['title'] = 'Administrator';

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
    ];

    $data['content'] = $this->load->view('pages/admin/index', '', true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

  public function pages($parent_id = NULL) {
    $data = array();
    $data['title'] = 'Administrator: Pages';

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/pages', 'Pages'],
    ];

    $this->load->model('admin/pages_model');

    if ($parent_id === NULL OR $parent_id == 0) {
      $nested_pages = $this->pages_model->get_nested_pages_by_id(NULL);
      $base_page = $this->pages_model->get_page_by_id(1);
      $parent_page_link = ['/admin/pages', "Root"];

      $count_all_pages = $this->pages_model->count_all_pages();
      $count_published_pages = $this->pages_model->count_published_pages();

      $data['count_all_pages'] = $count_all_pages;
      $data['count_published_pages'] = $count_published_pages;
    } else {
      $base_page = $this->pages_model->get_page_by_id($parent_id);
      $nested_pages = $this->pages_model->get_nested_pages_by_id($parent_id);
      $res = $this->pages_model->get_page_by_id($base_page['parent_id']);
      $parent_page_link = $res ? ["/admin/pages/" . $res['id'], $res['name']] : ['/admin/pages', "Root"];
      array_push($data['bc'], ['/admin/pages/' . $parent_id, $base_page['name']]);
    }

    $data['parent_page_link'] = $parent_page_link;

    if ($parent_id === NULL) {
      $data['parent_page_link'] = NULL;
    }

    $data['base_page'] = $base_page;
    $data['nested_pages'] = $nested_pages;

    $data['content'] = $this->load->view('pages/admin/pages/pages', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

  // create new page

  public function create_page($id = NULL) {
    $data = array();
    $data['title'] = 'Administrator: Create New Page';

    $this->load->model('admin/pages_model');
    $pages = $this->pages_model->get_all_pages('id,parent_id,name');

    $data['pages'] = $pages;
    $data['level_page_id'] = $id;

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/pages', 'Pages'],
        ['admin/pages/create', 'Create New Page'],
    ];

    // Loading form validation library (not extended with my rules)
    $this->load->library('form_validation');

    /*
     *  Setting validation rules for login
     */
    // parent page ID
    $this->form_validation->set_rules('parent_page_id', 'Parent Page', 'required|integer');
    // name
    $this->form_validation->set_rules('name', 'Page Name', 'trim|required|max_length[255]');
    // slug
    $this->form_validation->set_rules('slug', 'Slug', 'trim|required|max_length[64]|alpha_dash|is_unique[pages.slug]');



    /*
     * Validating form
     */
    if ($this->form_validation->run() === false) {
      /*
       * If form data is not valid I generate this page again with errors
       * and return to start over
       */
      $data['content'] = $this->load->view('pages/admin/pages/create_page', $data, true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }

    $db_data = array();
    $db_data['parent_id'] = Str::encode_plain_string($this->input->post('parent_page_id'));
    $db_data['name'] = Str::encode_plain_string($this->input->post('name'));
    $db_data['slug'] = mb_strtolower(Str::encode_plain_string($this->input->post('slug')));
    
    // hard code : template for new page
    $db_data['template'] = "template_d";

    // save to db and get back insert ID
    $insert_id = $this->pages_model->insert_new_page($db_data);

    $data['insert_id'] = $insert_id;
    $data['bc'][] = ['admin/pages/create', 'Saved'];

    $data['content'] = $this->load->view('pages/admin/pages/page_create_save_success', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

  // edit page

  public function edit_page($id = NULL) {
    if ($id === NULL) {
      redirect("/admin/pages");
    }
    $data = array();
    $data['title'] = 'Administrator: Pages - Edit Page';

    $this->load->model('admin/pages_model');

    $page = $this->pages_model->get_page_by_id($id);
    $all_pages = $this->pages_model->get_all_pages('id,parent_id,name');

    $this->load->helper('file');
    $files_in_template_dir = get_filenames(VIEWPATH . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . "public", false);
    $template_names = Utils::get_template_names_from_files($files_in_template_dir);

    $data['meta_robots_values'] = $this->pages_model->get_meta_robots_values();

    $data['page'] = $page;
    $data['all_pages'] = $all_pages;
    $data['template_names'] = $template_names;


    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/pages', 'Pages'],
            // ["admin/pages/{$page['id']}", "Editing: " . Str::decode_plain_string($page['name'])],
    ];


    // Loading form validation library
    $this->load->library('form_validation');

    /*
     *  Setting validation rules for page fields
     */

    // name
    $this->form_validation->set_rules('name', 'Page Name', 'trim|required|max_length[255]');

    // parent_id
    if ($id != 1) {
      $this->form_validation->set_rules('parent_id', 'Parent Page', "trim|required|integer|my_differs[{$id}]");
    }

    // template
    $this->form_validation->set_rules('template', 'Template', 'trim|required');

    // slug
    $this->form_validation->set_rules('slug', 'Slug', "trim|required|max_length[64]|alpha_dash|my_unique_page_slug[{$id}]");

    // title
    $this->form_validation->set_rules('title', 'Title', 'trim|max_length[512]');

    // meta description
    $this->form_validation->set_rules('meta_description', 'Meta Description', 'trim|max_length[512]');

    // meta keywords
    $this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim|max_length[512]');

    // meta robots
    $this->form_validation->set_rules('meta_robots', 'Meta Robots', 'trim|required');

    // breadcrumb
    $this->form_validation->set_rules('breadcrumb', 'Breadcrumb', 'trim|required|max_length[512]');

    // structured data
    $this->form_validation->set_rules('structured_data', 'Structured data', 'trim|max_length[16384]');

    // header
    $this->form_validation->set_rules('header', 'Header', 'trim|max_length[512]');

    // header description
    $this->form_validation->set_rules('header_description', 'Header description', 'trim|max_length[2048]');

    // content (html, js, php, etc.)
    $this->form_validation->set_rules('html', 'Content', 'trim');

    // redirect_301_url
    $this->form_validation->set_rules('redirect_301_url', 'Redirect (301) URL', 'trim|max_length[2048]');

    // is_published
    $this->form_validation->set_rules('is_published', 'Publish', 'trim');

    // is_in_menu
    $this->form_validation->set_rules('is_in_menu', 'Assign to menu', 'trim');


    /*
     * Validating form
     */
    if ($this->form_validation->run() === false) {
      /*
       * If form data is not valid I generate this page again with errors
       * and return to start over
       */

      $data['bc'][] = ["admin/pages/{$page['id']}", "Editing: " . Str::decode_plain_string($page['name'])];

      $data['content'] = $this->load->view('pages/admin/pages/edit_page', $data, true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }


    $db_data = array();
    if ($id != 1) {
      $db_data['parent_id'] = Str::encode_plain_string($this->input->post('parent_id'));
    }
    $db_data['name'] = Str::encode_plain_string($this->input->post('name'));
    $db_data['template'] = Str::encode_plain_string($this->input->post('template'));
    $db_data['slug'] = Str::encode_plain_string($this->input->post('slug'));
    $db_data['title'] = Str::encode_plain_string($this->input->post('title'));
    $db_data['meta_description'] = Str::encode_plain_string($this->input->post('meta_description'));
    $db_data['meta_keywords'] = Str::encode_plain_string($this->input->post('meta_keywords'));
    $db_data['meta_robots'] = Str::encode_plain_string($this->input->post('meta_robots'));
    $db_data['breadcrumb'] = Str::encode_plain_string($this->input->post('breadcrumb'));
    $db_data['structured_data'] = Str::encode_html_string($this->input->post('structured_data'));
    $db_data['header'] = Str::encode_html_string($this->input->post('header'));
    $db_data['header_description'] = Str::encode_html_string($this->input->post('header_description'));
    $db_data['content'] = Str::encode_html_string($this->input->post('content'));
    $db_data['redirect_301_url'] = Str::encode_plain_string($this->input->post('redirect_301_url'));
    $db_data['is_published'] = !empty($this->input->post('is_published')) ? 1 : 0;
    $db_data['is_in_menu'] = !empty($this->input->post('is_in_menu')) ? 1 : 0;

    // save to db and get back insert ID
    $this->pages_model->update_page($id, $db_data);


    $data['insert_id'] = $id;

    $data['bc'][] = ["admin/pages/edit/{$id}", "Editing: " . Str::decode_plain_string($page['name'])];
    $data['bc'][] = ["admin/pages/edit/{$id}", "Saved"];

    $data['content'] = $this->load->view('pages/admin/pages/page_edit_save_success', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

  public function customer_requests() {
    $data = array();
    $data['title'] = 'Administrator: Customer Requests';

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/customer-requests', 'Customer Requests'],
    ];

    $this->load->model('customer_requests_model', 'cr_model');
    $requests = $this->cr_model->get_requests();

    $data['requests'] = $requests;

    $data['content'] = $this->load->view('pages/admin/customer-requests', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

  public function customer_request($id) {
    $data = array();
    $data['title'] = 'Administrator: Customer Request';

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/customer-requests', 'Customer Requests'],
    ];

    $this->load->model('customer_requests_model', 'cr_model');

    $request = $this->cr_model->get_request_by_id($id);

    $data['bc'][] = ["admin/customer-requests/{$request['id']}", Str::decode_plain_string($request['name'])];

    $data['request'] = $request;

    $data['content'] = $this->load->view('pages/admin/customer-request', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

  public function success_stories() {
    $data = array();
    $data['title'] = 'Administrator: Success Stories Section';

    $this->load->model('success_stories_model', 'ss_model');

    $cats = $this->ss_model->get_cats();

    $data['cats'] = $cats;

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/success-stories', 'Success Stories Section'],
    ];

    $data['content'] = $this->load->view('pages/admin/success_stories', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

  public function success_stories_category($uri = NULL) {
    $data = array();
    $data['title'] = 'Administrator: Success Stories Section';

    $this->load->model('success_stories_model', 'ss_model');

    $cat = $this->ss_model->get_cat_by_id($uri);
    $data['cat'] = $cat;

    $projects = $this->ss_model->get_projects_by_cat_id($uri);
    $data['projects'] = $projects;

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/success-stories', 'Success Stories Section'],
        ["admin/success-stories/{$cat['id']}", Str::decode_plain_string($cat['name'])],
    ];

    $data['content'] = $this->load->view('pages/admin/success_stories_category', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

  // create new project

  public function create_project() {
    $data = array();
    $data['title'] = 'Administrator: Success Stories - Create New Project';

    $this->load->model('success_stories_model', 'ss_model');
    $cats = $this->ss_model->get_cats();

    $data['cats'] = $cats;

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/success-stories', 'Success Stories Section'],
        ['admin/success-stories/create-project', 'Create New Project'],
    ];

    // Loading form validation library (not extended with my rules)
    $this->load->library('form_validation');

    /*
     *  Setting validation rules for login
     */
    // category
    $this->form_validation->set_rules('category', 'Category', 'required|in_list[1,2,3,4,5]');

    // name
    $this->form_validation->set_rules('name', 'Project Name', 'trim|required|max_length[255]');


    /*
     * Validating form
     */
    if ($this->form_validation->run() === false) {
      /*
       * If form data is not valid I generate this page again with errors
       * and return to start over
       */
      $data['content'] = $this->load->view('pages/admin/success_stories_create_project', $data, true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }

    $slug = Str::create_slug_from_string($this->input->post('name'));

    if ($slug === '') {
      $data['slug_error_msg'] = "URL-slug can not be generated from this project name because wrong symbols is used that have been transliterated badly. Please try to use another project name.";
      $data['content'] = $this->load->view('pages/admin/success_stories_create_project', $data, true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }

    $db_data = array();
    $db_data['name'] = Str::encode_plain_string($this->input->post('name'));
    $db_data['category_id'] = Str::encode_plain_string($this->input->post('category'));
    $db_data['slug'] = Str::encode_plain_string($slug);

    // check whether slug is unique before saving to db
    if ($this->ss_model->slug_exists($slug)) {
      $data['slug_error_msg'] = "URL-slug configured automatically from the project name you specified is not unique. Please try to use another project name.";
      $data['content'] = $this->load->view('pages/admin/success_stories_create_project', $data, true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }

    // save to db and get back insert ID
    $insert_id = $this->ss_model->insert_new_project($db_data);

    $data['insert_id'] = $insert_id;
    $data['bc'][] = ['admin/success-stories/create-project', 'Saved'];

    $data['content'] = $this->load->view('pages/admin/success_stories_project_save_success', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

  // edit project

  public function edit_project($num_id = NULL) {
    $data = array();
    $data['title'] = 'Administrator: Success Stories - Edit Project';

    $this->load->model('success_stories_model', 'ss_model');

    $cats = $this->ss_model->get_cats();

    $project = $this->ss_model->get_project_by_id($num_id);
    $cat = $this->ss_model->get_cat_by_id($project['success_stories_cat_id']);

    $data['project'] = $project;
    $data['cat'] = $cat;
    $data['cats'] = $cats;

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/success-stories', 'Success Stories Section'],
        ["admin/success-stories/{$cat['id']}", Str::decode_plain_string($cat['name'])],
    ];


    // Loading form validation library
    $this->load->library('form_validation');

    /*
     *  Setting validation rules for category
     */
    // category
    $this->form_validation->set_rules('category', 'Category', 'required|in_list[1,2,3,4,5]');

    // name
    $this->form_validation->set_rules('name', 'Project Name', 'trim|required|max_length[255]');

    // title
    $this->form_validation->set_rules('title', 'Title', 'trim|required|max_length[255]');

    // title
    $this->form_validation->set_rules('meta_description', 'Meta Description', 'trim|required|max_length[255]');

    // lead
    $this->form_validation->set_rules('lead', 'Lead', 'trim|required|max_length[255]');

    // html
    $this->form_validation->set_rules('html', 'Content', 'trim');


    /*
     * Validating form
     */
    if ($this->form_validation->run() === false) {
      /*
       * If form data is not valid I generate this page again with errors
       * and return to start over
       */

      $data['bc'][] = ["admin/success-stories/edit-project/{$project['id']}", "Editing: " . Str::decode_plain_string($project['name'])];

      $data['content'] = $this->load->view('pages/admin/success_stories_edit_project', $data, true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }

    $this->load->model('success_stories_model', 'ss_model');

    $db_data = array();
    $db_data['category_id'] = Str::encode_plain_string($this->input->post('category'));
    $db_data['name'] = Str::encode_plain_string($this->input->post('name'));
    $db_data['title'] = Str::encode_plain_string($this->input->post('title'));
    $db_data['meta_description'] = Str::encode_plain_string($this->input->post('meta_description'));
    $db_data['lead'] = Str::encode_plain_string($this->input->post('lead'));
    $db_data['html'] = Str::encode_html_string($this->input->post('html'));
    $db_data['published'] = !empty($this->input->post('published')) ? 1 : 0;

    // save to db and get back insert ID
    $this->ss_model->update_project($num_id, $db_data);

    $data['insert_id'] = $num_id;

    $data['bc'][] = ["admin/success-stories/edit-project/{$project['id']}", "Editing: " . Str::decode_plain_string($project['name'])];
    $data['bc'][] = ["admin/success-stories/edit-project/{$project['id']}", "Saved"];

    $data['content'] = $this->load->view('pages/admin/success_stories_project_save_success', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

  // image file upload handler
  public function img_upload() {

    if (empty($_REQUEST['project_id']) && empty($_FILES['fileUpload']['name'])) {
      echo "error";
      return;
    }

    $file_name_arr = explode('.', $_FILES['fileUpload']['name']);
    $extension = $file_name_arr[count($file_name_arr) - 1];

    if ($extension != "jpg" && $extension != "jpeg" && $extension != "png" && $extension != "gif") {
      echo "error";
      return;
    }

    $uploaddir = config_item('ss_projects_img_dir');
    $uploadfile = $uploaddir . $_REQUEST['project_id'] . '.' . $extension;


    $this->load->model('success_stories_model', 'ss_model');
    $this->ss_model->update_img_extension($_REQUEST['project_id'], $extension);

    if (move_uploaded_file($_FILES['fileUpload']['tmp_name'], $uploadfile)) {
      //echo "<script>alert('File uploaded successfully!');</script>";
      //echo "success";
      echo $_REQUEST['project_id'] . '.' . $extension;
    } else {
      echo "error";
    }
  }

  public function delete_project($num_id) {
    $data = array();
    $data['title'] = 'Administrator: Success Stories - Delete Project';

    $this->load->model('success_stories_model', 'ss_model');

    $project = $this->ss_model->get_project_by_id($num_id);

    $cat = $this->ss_model->get_cat_by_id($project['success_stories_cat_id']);

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/success-stories', 'Success Stories Section'],
    ];

    //Array ( [id] => 12 [success_stories_cat_id] => 1 [name] => sdf [slug] => sdf [title] => [meta_description] => [lead] => [html] => [img_extension] => [published] => 0 )

    if (empty($project)) {
      $data['bc'][] = ["admin/success-stories", "Project delete failure"];

      $data['content'] = $this->load->view('pages/admin/success_stories_project_delete_failure', $data, true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }

    $data['project'] = $project;

    // delete from database
    $this->ss_model->delete_project($num_id);

    // delete images files

    $exts = array('jpg', 'jpeg', 'png', 'gif');

    foreach ($exts as $ext) {
      $file = config_item('ss_projects_img_dir') . $project['id'] . "." . $ext;
      if (file_exists($file)) {
        unlink($file);
      }
    }

    $data['bc'][] = ["admin/success-stories/{$cat['id']}", Str::decode_plain_string($cat['name'])];
    $data['bc'][] = ["admin/success-stories", "Project deleted"];

    $data['content'] = $this->load->view('pages/admin/success_stories_project_delete_success', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

  public function delete_page($id = NULL) {
    if ($id === NULL) {
      redirect("/admin/pages");
    }

    $data = array();
    $data['title'] = 'Administrator: Delete Page';

    $this->load->model('admin/pages_model');

    $page = $this->pages_model->get_page_by_id($id);

    if (empty($page)) {
      redirect('/admin/pages');
    }

    if ($id == 1) {
      $data['bc'][] = ["admin/pages", "Delete failure"];

      $data['content'] = $this->load->view('pages/admin/pages/page_root_delete_failure', $data, true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }

    $nested_pages = $this->pages_model->get_nested_pages_by_id($id);

    if (!empty($nested_pages)) {
      $data['bc'][] = ["admin/pages", "Delete failure"];

      $data['content'] = $this->load->view('pages/admin/pages/page_delete_failure', $data, true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/pages', 'Pages'],
    ];

    $data['page'] = $page;

    // delete from database
    $this->pages_model->delete_page($id);

    $data['bc'][] = ["admin/pages/{$id}", Str::decode_plain_string($page['name'])];
    $data['bc'][] = ["admin/pages/{$id}", "Deleted"];

    $data['content'] = $this->load->view('pages/admin/pages/page_delete_success', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

  public function file_manager() {
    /*
      $data['meta_title'] = "File manager";
      $this->load->view('templates/admin/header', $data);
      $this->load->view('admin/file_manager_view', $data);
      $this->load->view('templates/admin/footer', $data);
     */

    // breadcrumbs
    $data['bc'] = [
        ['admin', 'Admin'],
        ['admin/file-manager', 'File manager'],
    ];
    $data['content'] = $this->load->view('pages/admin/file_manager', $data, true);
    $this->load->view('templates/public/template_d', $data, false);
  }

}
