<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends MY_Controller {

  public function index() { // login
    if ($this->auth->is_admin()) {
      $data['title'] = 'Logged In';
      // breadcrumbs
      $data['bc'] = [
          ['admin', 'Admin'],
          ['login', 'Logged In'],
      ];
      $data['content'] = $this->load->view('pages/logged', "", true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }

    $data = array();
    $data['title'] = 'Login';
    $data['meta_robots'] = 'noindex, nofollow';

    // breadcrumbs
    $data['bc'] = [
        ['login', 'Login'],
    ];

    $this->load->helper('form');

    // Loading form validation library (not extended with my rules)
    $this->load->library('form_validation');

    /*
     *  Setting validation rules for login
     */
    // login
    $this->form_validation->set_rules('login', 'Login', 'trim|required|max_length[64]');

    // pwd (password)
    $this->form_validation->set_rules('pwd', 'Password', 'trim|required|max_length[32]');


    /*
     * Validating form
     */
    if ($this->form_validation->run() === false) {
      /*
       * If form data is not valid I generate this page again with errors
       * and return to start over
       */
      $data['content'] = $this->load->view('pages/login', '', true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }
    if (
            !password_verify($this->input->post('login'), config_item('login')) ||
            !password_verify($this->input->post('pwd'), config_item('password'))
    ) {
      $data['login_error_msg'] = "Login or password is not correct. Please try again.";
      $data['content'] = $this->load->view('pages/login', $data, true);
      $this->load->view('templates/admin/template_admin_a', $data, false);
      return;
    }


    $_SESSION['user']['is_admin'] = 1;
    redirect('admin', 'location', 301);
  }

  public function logout() {
    if (!$this->auth->is_admin()) {
      redirect('login', 'location', 301);
    }

    $_SESSION['user']['is_admin'] = 0;
    $data = array();
    $data['title'] = "Logged Out";
    $data['header'] = "Logged Out";
    $data['header_description'] = "You have logged out";

    // breadcrumbs
    $data['bc'] = [
        ['logout', 'Logout'],
    ];

    $data['content'] = $this->load->view('pages/logout', $data, true);
    $this->load->view('templates/admin/template_admin_a', $data, false);
  }

}
