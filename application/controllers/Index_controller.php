<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Index_Controller extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('admin/pages_model');
	}

	public function index() {

		if ($this->uri->total_segments() === 0) {
			$page = $this->pages_model->get_page_by_id(1);
			$page['breadcrumbs'] = array(['/', $page['breadcrumb']]);
			$page['breadcrumbs'] = [];
			$this->generate_page($page, true);
			return;
		}

		$uri_segments = explode("/", $this->uri->uri_string());

		$page = $this->pages_model->get_page_id_from_last_slug($uri_segments);

		if (empty($page)) {
			show_404();
		}

		$breadcrumbs = $this->uri_chain_is_true($page, $uri_segments);

		// _debug($breadcrumbs);
		// _debug($uri_segments);

		if ($breadcrumbs === false) {
			show_404();
			return;
		}

		$breadcrumbs = $this->fix_breadcrumbs_links($breadcrumbs);

		$page['breadcrumbs'] = $breadcrumbs;
		$this->generate_page($page);
	}

	private function fix_breadcrumbs_links($breadcrumbs) {
		$breadcrumbs = array_reverse($breadcrumbs);
		$new_breadcrumbs = [];
		$link = "";
		$len = count($breadcrumbs) - 1;
		for ($i = 0; $i <= $len; $i++) {
			$link .= $breadcrumbs[$i][0] . '/';
			$new_breadcrumbs[] = [substr($link, 0, strlen($link) - 1), $breadcrumbs[$i][1]];
		}
		return $new_breadcrumbs;
	}

	/**
	 * ---------------------------------------------------------------------------
	 * Checks URI slug chain and forms an array of BREADCRUMBS as single segments.
	 * Returns FALSE if chain is wrong!
	 * Return ARRAY OF BREADCRUMB VALUES if chain is true.
	 * ---------------------------------------------------------------------------
	 *
	 * @param type $last_segment_page
	 * @param type $uri_segments
	 * @return mixed
	 */
	private function uri_chain_is_true($last_segment_page, $uri_segments) {
		$uri_segments = array_reverse($uri_segments);
		$parent_page_id = $last_segment_page['parent_id'];

		$tree = [];
		$tree[] = [
			'id' 			=> $last_segment_page['id'],
			'parent_id' 	=> $last_segment_page['parent_id'],
			'slug' 			=> $last_segment_page['slug'],
			'breadcrumb' 	=> $last_segment_page['breadcrumb']
		];

		foreach ($uri_segments as $segment) {
			$parent_page_data = $this->pages_model->get_page_by_id($parent_page_id, 'id,parent_id,slug,breadcrumb');
			$tree[] = $parent_page_data;
			$parent_page_id = $parent_page_data['parent_id'];
		}

		if (!is_null($tree[sizeof($tree) - 1]['parent_id'])) {
			return false;
		}

		$breadcrumbs = [];
		foreach ($tree as $item) {
			$breadcrumbs[] = [$item['slug'], $item['breadcrumb']];
		}
		array_pop($breadcrumbs);
		return $breadcrumbs;
	}

	/**
	 *
	 * ---------------------------------------------------------------------------
	 * Generates a page by known ID
	 * ---------------------------------------------------------------------------
	 *
	 * @param type $id
	 */
	private function generate_page($page, $is_root = NULL) {
		if (empty($page) or $page['is_published'] == 0) {
			show_404();
		}
		if (!empty($page['redirect_301_url'])) {
			redirect($page['redirect_301_url'], "location", 301);
		}
		$data = array();
		$data['title'] = Str::decode_plain_string($page['title']);
		$data['meta_description'] = Str::decode_plain_string($page['meta_description']);
		$data['meta_keywords'] = Str::decode_plain_string($page['meta_keywords']);
		$data['meta_robots'] = $page['meta_robots'];
		$data['structured_data'] = Str::decode_html_string($page['structured_data']);
		$data['bc'] = $page['breadcrumbs'];
		$data['header'] = Str::decode_html_string($page['header']);
		$data['header_description'] = Str::decode_html_string($page['header_description']);
		$data['content'] = Str::decode_html_string($page['content']);
		$this->load->view('templates/public/' . $page['template'], $data, false);
	}

}
