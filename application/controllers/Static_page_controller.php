<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Static_page_Controller extends MY_Controller {

  /**
   * ---------------------------------------------------------------------------
   * Generates Contact Us page as exclusion
   * ---------------------------------------------------------------------------
   * 
   * @return type
   */
  public function contact_us() {
    $data['title'] = "Contact Pacific Land Group: Challenging Land Use Planning & Environmental Solutions";
    $data['meta_description'] = "Contact Pacific Land Group for help & support with land use planning, development and environmental solutions, erosion & sediment control, civil engineering and land survey – (604) 501-1624";
    // $data['header'] = "Contact Us";
    // $data['header_description'] = "Please help us to better direct your inquiry by indicating the area of your interests";
    // breadcrumbs
    $data['bc'] = [
        ['contact-us', 'Contact Us'],
    ];

    $this->load->helper('form');

    // Loading form validation library (not extended with my rules)
    $this->load->library('form_validation');

    /*
     *  Setting validation rules
     */
    $this->form_validation->set_rules('name', 'Name', "required|max_length[255]");
    $this->form_validation->set_rules('company', 'Company', 'max_length[255]');
    $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|valid_email');
    $this->form_validation->set_rules('phone', 'Phone', 'required|max_length[255]');
    $this->form_validation->set_rules('message', 'Message', 'required|max_length[9999]');
    $this->form_validation->set_rules('types[]', 'Area of interests', 'in_list[lup,es,ersc,ce,ls]');
    $this->form_validation->set_rules('foc[]', 'Area of interests', 'in_list[phone,email]');

    /*
     * Validating form
     */
    if ($this->form_validation->run() === false) {
      /*
       * If form data is not valid I generate this page again with errors
       * and return to start over
       */
      $data['content'] = $this->load->view('pages/contact-us', $data, true);
      $this->load->view('templates/public/template_d', $data, false);
      return;
    }

    $db_data = array();
    $db_data['name'] = Str::encode_plain_string($this->input->post('name'));
    $db_data['company'] = Str::encode_plain_string($this->input->post('company'));
    $db_data['email'] = Str::encode_plain_string($this->input->post('email'));
    $db_data['phone'] = Str::encode_plain_string($this->input->post('phone'));
    $db_data['message'] = Str::encode_plain_string($this->input->post('message'));
    $db_data['type'] = !empty($this->input->post('types[]')) ? Str::prepare_for_set_type($this->input->post('types[]')) : '';
    $db_data['foc'] = !empty($this->input->post('foc[]')) ? Str::prepare_for_set_type($this->input->post('foc[]')) : '';
    $db_data['time'] = time();

    // saving data to DB
    $this->load->model('customer_requests_model', 'cr_model');
    $this->load->library('Mailer');

    $msg = Str::create_customer_request_mail_message($db_data);

    $this->cr_model->save_request($db_data);
    $this->mailer->email_customer_request($msg);

    $_SESSION['customer_request_done'] = 1;
    redirect('customer-request-done', 'location', 301);
  }

  /**
   * ---------------------------------------------------------------------------
   * Generates Contact From Page Submition as exclusion
   * ---------------------------------------------------------------------------
   * 
   * @return type
   */
  public function customer_request_done() {
    // breadcrumbs
    $data['bc'] = [
        ['contact-us', 'Contact Us'],
        ['customer-request-done', 'Request Success'],
    ];
    if (isset($_SESSION['customer_request_done'])) {
      unset($_SESSION['customer_request_done']);
      $data['content'] = $this->load->view('pages/customer-request-success', $data, true);
      $this->load->view('templates/public/template_d', $data, false);
      return;
    }
    redirect('/', 'location', 301);
  }

}
