<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator_ajax_controller extends Admin_ajax_Controller {

  public function delete_img() {
    $exts = array('jpg', 'jpeg', 'png', 'gif');
    $error = false;
    $files_deleted = 0;

    foreach ($exts as $ext) {
      $file = config_item('ss_projects_img_dir') . $this->input->post('project_id') . "." . $ext;
      if (file_exists($file)) {
        if (!unlink($file)) {
          $error = true;
        } else {
          $files_deleted++;
        }
      }
    }
    if ($files_deleted === 0) {
      echo "There is no image to delete!";
      return;
    }
    if ($error) {
      echo "Error occured while trying to delete image!";
    } else {
      echo "Image deleted successfully!";
    }
  }

}
