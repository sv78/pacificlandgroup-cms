<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Success_stories_controller extends MY_Controller {
  
  public $content = [];
  public $header_file;
  
  public $ss_header = "";
  
  public function __construct() {
    parent::__construct();
    $this->header_file = $this->config->item('success_stories_header_file');
    $this->ss_header = file_get_contents($this->header_file);
  }

  public function index() {
    $data = array();
    $data['title'] = 'Land Use Planning & Land Development Success Stories – Pacific Land Group';
    $data['meta_description'] = 'Successful land development, sustainable design, commercial & industrial development and best land use analysis projects by Pacific Land Group, Surrey, British Columbia (604) 501-1624.';
    
    $this->load->model('success_stories_model', 'ss_model');

    // creating project lists by categories - one by one ))))

    $sustainability = $this->ss_model->get_projects_by_cat_id(1, true);
    $data['project_list'] = $sustainability;
    $data['projects']['sustainability'] = $this->load->view('chunks/success-stories/projects-list', $data, true);

    $agricultural_land = $this->ss_model->get_projects_by_cat_id(2, true);
    $data['project_list'] = $agricultural_land;
    $data['projects']['agricultural_land'] = $this->load->view('chunks/success-stories/projects-list', $data, true);

    $expropriation_impact = $this->ss_model->get_projects_by_cat_id(3, true);
    $data['project_list'] = $expropriation_impact;
    $data['projects']['expropriation_impact'] = $this->load->view('chunks/success-stories/projects-list', $data, true);

    $commercial = $this->ss_model->get_projects_by_cat_id(4, true);
    $data['project_list'] = $commercial;
    $data['projects']['commercial'] = $this->load->view('chunks/success-stories/projects-list', $data, true);

    $residential_development = $this->ss_model->get_projects_by_cat_id(5, true);
    $data['project_list'] = $residential_development;
    $data['projects']['residential_development'] = $this->load->view('chunks/success-stories/projects-list', $data, true);

    // breadcrumbs
    $data['bc'] = [
        ['success-stories', 'Success Stories'],
    ];
    
    $this->content = $data['projects'];
    
    $replace_callback = function ($match) {
      $m = $match[1];
      if (isset($this->content[$m])) {
        return $this->content[$m];
      } else {
        return $match[0];
      }
    };

    $content = $this->load->view('pages/success-stories/index_editable', $data, true);
    $content = preg_replace_callback("#\{\{(.*)\}\}#U", $replace_callback, $content);
    
    $data['content'] = $content;
    $data['header'] = $this->ss_header;
    $this->load->view('templates/success_stories/template_ss', $data, false);
  }

  public function project($cat_uri, $project_uri) {
    $this->load->model('success_stories_model', 'ss_model');
    $project = $this->ss_model->get_project_by_slug($project_uri);

    $cat = $this->ss_model->get_cat_by_id($project['success_stories_cat_id']);

    if ($cat['slug'] != $cat_uri) {
      show_404();
    }

    $data = array();

    $data['project'] = $project;

    $data['title'] = Str::decode_plain_string($project['title']);
    $data['meta_description'] = Str::decode_plain_string($project['meta_description']);

    // breadcrumbs
    $data['bc'] = [
        ['success-stories', 'Success Stories'],
        ["success-stories/{$cat['slug']}", Str::decode_plain_string($cat['name'])],
        ["success-stories/{$cat['slug']}/{$project['slug']}", Str::decode_plain_string($project['name'])],
    ];

    $data['header'] = $this->ss_header;

    $data['content'] = Str::decode_html_string($project['html']);
    $this->load->view('templates/success_stories/template_ss', $data, false);
  }

  public function category($uri) {
    $this->load->model('success_stories_model', 'ss_model');
    $cat = $this->ss_model->get_cat_by_slug($uri);

    // breadcrumbs
    $data['bc'] = [
        ['success-stories', 'Success Stories'],
        ["success-stories/{$cat['slug']}", Str::decode_plain_string($cat['name'])],
    ];

    if (empty($cat)) {
      show_404();
      return;
    }
    
    $cat_projects = $this->ss_model->get_projects_by_cat_id($cat['id'], true);
    $data['project_list'] = $cat_projects;
    $data['projects']['category'] = $this->load->view('chunks/success-stories/projects-list', $data, true);

    $data['title'] = "Pacific Land Use Success Stories";
    $data['meta_description'] = "Pacific Land Use Success Stories";

    $data['header_description'] = Str::decode_plain_string($cat['name']);

    $data['header'] = $this->ss_header;
    $data['header_2'] = Str::decode_plain_string($cat['name']);
    
    $data['content'] = $this->load->view('pages/success-stories/category', $data, true);
    $this->load->view('templates/success_stories/template_ss', $data, false);
  }

}
